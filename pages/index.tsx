import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import SectionA from '../components/home/SectionA'
import { RootState } from '../redux/reducers'
import Location from '../components/home/Location'
import Navbar from '../components/shared/Navbar'
import Pricing from '../components/home/Pricing'
import { Divider } from '@chakra-ui/react'
import Process from '../components/home/Process'
import Services from '../components/home/Services'
import Contact from '../components/home/Contact'
import Footer from '../components/home/Footer'
import { GetStaticProps } from 'next'
import APIService from '../utils/api'

type Props = {
  prices: {
    delivery: number
    drySelf: number
    washDrySelf: number
    iron: number
  }
}

const Home = ({ prices }: Props) => {
  const router = useRouter()
  const { user, isAuthenticated, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )

  useEffect(() => {
    if (!authLoading && user && isAuthenticated) {
      router.prefetch('/dashboard')
    }
  }, [user, isAuthenticated, authLoading])

  return (
    <div>
      <Head>
        <title>Instant Wash N Dry Laundromat</title>
        <meta name='keywords' content='Laundry Service' />
      </Head>
      <Navbar />
      <SectionA />
      <Process />
      <Divider my='8rem' />
      <Services />
      <Divider my='8rem' />
      <Pricing prices={prices} />
      <Divider my='8rem' />
      <Location />
      <Divider my='8rem' />
      <Contact />
      <Divider my='8rem' />
      <footer>
        <Footer />
      </footer>
    </div>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const res = await APIService.getPrices()
    const prices = res.data.data[0]
    return {
      props: {
        prices,
      },
    }
  } catch (error) {
    return {
      revalidate: 43200,
      props: {
        prices: {},
      },
    }
  }
}

export default Home
