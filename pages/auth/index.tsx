import { useRouter } from 'next/router'
import Head from 'next/head'
import React from 'react'
import { Box, Flex, Link } from '@chakra-ui/layout'
import Login from '../../components/auth/Login'
import Signup from '../../components/auth/Signup'
import RightSection from '../../components/auth/RightSection'
import ForgotPassword from '../../components/auth/ForgotPassword'
import Reset from '../../components/auth/Reset'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'

const Auth = () => {
  const route = useRouter()
  const { isMobile } = useSelector((state: RootState) => state.appReducer)

  return (
    <Box>
      <Head>
        <title>Get Started</title>
      </Head>
      <Flex
        flexDirection={['column', 'column', 'row']}
        width='100%'
        height='100vh'
      >
        <Box width={['100%', '100%', '50%']}>
          {route.query.type === 'login' ? (
            <Login />
          ) : route.query.type === 'forgotPassword' ? (
            <ForgotPassword />
          ) : route.query.type === 'reset' ? (
            <Reset />
          ) : (
            <Signup />
          )}
        </Box>

        {!isMobile && <RightSection />}
      </Flex>
    </Box>
  )
}

export default Auth
