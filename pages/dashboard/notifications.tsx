import React, { useEffect, useState } from 'react'
import { Box, HStack } from '@chakra-ui/react'
import Head from 'next/head'
import SideBar from '../../components/dashboard/SideBar'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'
import NoAuth from '../../components/dashboard/NoAuth'
import StoreNotifications from '../../components/dashboard/store/StoreNotifications'
import { bindActionCreators } from 'redux'
import appActions from '../../redux/actions/app'
import { useRouter } from 'next/router'

const notifications = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const [searchQuery, setSearchQuery] = useState('')
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { _getUsers } = bindActionCreators(appActions, dispatch)

  const onSearch = async (query: string) => {
    setSearchQuery(query)
    await _getUsers(query, undefined, {
      page: 1,
      limit: 5,
      callback: () => {},
    })
  }

  useEffect(() => {
    if (
      authLoading === false &&
      user &&
      isAuthenticated &&
      user.role === 'user'
    ) {
      router.replace('/dashboard')
    }
  }, [isAuthenticated, user, authLoading])

  return (
    <Box h='100vh' w='full'>
      <Head>
        <title>Dashboard | Notifications</title>
      </Head>
      <HStack alignItems='flex-start'>
        <SideBar callback={onSearch} />
        <Box h='full' width='75%' position='relative'>
          <NoAuth />
          <StoreNotifications searchQuery={searchQuery} />
        </Box>
      </HStack>
    </Box>
  )
}

export default notifications
