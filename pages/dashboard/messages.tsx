import React, { useEffect, useState } from 'react'
import { Box, HStack, Text } from '@chakra-ui/layout'
import Head from 'next/head'
import SideBar from '../../components/dashboard/SideBar'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'
import NoAuth from '../../components/dashboard/NoAuth'
import { useRouter } from 'next/router'
import { bindActionCreators } from 'redux'
import appActions from '../../redux/actions/app'
import { Spinner } from '@chakra-ui/spinner'
import StoreMessages from '../../components/dashboard/store/StoreMessages'
import { Image } from '@chakra-ui/image'

const messages = () => {
  const dispatch = useDispatch()
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { messages } = useSelector((state: RootState) => state.dashboardReducer)

  const { _getMessages } = bindActionCreators(appActions, dispatch)

  const loadData = async () => {
    setLoading(true)
    await _getMessages(setLoading)
  }

  useEffect(() => {
    if (
      authLoading === false &&
      user &&
      isAuthenticated &&
      user.role === 'user'
    ) {
      router.replace('/dashboard')
    }

    if (authLoading === false && user && isAuthenticated) loadData()
  }, [isAuthenticated, user, authLoading])

  return (
    <Box h='100vh' w='full'>
      <Head>
        <title>Dashboard | Messages</title>
      </Head>
      <HStack alignItems='flex-start'>
        <SideBar />
        <Box height='100vh' width='75%' position='relative'>
          <NoAuth />

          {authLoading === false && user && isAuthenticated && (
            <>
              {loading && (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                >
                  <Spinner size='lg' />
                </Box>
              )}
              {!loading && messages.data.length === 0 ? (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                  textAlign='center'
                >
                  <Image src='/nomessage.png' />
                  <Text fontWeight='bold' color='gray.800'>
                    No Messages
                  </Text>
                </Box>
              ) : (
                <Box pt='1rem'>
                  <StoreMessages />
                </Box>
              )}
            </>
          )}
        </Box>
      </HStack>
    </Box>
  )
}

export default messages
