import React, { useEffect, useState } from 'react'
import { Text, Box, HStack, Heading, VStack } from '@chakra-ui/layout'
import Head from 'next/head'
import SideBar from '../../../../components/dashboard/SideBar'
import { Button } from '@chakra-ui/button'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../../redux/reducers'
import NoAuth from '../../../../components/dashboard/NoAuth'
import router from 'next/router'
import { Image } from '@chakra-ui/image'
import appActions from '../../../../redux/actions/app'
import { bindActionCreators } from 'redux'
import AlertDialog from '../../../../components/shared/AlertDialog'
import { useDisclosure } from '@chakra-ui/hooks'
import { EServiceType } from '../../../../utils/generics'
import DashboardNavbarMobile from '../../../../components/shared/DashboardNavbarMobile'

const laundry = () => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const { newLaundry, isMobile } = useSelector(
    (state: RootState) => state.appReducer,
  )
  const { isAuthenticated, authLoading, user } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { isOpen, onOpen, onClose } = useDisclosure()
  const {
    isOpen: isCancelOpen,
    onOpen: onCancelOpen,
    onClose: onCancelClose,
  } = useDisclosure()

  const { _createNewLaundry, _createLaundry } = bindActionCreators(
    appActions,
    dispatch,
  )

  const onCancel = () => {
    onCancelClose()
    _createLaundry('delete')
    router.replace({
      pathname: `/dashboard/laundry`,
      query: { tab: 212 },
    })
  }

  const next = async () => {
    setLoading(true)
    await _createNewLaundry(
      { ...newLaundry, laundryDetails: [], total: 0, subTotal: 0 },
      user?.role,
      setLoading,
    )

    user?.role === 'user'
      ? onOpen()
      : router.replace({
          pathname: `/dashboard/laundry`,
          query: { tab: 212 },
        })
  }

  const onFinish = async () => {
    onClose()
    router.replace({
      pathname: `/dashboard/laundry`,
      query: { tab: 212 },
    })
  }

  useEffect(() => {
    _createLaundry('get')
  }, [])

  return (
    <Box h='100vh' w='full'>
      <Head>
        <title>Dashboard | Laundry</title>
      </Head>
      {isMobile && <DashboardNavbarMobile />}
      <AlertDialog
        isOpen={isOpen}
        onClose={onClose}
        heading='Laundry Created'
        body={
          newLaundry.serviceType === EServiceType.delivery
            ? 'Your laundry has been created you will get a call soon'
            : 'Your laundry has been created please await inspection'
        }
        type='info'
        onOkayClick={onFinish}
      />
      <AlertDialog
        onYesClick={onCancel}
        onNoClick={onCancelClose}
        heading='Discard Changes'
        onClose={onCancelClose}
        isOpen={isCancelOpen}
        body='Are you sure?'
        type='prompt'
      />
      <HStack alignItems='flex-start'>
        {!isMobile && <SideBar />}
        <Box height='100vh' width={['100%', '100%', '75%']} position='relative'>
          <NoAuth />

          {authLoading === false &&
            user &&
            isAuthenticated &&
            (!newLaundry.id ? (
              <Box
                position='absolute'
                top='50%'
                left='50%'
                transform='translate(-50%, -50%)'
                textAlign='center'
              >
                <Image src='/emptyCart.png' w={['90%', '90%', '75%']} />

                <Button
                  onClick={() =>
                    router.replace({
                      pathname: '/dashboard/laundry',
                      query: { tab: router.query.tab },
                    })
                  }
                  colorScheme='blue'
                  _focus={{ outline: 'none' }}
                >
                  Create Laundry
                </Button>
              </Box>
            ) : (
              <Box padding='2rem'>
                <HStack spacing={8} mb='1rem'>
                  {newLaundry.service === 'Wash And Dry' ? (
                    <>
                      <Image src='/bubbles.png' width='100px' height='100px' />
                      <Heading>Wash And Dry</Heading>
                    </>
                  ) : newLaundry.service === 'Dry' ? (
                    <>
                      <Image src='/dry.png' width='100px' height='100px' />
                      <Heading>Dry</Heading>
                    </>
                  ) : (
                    <>
                      <Image src='/ironing.png' width='100px' height='100px' />
                      <Heading>Iron</Heading>
                    </>
                  )}
                </HStack>
                <VStack
                  spacing={4}
                  alignItems='flex-start'
                  fontWeight='bold'
                  mb='1.5rem'
                >
                  {user.role !== 'user' && (
                    <>
                      <Text>Name: {newLaundry.user.name}</Text>
                      <Text>Email: {newLaundry.user.email}</Text>
                      <Text>Phone: {newLaundry.user.phone}</Text>
                    </>
                  )}
                  {newLaundry.service !== 'Iron' && (
                    <Text>
                      {newLaundry.service} : {newLaundry.serviceQty}
                    </Text>
                  )}
                  <Text>
                    Service Type:{' '}
                    {newLaundry.serviceType === EServiceType.delivery
                      ? 'Delivery Service'
                      : newLaundry.serviceType === EServiceType.dropoff
                      ? 'Drop Off Service'
                      : 'Self Service'}
                  </Text>
                  {newLaundry.iron && <Text>Iron</Text>}
                  {newLaundry.detergent && (
                    <Text>Detergent : {newLaundry.detergentQty}</Text>
                  )}
                  {newLaundry.laundryBag && (
                    <Text>Laundry Bag : {newLaundry.laundryBagQty}</Text>
                  )}
                  <Text>Address : {newLaundry.location}</Text>
                  <Text>Payment Method: {newLaundry.paymentMethod}</Text>
                </VStack>
                <Box mb='1rem'>
                  <Text color='red.500' fontStyle='italic'>
                    Prices isn't calculated yet.
                  </Text>
                </Box>
                <HStack spacing={4}>
                  <Button
                    onClick={onCancelOpen}
                    _focus={{ outline: 'none' }}
                    variant='ghost'
                    px='2.5rem'
                    py='1.3rem'
                  >
                    Cancel
                  </Button>
                  <Button
                    colorScheme='blue'
                    onClick={next}
                    _focus={{ outline: 'none' }}
                    isLoading={loading}
                    px='2.5rem'
                    py='1.3rem'
                  >
                    Next
                  </Button>
                </HStack>
              </Box>
            ))}
        </Box>
      </HStack>
    </Box>
  )
}

export default laundry
