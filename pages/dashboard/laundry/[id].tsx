import React from 'react'
import { useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { Box } from '@chakra-ui/layout'
import UserLaundry from '../../../components/dashboard/user/UserLaundry'
import StoreLaundry from '../../../components/dashboard/store/StoreLaundry'

const Laundry = () => {
  const { user } = useSelector((state: RootState) => state.authReducer)

  return (
    <Box h='100vh' w='full'>
      {user?.role === 'user' ? <UserLaundry /> : <StoreLaundry />}
    </Box>
  )
}

export default Laundry
