import React, { useState } from 'react'
import { Box, HStack, Text } from '@chakra-ui/layout'
import Head from 'next/head'
import SideBar from '../../../components/dashboard/SideBar'
import FloatingButton from '../../../components/shared/FloatingButton'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import NoAuth from '../../../components/dashboard/NoAuth'
import { useEffect } from 'react'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import LaundryModal from '../../../components/dashboard/LaundryModal'
import { useDisclosure } from '@chakra-ui/react'
import UserLaundryDashboard from '../../../components/dashboard/user/UserLaundryDashboard'
import StoreLaundryDashboard from '../../../components/dashboard/store/StoreLaundryDashboard'
import { useRouter } from 'next/router'
import DashboardNavbarMobile from '../../../components/shared/DashboardNavbarMobile'

const Laundries = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const [searchQuery, setSearchQuery] = useState('')
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { isMobile } = useSelector((state: RootState) => state.appReducer)

  const { _getLaundries } = bindActionCreators(appActions, dispatch)

  const onSearch = async (query: string) => {
    setSearchQuery(query)
    await _getLaundries(query)
  }

  useEffect(() => {
    router.prefetch('/dashboard/laundry/new')
  }, [])

  return (
    <Box h='100vh' w='full'>
      <Head>
        <title>Dashboard | Laundry</title>
      </Head>
      {isMobile && <DashboardNavbarMobile />}
      <LaundryModal onClose={onClose} isOpen={isOpen} />
      <HStack alignItems='flex-start'>
        {!isMobile && <SideBar callback={onSearch} />}
        <Box height='100vh' width={['100%', '100%', '75%']} position='relative'>
          <NoAuth />

          {authLoading === false && user && isAuthenticated && (
            <>
              {user.role === 'user' ? (
                <UserLaundryDashboard />
              ) : (
                <StoreLaundryDashboard searchQuery={searchQuery} />
              )}
            </>
          )}
        </Box>
      </HStack>
      {isAuthenticated && user && <FloatingButton callback={onOpen} />}
    </Box>
  )
}

export default Laundries
