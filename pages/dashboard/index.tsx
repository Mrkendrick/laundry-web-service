import { useRouter } from 'next/router'
import React from 'react'
import { useEffect } from 'react'

const Dashboard = () => {
  const router = useRouter()

  useEffect(() => {
    router.replace({
      pathname: '/dashboard/laundry',
      query: { tab: 212 },
    })
  }, [])

  return <></>
}

export default Dashboard
