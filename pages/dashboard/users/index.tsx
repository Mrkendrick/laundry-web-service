import React, { useState } from 'react'
import { Box, HStack, Text } from '@chakra-ui/react'
import Head from 'next/head'
import SideBar from '../../../components/dashboard/SideBar'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import NoAuth from '../../../components/dashboard/NoAuth'
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import UsersTable from '../../../components/dashboard/store/UsersTable'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { Spinner } from '@chakra-ui/spinner'
import { Image } from '@chakra-ui/image'

const users = () => {
  const dispatch = useDispatch()
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const [searchQuery, setSearchQuery] = useState('')
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { users } = useSelector((state: RootState) => state.dashboardReducer)

  const { _getUsers } = bindActionCreators(appActions, dispatch)

  const onSearch = async (query: string) => {
    setSearchQuery(query)
    await _getUsers(query)
  }

  const loadData = async () => {
    if (isAuthenticated && user && authLoading === false) {
      setLoading(true)
      await _getUsers('', setLoading)
    }
  }

  useEffect(() => {
    if (
      authLoading === false &&
      user &&
      isAuthenticated &&
      user.role === 'user'
    ) {
      router.replace('/dashboard')
    }
    loadData()
  }, [isAuthenticated, user, authLoading])

  return (
    <Box h='100vh' w='full'>
      <Head>
        <title>Dashboard | Users</title>
      </Head>
      <HStack alignItems='flex-start'>
        <SideBar callback={onSearch} />
        <Box height='100vh' width='75%' position='relative'>
          <NoAuth />
          {authLoading === false &&
            user &&
            isAuthenticated &&
            (loading ? (
              <Box
                position='absolute'
                top='50%'
                left='50%'
                transform='translate(-50%, -50%)'
              >
                <Spinner size='xl' />
              </Box>
            ) : users.data.length === 0 ? (
              <Box
                position='absolute'
                top='50%'
                left='50%'
                transform='translate(-50%, -50%)'
                textAlign='center'
              >
                <Image w='70%' src='/nodata.png' />
                <Text fontWeight='bold'>No Users found</Text>
              </Box>
            ) : (
              <Box h='full' pt='1rem'>
                <UsersTable loadData={loadData} searchQuery={searchQuery} />
              </Box>
            ))}
        </Box>
      </HStack>
    </Box>
  )
}

export default users
