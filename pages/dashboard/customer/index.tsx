import React, { useEffect, useState } from 'react'
import { Box, HStack, Text } from '@chakra-ui/layout'
import Head from 'next/head'
import SideBar from '../../../components/dashboard/SideBar'
import FloatingButton from '../../../components/shared/FloatingButton'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import NoAuth from '../../../components/dashboard/NoAuth'
import { useRouter } from 'next/router'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { Spinner } from '@chakra-ui/spinner'
import { Image } from '@chakra-ui/image'
import StoreCustomerDashboard from '../../../components/dashboard/store/StoreCustomerDashboard'
import { useDisclosure } from '@chakra-ui/react'
import CustomerModal from '../../../components/dashboard/store/CustomerModal'

const customers = () => {
  const dispatch = useDispatch()
  const router = useRouter()
  const [searchQuery, setSearchQuery] = useState('')
  const { isOpen, onClose, onOpen } = useDisclosure()
  const [loading, setLoading] = useState(false)
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { customers } = useSelector(
    (state: RootState) => state.dashboardReducer,
  )

  const { _getCustomers } = bindActionCreators(appActions, dispatch)

  const onSearch = async (query: string) => {
    setSearchQuery(query)
    await _getCustomers(query, setLoading)
  }

  const loadData = async () => {
    setLoading(true)
    await _getCustomers(undefined, setLoading)
  }

  useEffect(() => {
    if (
      authLoading === false &&
      user &&
      isAuthenticated &&
      user.role === 'user'
    ) {
      router.replace('/dashboard')
    }

    if (authLoading === false && user && isAuthenticated) loadData()
  }, [isAuthenticated, user, authLoading])
  return (
    <Box h='100vh' w='full'>
      <Head>
        <title>Dashboard | Customers</title>
      </Head>
      <CustomerModal isOpen={isOpen} onClose={onClose} type='create' />
      <HStack alignItems='flex-start'>
        <SideBar callback={onSearch} />
        <Box height='100vh' width='75%' position='relative'>
          <NoAuth />
          {authLoading === false && user && isAuthenticated && (
            <>
              {loading && (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                >
                  <Spinner size='xl' />
                </Box>
              )}
              {!loading && customers.data.length === 0 ? (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                  textAlign='center'
                >
                  <Image src='/nouser.png' />
                  <Text fontWeight='bold' color='gray.800'>
                    No Customers
                  </Text>
                </Box>
              ) : (
                <Box pt='1rem'>
                  <StoreCustomerDashboard searchQuery={searchQuery} />
                </Box>
              )}
            </>
          )}
        </Box>
      </HStack>
      {isAuthenticated && user && <FloatingButton callback={onOpen} />}
    </Box>
  )
}

export default customers
