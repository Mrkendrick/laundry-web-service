import Head from 'next/head'
import React, { useState } from 'react'
import { HStack, Box, Text, VStack, Badge } from '@chakra-ui/layout'
import SideBar from '../../../components/dashboard/SideBar'
import NoAuth from '../../../components/dashboard/NoAuth'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { Spinner } from '@chakra-ui/spinner'
import { Image } from '@chakra-ui/image'
import { useEffect } from 'react'
import { useCallback } from 'react'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { useRouter } from 'next/router'
import { Avatar } from '@chakra-ui/avatar'
import { Button, IconButton } from '@chakra-ui/button'
import { Table, Thead, Tbody, Tr, Th, Td } from '@chakra-ui/table'
import { EProgress, EServiceType } from '../../../utils/generics'
import UtilityService from '../../../utils/utils'
import Pagination from '../../../components/shared/Pagination'
import { MdEdit } from 'react-icons/md'
import { AiFillDelete } from 'react-icons/ai'
import { useDisclosure } from '@chakra-ui/react'
import CustomerModal from '../../../components/dashboard/store/CustomerModal'
import AlertDialog from '../../../components/shared/AlertDialog'
import LaundryModal from '../../../components/dashboard/LaundryModal'
import { floor } from 'lodash'

const customer = () => {
  const dispatch = useDispatch()
  const router = useRouter()
  const {
    isOpen: isEditOpen,
    onOpen: onEditOpen,
    onClose: onEditClose,
  } = useDisclosure()

  const {
    isOpen: isDeleteOpen,
    onOpen: onDeleteOpen,
    onClose: onDeleteClose,
  } = useDisclosure()

  const {
    isOpen: isAddLaundryOpen,
    onOpen: onAddLaundryOpen,
    onClose: onAddLaundryClose,
  } = useDisclosure()

  const [loading, setLoading] = useState(false)
  const { authLoading, isAuthenticated, user } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { customer, customerLaundries } = useSelector(
    (state: RootState) => state.dashboardReducer,
  )
  const {
    _getCustomer,
    _getCustomerLaundries,
    _deleteCustomer,
    _resetCustomerPage,
  } = bindActionCreators(appActions, dispatch)

  const setPage = async (num: number, callback: () => void) => {
    const options = {
      page: num,
      limit: 10,
      callback,
    }

    await _getCustomerLaundries(router.query?.email, undefined, options)
  }

  const onDelete = async () => {
    onDeleteClose()
    await _deleteCustomer(customer._id)
    router.replace({
      pathname: `/dashboard/customer`,
      query: { tab: 217 },
    })
  }

  const loadData = useCallback(async () => {
    if (isAuthenticated && user && authLoading === false) {
      setLoading(true)

      const options = {
        page: 1,
        limit: 10,
        callback: () => {},
      }
      await Promise.all([
        _getCustomer(router.query.id, undefined),
        _getCustomerLaundries(router.query?.email, undefined, options),
      ])

      setLoading(false)
    }
  }, [isAuthenticated, user, authLoading, router.query])

  useEffect(() => {
    router.query.id && router.query.email && loadData()

    return () => {
      _resetCustomerPage()
    }
  }, [isAuthenticated, user, authLoading, router.query])

  return (
    <>
      <Head>
        <title>Dashboard | Customers</title>
      </Head>
      <CustomerModal type='edit' onClose={onEditClose} isOpen={isEditOpen} />
      <AlertDialog
        isOpen={isDeleteOpen}
        onClose={onDeleteClose}
        type='delete'
        heading='Delete Customer'
        body='Are you sure?'
        onDeleteClick={onDelete}
      />
      <LaundryModal onClose={onAddLaundryClose} isOpen={isAddLaundryOpen} />
      <HStack>
        <SideBar />
        <Box height='100vh' width='75%' position='relative'>
          <NoAuth />
          {authLoading === false && user && isAuthenticated && (
            <Box>
              {loading && (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                >
                  <Spinner size='xl' />
                </Box>
              )}
              {!loading && customer._id === '' ? (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                  textAlign='center'
                >
                  <Image src='/nouser.png' w='75%' />
                  <Text fontWeight='bold'>No Customer found</Text>
                </Box>
              ) : (
                <VStack padding='1rem' h='100vh' alignItems='stretch'>
                  <Box mb='1.5rem'>
                    <HStack spacing={4} mb='0.7rem'>
                      <Avatar name={customer.name} size='xl' />
                      <Box>
                        <Text fontWeight='bold' fontSize='1.1rem'>
                          {customer.name}
                        </Text>
                        <Text fontSize='0.9rem'>{customer.email}</Text>
                      </Box>
                      <HStack>
                        <IconButton
                          aria-label='edit-customer'
                          icon={<MdEdit />}
                          colorScheme='blue'
                          variant='ghost'
                          _focus={{ outline: 'none' }}
                          onClick={onEditOpen}
                        />
                        <IconButton
                          aria-label='delete-customer'
                          icon={<AiFillDelete />}
                          colorScheme='red'
                          variant='ghost'
                          _focus={{ outline: 'none' }}
                          onClick={onDeleteOpen}
                        />
                      </HStack>
                    </HStack>
                    <Text fontSize='0.9rem'>
                      Points: {floor(customer.points, 1)}
                    </Text>
                    <Text fontSize='0.9rem'>Phone: {customer.phone}</Text>
                    <Text fontSize='0.9rem'>Address: {customer.address}</Text>
                  </Box>
                  <Box position='relative' flex='3'>
                    <HStack pr='1rem' justifyContent='space-between'>
                      <Box>
                        <HStack spacing={6}>
                          <Text fontWeight='bold' fontSize='1.1rem'>
                            Customer's Laundries
                          </Text>
                          <Button
                            size='sm'
                            colorScheme='blue'
                            onClick={onAddLaundryOpen}
                          >
                            Add Laundry
                          </Button>
                        </HStack>
                      </Box>
                      <Box>
                        {customerLaundries.totalCount > 10 && (
                          <Pagination
                            setPage={setPage}
                            count={customerLaundries.totalCount}
                            itemsPerPage={10}
                          />
                        )}
                      </Box>
                    </HStack>
                    <Box mt='1.5rem'>
                      {!loading && customerLaundries.data.length === 0 ? (
                        <Box
                          width='40%'
                          position='absolute'
                          top='50%'
                          left='50%'
                          transform='translate(-50%, -50%)'
                          textAlign='center'
                        >
                          <Image src='/emptyCart.png' />
                          <Text fontWeight='bold'>No Laundry</Text>
                        </Box>
                      ) : (
                        <Table variant='simple' size='sm'>
                          <Thead>
                            <Tr>
                              <Th>Service</Th>
                              <Th>Quantity</Th>
                              <Th>Type</Th>
                              <Th>Address</Th>
                              <Th>Progress</Th>
                              <Th>Total</Th>
                              <Th>Paid</Th>
                              <Th>Date</Th>
                            </Tr>
                          </Thead>
                          <Tbody>
                            {customerLaundries.data.map(laundry => (
                              <Tr
                                key={laundry._id}
                                _hover={{
                                  cursor: 'pointer',
                                  background: 'gray.50',
                                }}
                                transition='background ease 300ms'
                                onClick={() =>
                                  router.push({
                                    pathname: `/dashboard/laundry/${laundry._id}`,
                                    query: { tab: 212 },
                                  })
                                }
                              >
                                <Td>{laundry.service}</Td>
                                <Td>{laundry.serviceQty}</Td>
                                <Td>
                                  {laundry.serviceType === EServiceType.delivery
                                    ? 'Delivery'
                                    : laundry.serviceType ===
                                      EServiceType.dropoff
                                    ? 'Drop-off'
                                    : 'Self Service'}
                                </Td>
                                <Td>{laundry.location}</Td>
                                <Td>
                                  <Badge
                                    fontSize='10px'
                                    colorScheme={
                                      laundry.progress === EProgress.pending
                                        ? 'yellow'
                                        : laundry.progress === EProgress.washing
                                        ? 'blue'
                                        : laundry.progress ===
                                          EProgress.delivered
                                        ? 'green'
                                        : 'red'
                                    }
                                  >
                                    {laundry.progress}
                                  </Badge>
                                </Td>
                                <Td>
                                  {UtilityService.formatPrice(laundry.total)}
                                </Td>
                                <Td>
                                  {laundry.isPaid ? (
                                    <Badge fontSize='10px' colorScheme='green'>
                                      paid
                                    </Badge>
                                  ) : (
                                    <Badge fontSize='10px' colorScheme='yellow'>
                                      not paid
                                    </Badge>
                                  )}
                                </Td>
                                <Td
                                  color='gray.600'
                                  fontSize='xs'
                                  fontStyle='italic'
                                >
                                  {UtilityService.dateFromNow(
                                    laundry.createdAt,
                                  )}
                                </Td>
                              </Tr>
                            ))}
                          </Tbody>
                        </Table>
                      )}
                    </Box>
                  </Box>
                </VStack>
              )}
            </Box>
          )}
        </Box>
      </HStack>
    </>
  )
}

export default customer
