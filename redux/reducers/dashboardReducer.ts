import { EServiceType } from '../../utils/generics'
import { ActionTypes, Action } from '../actions/types'

type AppState = {
  laundries: {
    totalCount: number
    count: number
    data: {
      serviceQty: number
      iron: boolean
      detergent: boolean
      detergentQty: number
      laundryBag: boolean
      laundryBagQty: number
      extraCareOpt: { _id: string; product: string; quantity: number }[]
      isPaid: boolean
      isDelivered: boolean
      isPicked: boolean
      paymentMethod: string
      progress: string
      laundryDetails: { _id: string; fabric: string; count: number }[]
      _id: string
      service: 'Dry' | 'Wash And Dry'
      email: string
      user: {
        name: string
        phone: number
        photo: string
        address: string
        email: string
      }
      serviceType: EServiceType
      branchLocation: string
      location: string
      deliveredAt: string
      pickedAt: string
      subTotal: number
      total: number
      createdAt: string
      updatedAt: string
      paidAt: string
    }[]
  }

  customerLaundries: {
    totalCount: number
    count: number
    data: {
      serviceQty: number
      iron: boolean
      detergent: boolean
      detergentQty: number
      laundryBag: boolean
      laundryBagQty: number
      extraCareOpt: { _id: string; product: string; quantity: number }[]
      isPaid: boolean
      isDelivered: boolean
      isPicked: boolean
      paymentMethod: string
      progress: string
      laundryDetails: { _id: string; fabric: string; count: number }[]
      totalCount: number
      _id: string
      service: 'Dry' | 'Wash And Dry'
      email: string
      user: {
        name: string
        phone: number
        photo: string
        address: string
        email: string
      }
      serviceType: EServiceType
      branchLocation: string
      location: string
      deliveredAt: string
      pickedAt: string
      subTotal: number
      total: number
      createdAt: string
      updatedAt: string
      paidAt: string
    }[]
  }

  laundry: {
    serviceQty: number
    iron: boolean
    detergent: boolean
    detergentQty: number
    laundryBag: boolean
    laundryBagQty: number
    extraCareOpt: { _id: string; product: string; quantity: number }[]
    isPaid: boolean
    isDelivered: boolean
    isPicked: boolean
    laundryDetails: { _id: string; fabric: string; count: number }[]
    totalCount: number
    paymentMethod: string
    email: string
    user: {
      name: string
      phone: number
      photo: string
      address: string
      email: string
    }
    progress: string
    _id: string
    service: string
    serviceType: string
    branchLocation: string
    location: string
    deliveredAt: string
    pickedAt: string
    subTotal: number
    total: number
    createdAt: string
    updatedAt: string
    paidAt: string
  }

  prices: {
    _id: string
    washDrySelf: number
    drySelf: number
    washDryDropoff: number
    dryDropOff: number
    iron: number
    ironing: { _id: string; name: string; key: string; price: number }[]
    detergent: number
    bleach: number
    softner: number
    stainRemover: number
    delivery: number
    laundryBag: number
    createdAt: string
    updatedAt: string
  }[]

  users: {
    totalCount: number
    count: number
    data: {
      verified: boolean
      role: string
      gender: 'male' | 'female' | ''
      active: boolean
      address: string
      photo: string
      phone: number
      _id: string
      name: string
      email: string
      createdAt: string
      updatedAt: string
    }[]
  }

  user: {
    verified: boolean
    role: string
    gender: string
    active: boolean
    address: string
    photo: string
    phone: number
    _id: string
    name: string
    email: string
    createdAt: string
    updatedAt: string
  }

  messages: {
    totalCount: number
    count: number
    data: {
      _id: string
      name: string
      email: string
      message: string
      createdAt: string
    }[]
  }

  message: {
    _id: string
    name: string
    email: string
    message: string
    createdAt: string
  }

  customers: {
    totalCount: number
    count: number
    data: {
      _id: string
      name: string
      phone: number
      address: string
      email: string
      points: number
      createdAt: string
    }[]
  }

  customer: {
    _id: string
    name: string
    phone: number
    address: string
    email: string
    points: number
    createdAt: string
  }

  cloths: {
    _id: string
    cloth: string
    key: string
  }[]

  settings: {
    totalCount: number
    count: number
    data: {
      branches: string[]
      attendant: string[]
      dispatch: string[]
      manager: string[]
      _id: string
      createdAt: string
      updatedAt: string
    }[]
  }
}

const initialState = {
  laundries: {
    totalCount: 0,
    count: 0,
    data: [],
  },

  customerLaundries: {
    totalCount: 0,
    count: 0,
    data: [],
  },

  isBoarded: null,

  laundry: {
    serviceQty: 0,
    iron: false,
    detergent: false,
    detergentQty: 0,
    laundryBag: false,
    laundryBagQty: 0,
    extraCareOpt: [],
    isPaid: false,
    isDelivered: false,
    isPicked: false,
    paymentMethod: '',
    laundryDetails: [],
    totalCount: 0,
    progress: '',
    _id: '',
    service: '',
    email: '',
    user: {
      name: '',
      phone: 0,
      photo: '',
      address: '',
      email: '',
    },
    serviceType: '',
    branchLocation: '',
    location: '',
    deliveredAt: '',
    pickedAt: '',
    subTotal: 0,
    total: 0,
    createdAt: '',
    updatedAt: '',
    paidAt: '',
  },

  prices: [],

  newLaundry: {
    id: null,
    service: null,
    serviceType: null,
    email: null,
    serviceQty: 0,
    iron: null,
    detergent: null,
    detergentName: null,
    detergentQty: 0,
    laundryBag: null,
    laundryBagQty: 0,
    laundryDetails: null,
    extraCareOpt: [],
    branchLocation: null,
    location: null,
    isPaid: null,
    isDelivered: null,
    deliveredAt: null,
    isPicked: null,
    pickedAt: null,
    paymentMethod: null,
    progress: null,
    subTotal: 0,
    total: 0,
  },

  users: {
    totalCount: 0,
    count: 0,
    data: [],
  },

  user: {
    verified: false,
    role: '',
    gender: '',
    active: false,
    address: '',
    photo: '',
    phone: 0,
    _id: '',
    name: '',
    email: '',
    createdAt: '',
    updatedAt: '',
  },

  messages: { totalCount: 0, count: 0, data: [] },

  message: {
    _id: '',
    name: '',
    email: '',
    message: '',
    createdAt: '',
  },

  customers: { totalCount: 0, count: 0, data: [] },

  customer: {
    _id: '',
    name: '',
    phone: 0,
    address: '',
    email: '',
    points: 0,
    createdAt: '',
  },

  cloths: [],

  settings: {
    totalCount: 0,
    count: 0,
    data: [],
  },
}

const dashboardReducer = (
  state: AppState = initialState,
  action: Action,
): AppState => {
  switch (action.type) {
    case ActionTypes.dashboardLaundries:
      return {
        ...state,
        laundries: action.payload,
      }

    case ActionTypes.dashboardLaundry:
      return {
        ...state,
        laundry: action.payload,
      }

    case ActionTypes.dashboardUsers:
      return {
        ...state,
        users: action.payload,
      }

    case ActionTypes.dashboardUser:
      return {
        ...state,
        user: action.payload,
      }

    case ActionTypes.dashboardUserUpdate:
      return {
        ...state,
        user: action.payload,
      }

    case ActionTypes.dashboardUpdateLaundry:
      return {
        ...state,
        laundry: action.payload,
      }

    case ActionTypes.dashboardMessages:
      return {
        ...state,
        messages: action.payload,
      }

    case ActionTypes.dashboardDeleteMessages:
      return {
        ...state,
        messages: { totalCount: 0, count: 0, data: [] },
      }

    case ActionTypes.dashboardCustomers:
      return {
        ...state,
        customers: action.payload,
      }

    case ActionTypes.dashboardCreateCustomers:
      return {
        ...state,
        customers: action.payload,
      }

    case ActionTypes.dashboardUpdateCustomer:
      return {
        ...state,
        customer: action.payload,
      }

    case ActionTypes.dashboardCustomer:
      return {
        ...state,
        customer: action.payload,
      }

    case ActionTypes.dashboardCustomerLaundries:
      return {
        ...state,
        customerLaundries: action.payload,
      }

    case ActionTypes.dashboardDeleteCustomer:
      return {
        ...state,
        customer: {
          _id: '',
          name: '',
          phone: 0,
          address: '',
          email: '',
          points: 0,
          createdAt: '',
        },
      }

    case ActionTypes.dashboardResetCustomerPage:
      return {
        ...state,
        customerLaundries: initialState.customerLaundries,
        customer: initialState.customer,
      }

    case ActionTypes.dashboardCloths:
      return {
        ...state,
        cloths: action.payload,
      }

    case ActionTypes.dashboardUpdatePrices:
      return {
        ...state,
        prices: action.payload,
      }

    case ActionTypes.dashboardSettings:
      return {
        ...state,
        settings: action.payload,
      }

    default:
      return state
  }
}

export default dashboardReducer
