import { iwad, TRole } from './../../utils/generics'
import Cookies from 'universal-cookie'
import { Action, ActionTypes } from '../actions/types'

type AuthState = {
  isAuthenticated: boolean | null
  authLoading: boolean
  user: {
    verified: boolean
    role: TRole
    gender: 'male' | 'female' | ''
    active: boolean
    address: string
    photo: string
    phone: number
    _id: string
    name: string
    email: string
    createdAt: string
    updatedAt: string
  } | null
}

const initialState = {
  isAuthenticated: null,
  authLoading: false,
  user: null,
}

const cookies = new Cookies()

const authReducer = (
  state: AuthState = initialState,
  action: Action,
): AuthState => {
  switch (action.type) {
    case ActionTypes.auth:
      return {
        ...state,
        isAuthenticated: true,
      }

    case ActionTypes.authError:
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      }

    case ActionTypes.authLoading:
      return {
        ...state,
        authLoading: action.payload,
      }

    case ActionTypes.profile:
      return {
        ...state,
        user: action.payload,
      }

    case ActionTypes.logout:
      cookies.remove('iwad', { path: '/' })
      return {
        ...state,
        user: null,
        isAuthenticated: null,
      }

    default:
      return state
  }
}

export default authReducer
