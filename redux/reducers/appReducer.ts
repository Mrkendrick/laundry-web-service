import { imageURL } from './../../utils/api'
import { EServiceType } from '../../utils/generics'
import { ActionTypes, Action } from '../actions/types'

type AppState = {
  isBoarded: boolean | null
  isMobile: boolean

  laundries: {
    serviceQty: number
    iron: boolean
    detergent: boolean
    detergentQty: number
    laundryBag: boolean
    laundryBagQty: number
    extraCareOpt: { _id: string; product: string; quantity: number }[]
    isPaid: boolean
    isDelivered: boolean
    isPicked: boolean
    laundryDetails: { _id: string; fabric: string; count: number }[]
    totalCount: number
    paymentMethod: string
    email: string
    user: {
      name: string
      phone: number
      photo: string
      address: string
      email: string
    }
    progress: string
    _id: string
    service: string
    serviceType: string
    branchLocation: string
    location: string
    deliveredAt: string
    pickedAt: string
    subTotal: number
    total: number
    createdAt: string
    updatedAt: string
    paidAt: string
  }[]

  laundry: {
    serviceQty: number
    iron: boolean
    detergent: boolean
    detergentQty: number
    laundryBag: boolean
    laundryBagQty: number
    extraCareOpt: { _id: string; product: string; quantity: number }[]
    isPaid: boolean
    isDelivered: boolean
    isPicked: boolean
    laundryDetails: { _id: string; fabric: string; count: number }[]
    totalCount: number
    paymentMethod: string
    email: string
    user: {
      name: string
      phone: number
      photo: string
      address: string
      email: string
    }
    progress: string
    _id: string
    service: string
    serviceType: string
    branchLocation: string
    location: string
    deliveredAt: string
    pickedAt: string
    subTotal: number
    total: number
    createdAt: string
    updatedAt: string
    paidAt: string
  }

  prices: {
    _id: string
    washDrySelf: number
    drySelf: number
    washDryDropoff: number
    dryDropOff: number
    iron: number
    ironDropoff: number
    ironing: { _id: string; name: string; key: string; price: number }[]
    detergent: number
    bleach: number
    softner: number
    stainRemover: number
    delivery: number
    laundryBag: number
    createdAt: string
    updatedAt: string
  }[]

  newLaundry: {
    service: 'Dry' | 'Wash And Dry' | 'Iron' | null
    id: string | null
    email: string | null
    serviceType: EServiceType | null
    serviceQty: number | 0
    iron: boolean | null
    detergent: boolean | null
    detergentName: string | null
    detergentQty: number | 0
    user: {
      email: string
      name: string
      phone: number
      address: string
    }
    laundryBag: boolean | null
    laundryDetails: { _id: string; fabric: string; count: number }[]
    laundryBagQty: number | 0
    extraCareOpt: string[]
    branchLocation: string | null
    location: string | null
    isPaid: boolean | null
    isDelivered: boolean | null
    deliveredAt: string | null
    isPicked: boolean | null
    pickedAt: string | null
    paymentMethod: 'cash' | 'pos' | 'transfer' | 'card' | null
    progress: 'pending' | 'washing' | 'delivered' | 'cancelled' | null
    subTotal: number | 0
    total: number | 0
  }

  notifications: {
    _id: string
    user: string
    read: boolean
    message: string
    createdAt: string
  }[]
}

const initialState = {
  laundries: [],
  isBoarded: null,
  isMobile: false,
  notifications: [],

  laundry: {
    serviceQty: 0,
    iron: false,
    detergent: false,
    detergentQty: 0,
    laundryBag: false,
    laundryBagQty: 0,
    extraCareOpt: [],
    isPaid: false,
    isDelivered: false,
    isPicked: false,
    paymentMethod: '',
    laundryDetails: [],
    totalCount: 0,
    progress: '',
    _id: '',
    service: '',
    email: '',
    user: {
      name: '',
      phone: 0,
      photo: '',
      address: '',
      email: '',
    },
    serviceType: '',
    branchLocation: '',
    location: '',
    deliveredAt: '',
    pickedAt: '',
    subTotal: 0,
    total: 0,
    createdAt: '',
    updatedAt: '',
    paidAt: '',
  },

  prices: [],

  newLaundry: {
    id: null,
    service: null,
    serviceType: null,
    email: null,
    serviceQty: 0,
    iron: null,
    detergent: null,
    detergentName: null,
    detergentQty: 0,
    laundryBag: null,
    laundryBagQty: 0,
    laundryDetails: [],
    user: {
      email: '',
      name: '',
      phone: 0,
      address: '',
    },
    extraCareOpt: [],
    branchLocation: null,
    location: null,
    isPaid: null,
    isDelivered: null,
    deliveredAt: null,
    isPicked: null,
    pickedAt: null,
    paymentMethod: null,
    progress: null,
    subTotal: 0,
    total: 0,
  },
}

const appReducer = (
  state: AppState = initialState,
  action: Action,
): AppState => {
  switch (action.type) {
    case ActionTypes.laundries:
      return {
        ...state,
        laundries: action.payload,
      }

    case ActionTypes.laundry:
      return {
        ...state,
        laundry: action.payload,
      }

    case ActionTypes.updateLaundry:
      return {
        ...state,
        laundry: action.payload,
        laundries: state.laundries.map(laundry =>
          laundry._id === action.payload._id ? action.payload : laundry,
        ),
      }

    case ActionTypes.prices:
      return {
        ...state,
        prices: action.payload,
      }

    case ActionTypes.newLaundry:
      return {
        ...state,
        newLaundry: action.payload,
      }

    case ActionTypes.updateNewLaundry:
      return {
        ...state,
        newLaundry: { ...state.newLaundry, ...action.payload },
      }

    case ActionTypes.resetNewLaundry:
      return {
        ...state,
        newLaundry: initialState.newLaundry,
      }

    case ActionTypes.notifications:
      return {
        ...state,
        notifications: action.payload,
      }

    case ActionTypes.onBoarding:
      return {
        ...state,
        isBoarded: action.payload,
      }

    case ActionTypes.isMobile:
      return {
        ...state,
        isMobile: action.payload,
      }

    case ActionTypes.logout:
      return {
        ...state,
      }

    default:
      return state
  }
}

export default appReducer
