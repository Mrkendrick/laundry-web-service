import { combineReducers } from 'redux'
import appReducer from './appReducer'
import authReducer from './authReducer'
import dashboardReducer from './dashboardReducer'

const reducers = combineReducers({
  appReducer,
  authReducer,
  dashboardReducer,
})

export type RootState = ReturnType<typeof reducers>

export default reducers
