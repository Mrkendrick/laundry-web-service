import { Dispatch } from 'redux'
import Cookies from 'universal-cookie'
import UtilityService from '../../utils/utils'
import APIService from '../../utils/api'
import { ActionTypes, Action } from './types'
import { iwad } from '../../utils/generics'
import { AxiosError } from 'axios'

const _logout = () => (dispatch: Dispatch<Action>) => {
  dispatch({
    type: ActionTypes.logout,
  })
}

const _login =
  (
    data: { email: string; password: string },
    setLoading?: (x: boolean) => void,
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.login(data)

      const cookies = new Cookies()
      cookies.set(iwad, res.data.token, { path: '/' })

      dispatch({ type: ActionTypes.auth })

      setLoading && setLoading(false)
    } catch (error: any) {
      setLoading && setLoading(false)

      dispatch({ type: ActionTypes.authError })

      if (error?.message === 'Network Error') {
        UtilityService.toast({
          title: 'You are offline',
          description: 'Please check your connection',
          status: 'error',
        })
      } else if (error?.response?.data?.message === 'Invalid Credentials') {
        UtilityService.toast({
          title: 'Invalid Credentials',
          description: 'Please check your email and password',
          status: 'error',
        })
      } else {
        UtilityService.toast({
          title: 'Something went wrong!',
          status: 'error',
        })
      }
    }
  }

const _signup =
  (
    data: {
      name: string
      email: string
      password: string
      passwordConfirm: string
    },
    setLoading?: (x: boolean) => void,
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.signup(data)

      const cookies = new Cookies()
      cookies.set(iwad, res.data.token, { path: '/' })

      dispatch({ type: ActionTypes.auth })

      setLoading && setLoading(false)
    } catch (error: any) {
      setLoading && setLoading(false)

      dispatch({ type: ActionTypes.authError })

      if (error?.message === 'Network Error') {
        UtilityService.toast({
          title: 'You are offline',
          description: 'Please check your connection',
          status: 'error',
        })
      } else if (error?.response?.data?.message === 'User already exists.') {
        UtilityService.toast({
          title: 'User already exists.',
          description: 'Login with your credentials',
          status: 'error',
        })
      } else {
        UtilityService.toast({
          title: 'Something went wrong!',
          status: 'error',
        })
      }
    }
  }

const _forgotPassword =
  (data: { email: string }, setLoading?: (x: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      await APIService.forgotPassword(data)

      setLoading && setLoading(false)

      UtilityService.toast({
        title: 'Recovery link sent',
        description: 'Please check your email',
        status: 'info',
      })
    } catch (error: any) {
      setLoading && setLoading(false)

      if (error?.message === 'Network Error') {
        UtilityService.toast({
          title: 'You are offline',
          description: 'Please check your connection',
          status: 'error',
        })
      } else {
        UtilityService.toast({
          title: 'Something went wrong!',
          status: 'error',
        })
      }
    }
  }

const _resetPassword =
  (
    resetToken: string,
    data: { password: string; passwordConfirm: string },
    setLoading?: (condition: boolean) => void,
    callback?: () => void,
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.resetPassword(resetToken, data)

      const cookies = new Cookies()
      cookies.set(iwad, res.data.token, { path: '/' })

      dispatch({ type: ActionTypes.auth })

      setLoading && setLoading(false)
      callback && callback()
    } catch (error: any) {
      setLoading && setLoading(false)

      if (error?.message === 'Network Error') {
        UtilityService.toast({
          title: 'You are offline',
          description: 'Please check your connection',
          status: 'error',
        })
      } else {
        UtilityService.toast({
          title: 'Something went wrong!',
          status: 'error',
        })
      }
    }
  }

const _sendMessage =
  (
    data: any,
    setLoading?: (condition: boolean) => void,
    callback?: (type: 'success' | 'error') => void,
  ) =>
  async () => {
    try {
      await APIService.sendMessage(data)

      setLoading && setLoading(false)
      callback && callback('success')
    } catch (error) {
      setLoading && setLoading(false)
      callback && callback('error')
    }
  }

const _getMe = () => async (dispatch: Dispatch<Action>) => {
  try {
    dispatch({ type: ActionTypes.authLoading, payload: true })
    const res = await APIService.getProfile()

    dispatch({ type: ActionTypes.profile, payload: res.data.data })
    dispatch({ type: ActionTypes.auth })
    dispatch({ type: ActionTypes.authLoading, payload: false })
  } catch (error) {
    dispatch({ type: ActionTypes.authError })
    dispatch({ type: ActionTypes.authLoading, payload: false })
  }
}

const _updateProfile =
  (
    data: any,
    isAdmin?: boolean,
    setLoading?: (x: boolean) => void,
    callback?: () => void,
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      let imageURL
      let updatedData

      /* if (data.upload.isReady) {
          const res = await APIService.imageUpload(data.upload)
          // imageURL = res.data.secure_url
        } */

      if (imageURL) {
        delete data['upload']
        updatedData = {
          ...data,
          photo: imageURL,
        }
      } else {
        delete data['upload']
        updatedData = {
          ...data,
        }
      }

      const res = isAdmin
        ? await APIService.updateProfile(data)
        : await APIService.updateProfile(updatedData)

      isAdmin
        ? dispatch({
            type: ActionTypes.dashboardUserUpdate,
            payload: res.data.data,
          })
        : dispatch({ type: ActionTypes.profile, payload: res.data.data })

      setLoading && setLoading(false)
      callback && callback()
    } catch (error) {
      setLoading && setLoading(false)
    }
  }

const _getLaundries =
  (
    query?: string,
    role?: string,
    options?: { page: number; limit: number; callback(): void },
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getLaundries(query, options)

      if (!role || role === 'user')
        dispatch({
          type: ActionTypes.laundries,
          payload: res.data.data.reverse(),
        })

      if (role !== 'user')
        dispatch({
          type: ActionTypes.dashboardLaundries,
          payload: res.data,
        })

      options?.callback && options.callback()
    } catch (error) {
      options?.callback && options.callback()
    }
  }

const _getCustomerLaundries =
  (
    query?: string | string[],
    setLoading?: (condition: boolean) => void,
    options?: { page: number; limit: number; callback(): void },
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getLaundries(query, options)

      dispatch({
        type: ActionTypes.dashboardCustomerLaundries,
        payload: res.data,
      })

      options?.callback && options.callback()
      setLoading && setLoading(false)
    } catch (error) {
      options?.callback && options.callback()
      setLoading && setLoading(false)
    }
  }

const _getPrices =
  (setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getPrices()

      dispatch({ type: ActionTypes.prices, payload: res.data.data })
      setLoading && setLoading(false)
    } catch (error) {
      setLoading && setLoading(false)
    }
  }

const _createLaundry =
  (type: 'save' | 'delete' | 'get', data?: any) =>
  (dispatch: Dispatch<Action>) => {
    const session = window.sessionStorage

    switch (type) {
      case 'save':
        session.setItem(iwad, JSON.stringify(data))
        break

      case 'get':
        const storageData = session.getItem(iwad)
        storageData &&
          dispatch({
            type: ActionTypes.newLaundry,
            payload: JSON.parse(storageData),
          })
        break

      case 'delete':
        session.removeItem(iwad)

      default:
        break
    }
  }

const _createNewLaundry =
  (data: any, role?: string, setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.createLaundry(data)

      !role ||
        (role === 'user' &&
          dispatch({ type: ActionTypes.laundry, payload: res.data.data }))

      role !== 'user' &&
        dispatch({ type: ActionTypes.dashboardLaundry, payload: res.data.data })

      setLoading && setLoading(false)
    } catch (error) {
      setLoading && setLoading(false)
    }
  }

const _getLaundry =
  (id: string, role?: string, setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getLaundry(id)

      !role ||
        (role === 'user' &&
          dispatch({ type: ActionTypes.laundry, payload: res.data.data }))

      role !== 'user' &&
        dispatch({ type: ActionTypes.dashboardLaundry, payload: res.data.data })

      setLoading && setLoading(false)
    } catch (error) {
      setLoading && setLoading(false)
    }
  }

const _updateLaundry =
  (
    id: string,
    data: any,
    role?: string,
    setLoading?: (condition: boolean) => void,
  ) =>
  async (dispatch: Dispatch<Action>) => {
    console.log(data)
    try {
      const res = await APIService.updateLaundry(id, data)

      !role ||
        (role === 'user' &&
          dispatch({ type: ActionTypes.updateLaundry, payload: res.data.data }))

      if (role !== 'user') {
        dispatch({
          type: ActionTypes.dashboardUpdateLaundry,
          payload: res.data.data,
        })

        UtilityService.toast({
          title: 'Laundry updated successfully',
          status: 'info',
        })
      }

      setLoading && setLoading(false)
    } catch (error: any) {
      console.log(error.message)
      UtilityService.toast({
        title: 'Ops! Something went wrong',
        status: 'warning',
      })
      setLoading && setLoading(false)
    }
  }

const _updateNewLaundry = (data: any) => (dispatch: Dispatch<Action>) => {
  dispatch({ type: ActionTypes.updateNewLaundry, payload: data })
}

const _resetNewLaundry = () => (dispatch: Dispatch<Action>) => {
  dispatch({ type: ActionTypes.resetNewLaundry })
}

const _isOnBoarded =
  (type: 'check' | 'set') => (dispatch: Dispatch<Action>) => {
    const cookies = new Cookies()

    if (type === 'check') {
      const isNew = cookies.get('onBoarded', {})

      isNew
        ? dispatch({ type: ActionTypes.onBoarding, payload: true })
        : dispatch({ type: ActionTypes.onBoarding, payload: false })
    }

    if (type === 'set') {
      cookies.set('onBoarded', true, { path: '/' })
      dispatch({ type: ActionTypes.onBoarding, payload: true })
    }
  }

const _isMobile = (isMobile?: boolean) => (dispatch: Dispatch<Action>) => {
  dispatch({ type: ActionTypes.isMobile, payload: isMobile ? isMobile : false })
}

const _getUsers =
  (
    query?: string,
    setLoading?: (condition: boolean) => void,
    options?: { page: number; limit: number; callback(): void },
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getUsers(query, options)

      dispatch({ type: ActionTypes.dashboardUsers, payload: res.data })
      options?.callback && options.callback()
      setLoading && setLoading(false)
    } catch (error) {
      options?.callback && options.callback()
      setLoading && setLoading(false)
    }
  }

const _getUser = (data: any) => async (dispatch: Dispatch<Action>) => {
  dispatch({ type: ActionTypes.dashboardUser, payload: data })
}

const _usersByEmail =
  (email: string, setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.usersByEmail(email)

      dispatch({ type: ActionTypes.dashboardUsers, payload: res.data })
      setLoading && setLoading(false)
    } catch (error) {
      setLoading && setLoading(false)
    }
  }

const _sendNotification =
  (data: any, setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      await APIService.sendNotification(data)

      UtilityService.toast({ title: 'Push notification sent', status: 'info' })
      setLoading && setLoading(false)
    } catch (error) {
      setLoading && setLoading(false)
      UtilityService.toast({ title: 'Something Went Wrong', status: 'error' })
    }
  }

const _getNotifications =
  (setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getNotifications()

      dispatch({ type: ActionTypes.notifications, payload: res.data.data })
      setLoading && setLoading(false)
    } catch (error) {
      setLoading && setLoading(false)
    }
  }

const _getMessages =
  (
    setLoading?: (condition: boolean) => void,
    options?: { page: number; limit: number; callback(): void },
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getMessages(options)

      dispatch({ type: ActionTypes.dashboardMessages, payload: res.data })
      options?.callback()
      setLoading && setLoading(false)
    } catch (error) {
      options?.callback()
      setLoading && setLoading(false)
    }
  }

const _getMessage =
  (id: string, setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getMessage(id)

      dispatch({ type: ActionTypes.dashboardMessage, payload: res.data.data })
      setLoading && setLoading(false)
    } catch (error) {
      setLoading && setLoading(false)
    }
  }

const _deleteMessages = () => async (dispatch: Dispatch<Action>) => {
  try {
    await APIService.deleteMessages()

    dispatch({ type: ActionTypes.dashboardDeleteMessages })
  } catch (error) {
    UtilityService.toast({ title: 'Something Went Wrong', status: 'error' })
  }
}

const _getCustomers =
  (
    query?: string,
    setLoading?: (condition: boolean) => void,
    options?: { page: number; limit: number; callback(): void },
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = query
        ? await APIService.searchCustomer(query, options)
        : await APIService.getCustomers(options)

      dispatch({ type: ActionTypes.dashboardCustomers, payload: res.data })
      setLoading && setLoading(false)
      options?.callback()
    } catch (error) {
      setLoading && setLoading(false)
      options?.callback()
    }
  }

const _getCustomer =
  (id?: string | string[], setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getCustomer(id)

      dispatch({ type: ActionTypes.dashboardCustomer, payload: res.data.data })
      setLoading && setLoading(false)
    } catch (error) {
      setLoading && setLoading(false)
    }
  }

const _deleteCustomer = (id: string) => async (dispatch: Dispatch<Action>) => {
  try {
    await APIService.deleteCustomer(id)

    dispatch({ type: ActionTypes.dashboardDeleteCustomer })

    UtilityService.toast({
      title: 'Customer deleted',
      status: 'info',
    })
  } catch (error: any) {
    if (error?.message === 'Network Error') {
      UtilityService.toast({
        title: 'You are offline',
        description: 'Please check your connection',
        status: 'error',
      })
    } else {
      UtilityService.toast({
        title: 'Something went wrong!',
        status: 'error',
      })
    }
  }
}

const _createCustomer =
  (
    data: any,
    setLoading?: (condition: boolean) => void,
    callback?: () => void,
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      await APIService.createCustomer(data)

      const res = await APIService.getCustomers()

      dispatch({
        type: ActionTypes.dashboardCreateCustomers,
        payload: res.data,
      })
      setLoading && setLoading(false)
      callback && callback()
      UtilityService.toast({
        title: 'Customer added successfully',
        status: 'info',
      })
    } catch (error: any) {
      setLoading && setLoading(false)

      if (error?.message === 'Network Error') {
        UtilityService.toast({
          title: 'You are offline',
          description: 'Please check your connection',
          status: 'error',
        })
      } else if (error?.response?.data?.message === 'Customer already exists') {
        UtilityService.toast({
          title: 'Customer already exists',
          description: 'Search by name or use another email',
          status: 'error',
        })
      } else {
        UtilityService.toast({
          title: 'Something went wrong!',
          status: 'error',
        })
      }
    }
  }

const _updateCustomer =
  (
    data: any,
    setLoading?: (condition: boolean) => void,
    callback?: () => void,
  ) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.updateCustomer(data.id, data)

      dispatch({
        type: ActionTypes.dashboardUpdateCustomer,
        payload: res.data.data,
      })
      setLoading && setLoading(false)
      callback && callback()

      UtilityService.toast({
        title: 'Customer updated successfully',
        status: 'info',
      })
    } catch (error: any) {
      setLoading && setLoading(false)

      if (error?.message === 'Network Error') {
        UtilityService.toast({
          title: 'You are offline',
          description: 'Please check your connection',
          status: 'error',
        })
      } else {
        UtilityService.toast({
          title: 'Something went wrong!',
          status: 'error',
        })
      }
    }
  }

const _resetCustomerPage = () => (dispatch: Dispatch<Action>) => {
  dispatch({ type: ActionTypes.dashboardResetCustomerPage })
}

const _getCloths = () => async (dispatch: Dispatch<Action>) => {
  try {
    const res = await APIService.getCloths()

    dispatch({ type: ActionTypes.dashboardCloths, payload: res.data.data })
  } catch (error) {}
}

const _updatePrices =
  (id: string, data: any, setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.updatePrices(id, data)

      dispatch({
        type: ActionTypes.dashboardUpdatePrices,
        payload: res.data.data,
      })

      setLoading && setLoading(false)
    } catch (error: any) {
      setLoading && setLoading(false)

      if (error?.message === 'Network Error') {
        UtilityService.toast({
          title: 'You are offline',
          description: 'Please check your connection',
          status: 'error',
        })
      } else {
        UtilityService.toast({
          title: 'Something went wrong!',
          status: 'error',
        })
      }
    }
  }

const _getSettings =
  (setLoading?: (condition: boolean) => void) =>
  async (dispatch: Dispatch<Action>) => {
    try {
      const res = await APIService.getSettings()

      setLoading && setLoading(false)
    } catch (error: any) {
      setLoading && setLoading(false)
    }
  }

const appActions = {
  _logout,
  _login,
  _signup,
  _forgotPassword,
  _resetPassword,
  _sendMessage,
  _getMe,
  _updateProfile,
  _getLaundries,
  _getLaundry,
  _updateLaundry,
  _getPrices,
  _createLaundry,
  _updateNewLaundry,
  _resetNewLaundry,
  _createNewLaundry,
  _isOnBoarded,
  _isMobile,
  _getUsers,
  _getUser,
  _usersByEmail,
  _sendNotification,
  _getNotifications,
  _getMessages,
  _getMessage,
  _deleteMessages,
  _getCustomers,
  _createCustomer,
  _getCustomer,
  _getCustomerLaundries,
  _updateCustomer,
  _deleteCustomer,
  _resetCustomerPage,
  _getCloths,
  _updatePrices,
  _getSettings,
}

export default appActions
