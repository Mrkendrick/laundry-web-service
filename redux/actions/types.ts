export enum ActionTypes {
  logout = 'LOGOUT',
  auth = 'AUTH',
  authError = 'AUTH_ERROR',
  authLoading = 'AUTH_LOADING',
  profile = 'PROFILE',
  laundries = 'LAUNDRIES',
  laundry = 'LAUNDRY',
  updateLaundry = 'UPDATE_LAUNDRY',
  prices = 'PRICES',
  newLaundry = 'NEW_LAUNDRY',
  updateNewLaundry = 'UPDATE_NEW_LAUNDRY',
  resetNewLaundry = 'RESET_NEW_LAUNDRY',
  notifications = 'NOTIFICATIONS',
  onBoarding = 'ONBOARDING',
  dashboardLaundries = 'DASHBOARD_LAUNDRIES',
  dashboardLaundry = 'DASHBOARD_LAUNDRY',
  dashboardUpdateLaundry = 'DASHBOARD_UPDATE_LAUNDRY',
  dashboardUser = 'DASHBOARD_USER',
  dashboardUsers = 'DASHBOARD_USERS',
  dashboardUserUpdate = 'DASHBOARD_UPDATE_USER',
  dashboardMessages = 'DASHBOARD_MESSAGES',
  dashboardMessage = 'DASHBOARD_MESSAGE',
  dashboardDeleteMessages = 'DASHBOARD_DELETE_MESSAGES',
  dashboardCustomers = 'DASHBOARD_CUSTOMERS',
  dashboardCreateCustomers = 'DASHBOARD_CREATE_CUSTOMER',
  dashboardUpdateCustomer = 'DASHBOARD_UPDATE_CUSTOMER',
  dashboardCustomer = 'DASHBOARD_CUSTOMER',
  dashboardCustomerLaundries = 'DASHBOARD_CUSTOMER_LAUDRIES',
  dashboardDeleteCustomer = 'DASHBOARD_DELETE_CUSTOMER',
  dashboardResetCustomerPage = 'DASHBOARD_RESET_CUSTOMER_PAGE',
  isMobile = 'IS_MOBILE',
  dashboardCloths = 'DASHBOARD_CLOTHS',
  dashboardUpdatePrices = 'DASHBOARD_UPDATE_PRICES',
  dashboardSettings = 'DASHBOARD_SETTINGS',
}

interface Logout {
  type: ActionTypes.logout
}

interface Auth {
  type: ActionTypes.auth
}

interface AuthError {
  type: ActionTypes.authError
}

interface Profile {
  type: ActionTypes.profile
  payload: any
}

interface Laundries {
  type: ActionTypes.laundries
  payload: any
}

interface Laundry {
  type: ActionTypes.laundry
  payload: any
}

interface UpdateLaundry {
  type: ActionTypes.updateLaundry
  payload: any
}

interface Prices {
  type: ActionTypes.prices
  payload: any
}

interface NewLaundry {
  type: ActionTypes.newLaundry
  payload: any
}

interface UpdateNewLaundry {
  type: ActionTypes.updateNewLaundry
  payload: any
}

interface ResetNewLaundry {
  type: ActionTypes.resetNewLaundry
}

interface AuthLoading {
  type: ActionTypes.authLoading
  payload: boolean
}

interface OnBoarding {
  type: ActionTypes.onBoarding
  payload: boolean
}

interface OnMobile {
  type: ActionTypes.isMobile
  payload: boolean
}

interface DashboardLaundries {
  type: ActionTypes.dashboardLaundries
  payload: any
}

interface DashboardLaundry {
  type: ActionTypes.dashboardLaundry
  payload: any
}

interface DashboardUpdateLaundry {
  type: ActionTypes.dashboardUpdateLaundry
  payload: any
}

interface DashboardUser {
  type: ActionTypes.dashboardUser
  payload: any
}

interface DashboardUsers {
  type: ActionTypes.dashboardUsers
  payload: any
}

interface DashboardUpdateUser {
  type: ActionTypes.dashboardUserUpdate
  payload: any
}

interface Notifications {
  type: ActionTypes.notifications
  payload: any
}

interface DashboardMessages {
  type: ActionTypes.dashboardMessages
  payload: any
}

interface DashboardMessage {
  type: ActionTypes.dashboardMessage
  payload: any
}

interface DashboardDeleteMesaage {
  type: ActionTypes.dashboardDeleteMessages
}

interface DashboardCustomers {
  type: ActionTypes.dashboardCustomers
  payload: any
}

interface DashboardCreateCustomers {
  type: ActionTypes.dashboardCreateCustomers
  payload: any
}

interface DashboardUpdateCustomer {
  type: ActionTypes.dashboardUpdateCustomer
  payload: any
}

interface DashboardCustomer {
  type: ActionTypes.dashboardCustomer
  payload: any
}

interface DashboardCustomerLaundries {
  type: ActionTypes.dashboardCustomerLaundries
  payload: any
}

interface DashboardDeleteCustomer {
  type: ActionTypes.dashboardDeleteCustomer
}

interface DashboardResetCustomerPage {
  type: ActionTypes.dashboardResetCustomerPage
}

interface DashboardCloths {
  type: ActionTypes.dashboardCloths
  payload: any
}

interface DashboardUpdatePrices {
  type: ActionTypes.dashboardUpdatePrices
  payload: any
}

interface DashboardSettings {
  type: ActionTypes.dashboardSettings
  payload: any
}

export type Action =
  | Logout
  | Auth
  | AuthError
  | Profile
  | Laundries
  | Laundry
  | UpdateLaundry
  | Prices
  | NewLaundry
  | UpdateNewLaundry
  | ResetNewLaundry
  | AuthLoading
  | OnBoarding
  | OnMobile
  | DashboardLaundries
  | DashboardLaundry
  | DashboardUpdateLaundry
  | DashboardUser
  | DashboardUsers
  | DashboardUpdateUser
  | Notifications
  | DashboardMessage
  | DashboardMessages
  | DashboardDeleteMesaage
  | DashboardCustomers
  | DashboardCreateCustomers
  | DashboardCustomer
  | DashboardCustomerLaundries
  | DashboardUpdateCustomer
  | DashboardDeleteCustomer
  | DashboardResetCustomerPage
  | DashboardCloths
  | DashboardUpdatePrices
  | DashboardSettings
