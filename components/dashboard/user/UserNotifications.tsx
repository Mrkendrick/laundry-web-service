import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Box, Text, HStack, VStack } from '@chakra-ui/layout'
import { Icon } from '@chakra-ui/icon'
import { BsInfoCircleFill } from 'react-icons/bs'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { Spinner } from '@chakra-ui/spinner'
import UtilityService from '../../../utils/utils'
import ButtonPagination from '../../shared/ButtonPagination'
import { chunk } from 'lodash'

const UserNotifications = () => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [index, setIndex] = useState(0)
  const { notifications } = useSelector((state: RootState) => state.appReducer)
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { _getNotifications } = bindActionCreators(appActions, dispatch)

  const chunkedNotification = useMemo(
    () => chunk(notifications, 3),
    [notifications],
  )

  const onPaginationClick = (type: 'prev' | 'next') => {
    type === 'prev' && setIndex(prev => prev - 1)
    type === 'next' && setIndex(prev => prev + 1)
  }

  const loadData = useCallback(async () => {
    if (isAuthenticated && user && authLoading === false) {
      setLoading(true)
      await _getNotifications(setLoading)
    }
  }, [isAuthenticated, user, authLoading])

  useEffect(() => {
    loadData()
  }, [isAuthenticated, user, authLoading])

  return (
    <Box my='1.5rem' position='relative' h='100%'>
      <HStack justifyContent='space-between' mb='1rem'>
        <Text fontWeight='bold'>Notifications</Text>
        {chunkedNotification.length > 0 && (
          <ButtonPagination
            onClick={onPaginationClick}
            isNextDisabled={chunkedNotification.length - 1 === index}
            isPrevDisabled={index === 0}
          />
        )}
      </HStack>
      {loading || authLoading ? (
        <Box
          position='absolute'
          top='50%'
          left='50%'
          transform='translate(-50%, -50%)'
        >
          <Spinner />
        </Box>
      ) : null}

      {authLoading === false && !loading && chunkedNotification.length === 0 ? (
        <Box mt='2rem'>
          <Text color='gray.600' fontStyle='italic'>
            - No Notifications
          </Text>
        </Box>
      ) : (
        chunkedNotification.length > 0 &&
        chunkedNotification[index].map(notification => (
          <HStack
            spacing={3}
            shadow='md'
            bg='white'
            rounded='2'
            p='0.3rem'
            key={notification._id}
            my='0.7rem'
          >
            <Box h='50px' w='4px' rounded='2' bg='blue.500' />
            <Icon w={5} h={5} as={BsInfoCircleFill} color='blue.500' />
            <VStack>
              <HStack spacing={4} alignSelf='flex-start'>
                <Text fontWeight='bold' fontSize='0.9rem'>
                  Store
                </Text>
                <Text fontSize='xs' color='gray.500' fontStyle='italic'>
                  {UtilityService.dateFromNow(notification.createdAt)}
                </Text>
              </HStack>

              <Box>
                <Text color='gray.800' fontSize='sm'>
                  {notification.message}
                </Text>
              </Box>
            </VStack>
          </HStack>
        ))
      )}
    </Box>
  )
}

export default UserNotifications
