import React, { useState, useEffect } from 'react'
import { Box, Text } from '@chakra-ui/layout'
import { UserLaundryTable } from '../LaundryTable'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { Image } from '@chakra-ui/image'
import { Spinner } from '@chakra-ui/spinner'

const UserLaundryDashboard = () => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { laundries } = useSelector((state: RootState) => state.appReducer)
  const { _getLaundries, _getPrices } = bindActionCreators(appActions, dispatch)

  const loadData = async () => {
    if (isAuthenticated && user) {
      setLoading(true)
      await Promise.all([_getPrices(), _getLaundries(user.role)])
      setLoading(false)
    }
  }

  useEffect(() => {
    loadData()
  }, [isAuthenticated, user])

  return loading ? (
    <Box
      position='absolute'
      top='50%'
      left='50%'
      transform='translate(-50%, -50%)'
    >
      <Spinner size='lg' />
    </Box>
  ) : laundries.length === 0 ? (
    <Box
      position='absolute'
      top='50%'
      left='50%'
      transform='translate(-50%, -50%)'
      textAlign='center'
    >
      <Image src='/emptyCart.png' w={['90%', '90%', '75%']} />
      <Text fontWeight='bold'>No laundry found</Text>
    </Box>
  ) : (
    <Box pt='1rem'>
      <UserLaundryTable />
    </Box>
  )
}

export default UserLaundryDashboard
