import React, { useState, useEffect } from 'react'
import { Button } from '@chakra-ui/button'
import { HStack } from '@chakra-ui/layout'
import { Input } from '@chakra-ui/input'
import { Checkbox, CheckboxGroup } from '@chakra-ui/checkbox'
import { Radio, RadioGroup } from '@chakra-ui/radio'
import {
  NumberInputStepper,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInputField,
  NumberInput,
} from '@chakra-ui/number-input'
import { Select } from '@chakra-ui/select'
import { FormControl, FormLabel } from '@chakra-ui/form-control'
import {
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/modal'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { bindActionCreators } from 'redux'
import { useRouter } from 'next/router'
import appActions from '../../../redux/actions/app'
import { v4 } from 'uuid'

type Props = {
  onClose(): void
  isOpen: boolean
}

const UserLaundryModal = ({ isOpen, onClose }: Props) => {
  const [formData, setFormData] = useState({
    service: '',
    serviceType: '',
    serviceQty: 1,
    iron: false,
    detergent: true,
    detergentQty: 1,
    laundryBag: true,
    laundryBagQty: 1,
    paymentMethod: 'cash',
    extraCareOpt: [],
    branchLocation: 'Ikeja',
    location: '',
    isPaid: false,
    isPicked: false,
  })
  const [isServiceInvalid, setIsServiceInvalid] = useState(false)
  const [isServiceTypeInvalid, setIsServiceTypeInvalid] = useState(false)
  const [isLocationInvalid, setIsLocationInvalid] = useState(false)
  const { isAuthenticated, user } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const dispatch = useDispatch()
  const { _createLaundry } = bindActionCreators(appActions, dispatch)
  const router = useRouter()
  const currentTab = Number(router.query.tab)

  const onChange = (value: any, name: string) => {
    if (name === 'service') isServiceInvalid && setIsServiceInvalid(false)

    if (name === 'serviceType')
      isServiceTypeInvalid && setIsServiceTypeInvalid(false)

    if (name === 'location') isLocationInvalid && setIsLocationInvalid(false)

    setFormData({ ...formData, [name]: value })
  }

  const validate = (): boolean | undefined => {
    let errors: boolean | undefined

    const { service, serviceType, location } = formData

    if (service === '') {
      errors = true
      setIsServiceInvalid(true)
    }

    if (serviceType === '') {
      errors = true
      setIsServiceTypeInvalid(true)
    }

    if (location === '' || location.length < 4) {
      errors = true
      setIsLocationInvalid(true)
    }

    return errors
  }

  const submit = () => {
    const errors = validate()

    if (!errors) {
      const data = {
        ...formData,
        id: v4(),
        email: user?.email,
        location:
          formData.serviceType === 'delivery' ? formData.location : 'In Store',
        user: {
          name: user?.name,
          phone: user?.phone,
          address:
            formData.serviceType === 'delivery'
              ? formData.location
              : 'In Store',
          email: user?.email,
        },
      }
      _createLaundry('save', data)
      router.push({
        pathname: `/dashboard/laundry/new`,
        query: {
          tab: currentTab,
        },
      })
    }
  }

  useEffect(() => {
    setFormData({ ...formData, location: user?.address ? user?.address : '' })
  }, [isAuthenticated, isOpen])

  return (
    <ModalContent px='0.6rem'>
      <ModalHeader fontWeight='normal'>New Laundry</ModalHeader>
      <ModalCloseButton _focus={{ outline: 'none' }} />

      <ModalBody>
        <FormControl mb='1rem'>
          <Select
            isInvalid={isServiceInvalid}
            placeholder='Select Laundry'
            variant='filled'
            onChange={e => onChange(e.target.value, 'service')}
            value={formData.service}
          >
            <option value='Wash And Dry'>Wash And Dry</option>
            <option value='Dry'>Dry</option>
            <option value='Iron'>Iron</option>
          </Select>
        </FormControl>

        <FormControl mb='1rem'>
          <NumberInput
            value={formData.serviceQty}
            onChange={value => onChange(Number(value), 'serviceQty')}
            variant='filled'
            defaultValue={1}
            min={1}
            placeholder='Laundry Quantity'
            isDisabled={formData.service === 'Iron'}
          >
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
        </FormControl>

        <FormControl mb='1rem'>
          <Select
            isInvalid={isServiceTypeInvalid}
            placeholder='Select Type'
            variant='filled'
            onChange={e => onChange(e.target.value, 'serviceType')}
            value={formData.serviceType}
          >
            <option value='dropoff'>Drop Off</option>
            {formData.service !== 'Iron' && (
              <option value='self'>Self Service</option>
            )}
            <option value='delivery'>Delivery</option>
          </Select>
        </FormControl>

        <FormControl mb='1rem'>
          <Input
            type='text'
            autoCapitalize='true'
            errorBorderColor='red.500'
            isInvalid={isLocationInvalid}
            _focus={{ outline: 'none' }}
            name='location'
            variant='filled'
            placeholder='Address'
            value={
              formData.serviceType === 'delivery'
                ? formData.location
                : 'In Store'
            }
            onChange={e => onChange(e.target.value, 'location')}
            isDisabled={
              formData.serviceType === 'dropoff' ||
              formData.serviceType === 'self'
            }
          />
        </FormControl>

        <FormControl mb='1rem'>
          <Checkbox
            isChecked={formData.iron || formData.service === 'Iron'}
            isDisabled={formData.service === 'Iron'}
            onChange={e => onChange(e.target.checked, 'iron')}
          >
            Iron
          </Checkbox>
        </FormControl>
        <HStack mb='1rem' spacing={0}>
          <FormControl>
            <Checkbox
              isChecked={formData.detergent}
              onChange={e => onChange(e.target.checked, 'detergent')}
            >
              Detergent
            </Checkbox>
          </FormControl>

          <FormControl>
            <NumberInput
              value={formData.detergentQty}
              isDisabled={!formData.detergent}
              onChange={value => onChange(Number(value), 'detergentQty')}
              variant='filled'
              defaultValue={1}
              min={1}
              placeholder='Detergent Quantity'
            >
              <NumberInputField />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
          </FormControl>
        </HStack>

        <HStack mb='1rem' spacing={0}>
          <FormControl>
            <Checkbox
              isChecked={formData.laundryBag}
              onChange={e => onChange(e.target.checked, 'laundryBag')}
            >
              Laundry Bag
            </Checkbox>
          </FormControl>

          <FormControl>
            <NumberInput
              value={formData.laundryBagQty}
              isDisabled={!formData.laundryBag}
              onChange={value => onChange(Number(value), 'laundryBagQty')}
              variant='filled'
              defaultValue={1}
              min={1}
              placeholder='Bag Quantity'
            >
              <NumberInputField />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
          </FormControl>
        </HStack>

        <FormControl mb='1rem'>
          <FormLabel>Payment Method</FormLabel>
          <RadioGroup
            name='paymentMethod'
            onChange={val => onChange(val, 'paymentMethod')}
            value={formData.paymentMethod}
          >
            <HStack spacing={4}>
              <Radio
                value='card'
                colorScheme='yellow'
                _focus={{ outline: 'none' }}
              >
                Card
              </Radio>
              <Radio
                value='pos'
                colorScheme='blue'
                _focus={{ outline: 'none' }}
              >
                POS
              </Radio>
              <Radio
                value='cash'
                colorScheme='green'
                _focus={{ outline: 'none' }}
              >
                Cash
              </Radio>
              <Radio
                value='transfer'
                colorScheme='cyan'
                _focus={{ outline: 'none' }}
              >
                Transfer
              </Radio>
            </HStack>
          </RadioGroup>
        </FormControl>
      </ModalBody>

      <ModalFooter>
        <HStack spacing={4}>
          <Button
            _focus={{ outline: 'none' }}
            variant='ghost'
            onClick={onClose}
          >
            Cancel
          </Button>
          <Button
            _focus={{ outline: 'none' }}
            colorScheme='blue'
            mr={3}
            onClick={submit}
          >
            Next
          </Button>
        </HStack>
      </ModalFooter>
    </ModalContent>
  )
}

export default UserLaundryModal
