import { useRouter } from 'next/router'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { RootState } from '../../../redux/reducers'
import { Box, HStack, Text, Heading, VStack } from '@chakra-ui/layout'
import { Button } from '@chakra-ui/button'
import { Image } from '@chakra-ui/image'
import { Spinner } from '@chakra-ui/spinner'
import Head from 'next/head'
import SideBar from '../../../components/dashboard/SideBar'
import NoAuth from '../../../components/dashboard/NoAuth'
import UtilityService from '../../../utils/utils'
import { usePaystackPayment } from 'react-paystack'
import { capitalize } from 'lodash'
import { v4 } from 'uuid'
import DashboardNavbarMobile from '../../shared/DashboardNavbarMobile'

const UserLaundry = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const { query } = useRouter()
  const [loading, setLoading] = useState(false)
  const { laundry, isMobile } = useSelector(
    (state: RootState) => state.appReducer,
  )
  const { user, isAuthenticated, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { _getLaundry, _updateLaundry } = bindActionCreators(
    appActions,
    dispatch,
  )

  const onSuccess = async (ref: any) => {
    const data = {
      status: ref?.status,
      reference: ref?.transaction,
    }

    const updatedData = {
      isPaid: true,
      paidAt: new Date(),
    }

    await _updateLaundry(laundry._id, updatedData, 'user')
    UtilityService.toast({ title: 'Payment Successful', status: 'success' })
  }

  const paystackProps = {
    email: laundry.user.email,
    amount: laundry.total * 100,
    reference: v4(),
    publicKey: 'pk_test_79786defd13743e0187a62489c3ce5cd1f041dac',
  }

  const initializePayment = usePaystackPayment(paystackProps)

  const onCancel = async () =>
    UtilityService.toast({
      title: 'Payment Unsuccessful',
      status: 'error',
      description: 'Please try again later.',
    })

  const loadData = async () => {
    if (query.id) {
      setLoading(true)
      _getLaundry(query.id.toString(), user?.role, setLoading)
    }
  }

  useEffect(() => {
    loadData()
  }, [query.id])

  return (
    <>
      <Head>
        <title>Dashboard | Laundry</title>
      </Head>
      {isMobile && <DashboardNavbarMobile />}
      <HStack alignItems='flex-start'>
        {!isMobile && <SideBar />}
        <Box height='100vh' width={['100%', '100%', '75%']} position='relative'>
          <NoAuth />
          {authLoading === false && user && isAuthenticated && (
            <Box>
              {loading && (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                >
                  <Spinner size='xl' />
                </Box>
              )}
              {!loading && laundry._id === '' ? (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                  textAlign='center'
                >
                  <Image src='/emptyCart.png' w={['90%', '90%', '75%']} />
                  <Text fontWeight='bold'>No laundry found</Text>
                </Box>
              ) : (
                <HStack
                  padding='2rem'
                  justifyContent='stretch'
                  alignItems='stretch'
                  flexDirection={['column', 'column', 'row']}
                >
                  <Box w='full'>
                    <HStack spacing={8} mb='1rem'>
                      {laundry.service === 'Wash And Dry' ? (
                        <>
                          <Image
                            src='/bubbles.png'
                            width='100px'
                            height='100px'
                          />
                          <Heading>Wash And Dry</Heading>
                        </>
                      ) : laundry.service === 'Dry' ? (
                        <>
                          <Image src='/dry.png' width='100px' height='100px' />
                          <Heading>Dry</Heading>
                        </>
                      ) : (
                        <>
                          <Image
                            src='/ironing.png'
                            width='100px'
                            height='100px'
                          />
                          <Heading>Iron</Heading>
                        </>
                      )}
                    </HStack>
                    <VStack
                      spacing={4}
                      alignItems='flex-start'
                      fontWeight='bold'
                      mb='1.5rem'
                    >
                      <Text>
                        {laundry.service === 'Wash And Dry'
                          ? 'Wash And Dry'
                          : laundry.service === 'Dry'
                          ? 'Dry'
                          : 'Iron'}
                        : {laundry.serviceQty}
                      </Text>
                      <Text>Service Type: {laundry.serviceType}</Text>
                      {laundry.iron && <Text>Iron</Text>}
                      {laundry.detergent && (
                        <Text>Detergent : {laundry.detergentQty}</Text>
                      )}
                      {laundry.laundryBag && (
                        <Text>Laundry Bag : {laundry.laundryBagQty}</Text>
                      )}
                      {laundry.extraCareOpt.length > 0 &&
                        laundry.extraCareOpt.map(option => (
                          <Text key={option._id}>
                            {option.product}: {option.quantity}
                          </Text>
                        ))}
                      <Text>Address : {laundry.location}</Text>
                      <Text>
                        Payment Method :{' '}
                        {laundry.paymentMethod &&
                          capitalize(laundry.paymentMethod)}
                      </Text>
                    </VStack>
                    {isMobile && (
                      <Box w='full'>
                        {!laundry.laundryDetails ||
                        laundry.laundryDetails.length === 0 ? (
                          <Text fontWeight='bold'>No Ironing Info</Text>
                        ) : (
                          <Box>
                            <Text fontWeight='bold' mb='1.5rem'>
                              Ironing Details
                            </Text>
                            {laundry.laundryDetails.map(details => (
                              <Box key={details._id} mb='1rem'>
                                <Text fontWeight='bold'>
                                  {capitalize(details.fabric)}: {details.count}
                                </Text>
                              </Box>
                            ))}
                          </Box>
                        )}
                        {laundry.totalCount === 0 ? (
                          <Text fontWeight='bold' mb='1.5rem'>
                            Cloths not counted
                          </Text>
                        ) : (
                          <Text fontWeight='bold' fontSize='1.2rem' mb='1.5rem'>
                            Total Cloths: {laundry.totalCount}
                          </Text>
                        )}
                        {laundry.isPaid && (
                          <Text fontWeight='bold'>
                            Paid On:{' '}
                            {UtilityService.formatDate(
                              laundry.paidAt,
                              'MMM Do YY, h:mm a',
                            )}
                          </Text>
                        )}
                      </Box>
                    )}
                    <Box>
                      <Heading mb='1.5rem'>
                        Total : #{UtilityService.formatPrice(laundry.total)}
                      </Heading>
                      <HStack spacing={5}>
                        <Button
                          variant='ghost'
                          colorScheme='blue'
                          _focus={{ outline: 'none' }}
                          onClick={() => router.back()}
                        >
                          Back
                        </Button>

                        <Button
                          colorScheme='blue'
                          onClick={() => initializePayment(onSuccess, onCancel)}
                          _focus={{ outline: 'none' }}
                          disabled={laundry.isPaid || laundry.total === 0}
                        >
                          Pay Now
                        </Button>
                      </HStack>
                    </Box>
                  </Box>

                  {!isMobile && (
                    <Box w='full' pt='2rem' position='relative'>
                      {!laundry.laundryDetails ||
                      laundry.laundryDetails.length === 0 ? (
                        <Box
                          position='absolute'
                          top='50%'
                          left='50%'
                          transform='translate(-50%, -50%)'
                          textAlign='center'
                        >
                          <Image width='80%' src='/nodata.png' />
                          <Text fontWeight='bold'>No Ironing Info</Text>
                        </Box>
                      ) : (
                        <Box>
                          <Text fontSize='1.2rem' fontWeight='bold' mb='1.5rem'>
                            Ironing Details
                          </Text>
                          {laundry.laundryDetails.map(details => (
                            <Box key={details._id} mb='1rem'>
                              <Text fontWeight='bold'>
                                {capitalize(details.fabric)}: {details.count}
                              </Text>
                            </Box>
                          ))}
                        </Box>
                      )}
                      {laundry.totalCount === 0 ? (
                        <Text fontSize='1.2rem' fontWeight='bold' mb='1.5rem'>
                          Cloths not counted
                        </Text>
                      ) : (
                        <Text fontWeight='bold' fontSize='1.2rem' mb='1.5rem'>
                          Total Cloths: {laundry.totalCount}
                        </Text>
                      )}
                      {laundry.isPaid && (
                        <Text fontWeight='bold'>
                          Paid On:{' '}
                          {UtilityService.formatDate(
                            laundry.paidAt,
                            'MMM Do YY, h:mm a',
                          )}
                        </Text>
                      )}
                    </Box>
                  )}
                </HStack>
              )}
            </Box>
          )}
        </Box>
      </HStack>
    </>
  )
}

export default UserLaundry
