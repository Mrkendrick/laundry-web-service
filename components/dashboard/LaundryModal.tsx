import React from 'react'
import { Modal, ModalOverlay } from '@chakra-ui/modal'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'
import UserLaundryModal from './user/UserLaundryModal'
import StoreLaundryModal from './store/StoreLaundryModal'

type Props = {
  isOpen: boolean
  onClose(): void
}

const LaundryModal = ({ isOpen, onClose }: Props) => {
  const { user } = useSelector((state: RootState) => state.authReducer)

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      motionPreset='slideInBottom'
    >
      <ModalOverlay />
      {user?.role === 'user' ? (
        <UserLaundryModal onClose={onClose} isOpen={isOpen} />
      ) : (
        <StoreLaundryModal onClose={onClose} isOpen={isOpen} />
      )}
    </Modal>
  )
}

export default LaundryModal
