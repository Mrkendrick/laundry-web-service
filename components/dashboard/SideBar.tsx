import React, { memo, useEffect, useState } from 'react';
import { Box, VStack, HStack, Text, Center } from '@chakra-ui/layout';
import {
  Input,
  InputLeftElement,
  InputGroup,
  InputRightElement,
} from '@chakra-ui/input';
import { IconButton } from '@chakra-ui/button';
import { Image } from '@chakra-ui/image';
import NextLink from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/reducers';
import { Avatar, AvatarBadge } from '@chakra-ui/avatar';
import { Icon } from '@chakra-ui/icon';
import { Spinner } from '@chakra-ui/spinner';
import { useDisclosure } from '@chakra-ui/hooks';
import { GoGear } from 'react-icons/go';
import { BiSearchAlt, BiStats } from 'react-icons/bi';
import { FaTshirt } from 'react-icons/fa';
import { HiUser } from 'react-icons/hi';
import { MdLocalLaundryService, MdMessage } from 'react-icons/md';
import { useRouter } from 'next/router';
import ProfileModal from './ProfileModal';
import { IoMdLogOut, IoMdNotifications, IoMdPricetag } from 'react-icons/io';
import appActions from '../../redux/actions/app';
import { bindActionCreators } from 'redux';
import { useMemo } from 'react';
import { useDebouncedCallback } from 'use-debounce';
import { GrFormClose } from 'react-icons/gr';
import UserNotifications from './user/UserNotifications';
import PricesDrawer from './store/PricesDrawer';
import UtilityService from '../../utils/utils';

type Props = {
  callback?(query: string): void;
};

const SideBar = memo(({ callback }: Props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [text, setText] = useState('');
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  );
  const { isMobile } = useSelector((state: RootState) => state.appReducer);
  const { _logout } = bindActionCreators(appActions, dispatch);

  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isPricesOpen,
    onOpen: onPricesOpen,
    onClose: onPricesClose,
  } = useDisclosure();

  const currentTab = Number(router.query.tab);

  const debounced = useDebouncedCallback((value: string) => {
    callback && callback(value);
  }, 1000);

  const tabLinks = useMemo(
    () => [
      {
        name: 'Laundry',
        key: 'laundry',
        id: 212,
        alt: 'laundry',
        icon: FaTshirt,
      },
      {
        name: 'Users',
        key: 'users',
        id: 213,
        alt: 'users',
        icon: HiUser,
      },
      {
        name: 'Customers',
        alt: 'customer',
        id: 217,
        key: 'customer',
        icon: MdLocalLaundryService,
      },
      {
        name: 'Notifications',
        key: 'notifications',
        id: 214,
        alt: 'notifications',
        icon: IoMdNotifications,
      },
      {
        name: 'Messages',
        key: 'messages',
        id: 215,
        alt: 'messages',
        icon: MdMessage,
      },
    ],
    [],
  );

  const tabLinksUser = useMemo(
    () => [
      {
        name: 'Overview',
        alt: 'dashboard',
        id: 211,
        key: '',
        icon: BiStats,
      },
      {
        name: 'Laundry',
        key: 'laundry',
        id: 212,
        alt: 'laundry',
        icon: FaTshirt,
      },
    ],
    [],
  );

  useEffect(() => {
    UtilityService.checkNetwork();
    window.addEventListener('online', UtilityService.checkNetwork);
    window.addEventListener('offline', UtilityService.checkNetwork);

    return () => {
      window.removeEventListener('online', UtilityService.checkNetwork);
      window.removeEventListener('offline', UtilityService.checkNetwork);
    };
  }, []);

  return (
    <Box
      w={['100%', '100%', '25%']}
      maxW={['100%', '100%', '25%']}
      borderRightWidth={['0', '0', '1.8px']}
      color="gray.700"
      position="relative"
      pt="1rem"
      mt={['2rem', '2rem', '0']}
      px={['0', '0', '1.5rem']}
    >
      <ProfileModal isOpen={isOpen} onClose={onClose} />
      <PricesDrawer isOpen={isPricesOpen} onClose={onPricesClose} />
      <Center mb="1rem">
        <NextLink href="/" passHref>
          <Image
            src="/logo.png"
            alt="logo"
            width="165px"
            height="54px"
            textAlign="left"
            _hover={{ cursor: 'pointer' }}
          />
        </NextLink>
      </Center>
      <VStack h="full" spacing={6}>
        {authLoading ? (
          <Box position="absolute" top="50%" transform="translateY(-50%)">
            <Spinner color="blue.700" size="xl" />
          </Box>
        ) : (
          isAuthenticated && (
            <>
              {user?.role !== 'user' && (
                <HStack mt="0.8rem">
                  <InputGroup>
                    <InputLeftElement
                      children={<BiSearchAlt fontSize="20px" />}
                      color="gray.600"
                    />
                    <Input
                      placeholder="Search By Email"
                      name="search"
                      variant="filled"
                      type="email"
                      value={text}
                      onChange={(e) => {
                        setText(e.target.value);
                        debounced(e.target.value);
                      }}
                    />
                    <InputRightElement>
                      <Icon
                        _hover={{ cursor: 'pointer' }}
                        as={GrFormClose}
                        onClick={() => {
                          setText('');
                          debounced('');
                        }}
                      />
                    </InputRightElement>
                  </InputGroup>
                  <IconButton
                    _focus={{ outline: 'none' }}
                    aria-label="prices"
                    icon={<IoMdPricetag />}
                    onClick={onPricesOpen}
                  />
                </HStack>
              )}

              <Box w="full">
                {user?.role !== 'user'
                  ? tabLinks.map((link) => (
                      <NextLink
                        href={{
                          pathname: `/dashboard/${link.key}`,
                          query: { tab: link.id },
                        }}
                        passHref
                        key={link.key}
                      >
                        <HStack
                          spacing={4}
                          width="full"
                          bg={currentTab === link.id ? 'blue.600' : 'white'}
                          color={currentTab === link.id ? 'white' : 'gray.600'}
                          my="0.9rem"
                          _hover={{
                            background:
                              currentTab === link.id ? 'blue.600' : 'gray.100',
                            cursor: 'pointer',
                          }}
                          px="0.6rem"
                          py="0.55rem"
                          transition="background ease 300ms"
                          rounded="md"
                        >
                          <Icon as={link.icon} fontSize="20px" />
                          <Text>{link.name}</Text>
                        </HStack>
                      </NextLink>
                    ))
                  : tabLinksUser.map((link) => (
                      <NextLink
                        href={{
                          pathname: `/dashboard/${link.key}`,
                          query: { tab: link.id },
                        }}
                        passHref
                        key={link.key}
                      >
                        <HStack
                          spacing={4}
                          width="full"
                          bg={currentTab === link.id ? 'blue.600' : 'white'}
                          color={currentTab === link.id ? 'white' : 'gray.600'}
                          my="1rem"
                          _hover={{
                            background:
                              currentTab === link.id ? 'blue.600' : 'gray.100',
                            cursor: 'pointer',
                          }}
                          px="0.6rem"
                          py="0.55rem"
                          transition="background ease 300ms"
                          rounded="md"
                        >
                          <Icon as={link.icon} fontSize="20px" />
                          <Text>{link.name}</Text>
                        </HStack>
                      </NextLink>
                    ))}
                {user?.role === 'user' && <UserNotifications />}
              </Box>
            </>
          )
        )}
      </VStack>
      {isAuthenticated && user && !authLoading && (
        <>
          <HStack mb="1rem">
            {!isMobile && (
              <Avatar name={user?.name} src={user?.photo} size="sm">
                <AvatarBadge
                  bg={
                    user.role === 'user'
                      ? 'blue.400'
                      : user.role === 'attendant'
                      ? 'green.400'
                      : user.role === 'dispatch'
                      ? 'yellow.500'
                      : 'red.400'
                  }
                  boxSize="1em"
                />
              </Avatar>
            )}
            <Box>
              <Text fontSize="15px">{user?.name}</Text>
              <Text fontSize="13px">{user?.email}</Text>
            </Box>
            <IconButton
              aria-label="settings"
              colorScheme="gray"
              icon={<GoGear />}
              onClick={onOpen}
              _focus={{ outline: 'none' }}
              size="sm"
            />
            <IconButton
              onClick={_logout}
              _focus={{ outline: 'none' }}
              aria-label="logout"
              colorScheme="red"
              icon={<IoMdLogOut />}
              size="sm"
            />
          </HStack>
        </>
      )}
    </Box>
  );
});

export default SideBar;
