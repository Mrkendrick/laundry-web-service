import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'
import { Table, Thead, Tbody, Tr, Th, Td } from '@chakra-ui/table'
import UtilityService from '../../utils/utils'
import { Badge, Center } from '@chakra-ui/layout'
import { EProgress, EServiceType } from '../../utils/generics'
import { useRouter } from 'next/router'
import { truncate } from 'lodash'
import { Tooltip } from '@chakra-ui/tooltip'
import Pagination from '../shared/Pagination'
import { bindActionCreators } from 'redux'
import appActions from '../../redux/actions/app'

const UserLaundryTable = () => {
  const { laundries } = useSelector((state: RootState) => state.appReducer)
  const router = useRouter()

  return (
    <Table variant='simple' size='sm'>
      <Thead>
        <Tr>
          <Th>Service</Th>
          <Th display={['none', 'none', 'table-cell']}>Quantity</Th>
          <Th>Type</Th>
          <Th display={['none', 'none', 'table-cell']}>Address</Th>
          <Th>Progress</Th>
          <Th display={['none', 'none', 'table-cell']}>Total (#)</Th>
          <Th display={['none', 'none', 'table-cell']}>Paid</Th>
          <Th>Date</Th>
        </Tr>
      </Thead>
      <Tbody>
        {laundries.map(laundry => (
          <Tr
            key={laundry._id}
            _hover={{ cursor: 'pointer', background: 'gray.50' }}
            transition='background ease 300ms'
            onClick={() =>
              router.push({
                pathname: `/dashboard/laundry/${laundry._id}`,
                query: { tab: 212 },
              })
            }
          >
            <Td>{laundry.service}</Td>
            <Td display={['none', 'none', 'table-cell']}>
              {laundry.serviceQty}
            </Td>
            <Td>
              {laundry.serviceType === EServiceType.delivery
                ? 'Delivery'
                : laundry.serviceType === EServiceType.dropoff
                ? 'Drop-off'
                : 'Self Service'}
            </Td>
            <Td display={['none', 'none', 'table-cell']}>{laundry.location}</Td>
            <Td>
              <Badge
                fontSize='10px'
                colorScheme={
                  laundry.progress === EProgress.pending
                    ? 'yellow'
                    : laundry.progress === EProgress.washing
                    ? 'blue'
                    : laundry.progress === EProgress.delivered
                    ? 'green'
                    : 'red'
                }
              >
                {laundry.progress}
              </Badge>
            </Td>
            <Td display={['none', 'none', 'table-cell']}>
              {UtilityService.formatPrice(laundry.total)}
            </Td>
            <Td display={['none', 'none', 'table-cell']}>
              {laundry.isPaid ? (
                <Badge fontSize='10px' colorScheme='green'>
                  paid
                </Badge>
              ) : (
                <Badge fontSize='10px' colorScheme='yellow'>
                  not paid
                </Badge>
              )}
            </Td>
            <Td color='gray.600' fontSize='xs' fontStyle='italic'>
              {UtilityService.dateFromNow(laundry.createdAt)}
            </Td>
          </Tr>
        ))}
      </Tbody>
    </Table>
  )
}

type Props = {
  searchQuery: string
}

const StoreLaundryTable = ({ searchQuery }: Props) => {
  const { laundries } = useSelector(
    (state: RootState) => state.dashboardReducer,
  )
  const { user } = useSelector((state: RootState) => state.authReducer)
  const router = useRouter()
  const dispatch = useDispatch()
  const { _getLaundries } = bindActionCreators(appActions, dispatch)

  const setPage = async (num: number, callback: () => void) => {
    const options = {
      page: num,
      limit: 10,
      callback,
    }

    await _getLaundries(searchQuery, user?.role, options)
  }

  return (
    <>
      <Table variant='simple' size='sm'>
        <Thead>
          <Tr>
            <Th>Service</Th>
            <Th>Quantity</Th>
            <Th>Type</Th>
            <Th>User</Th>
            <Th>Address</Th>
            <Th>Progress</Th>
            <Th>Total (#)</Th>
            <Th>Paid</Th>
            <Th>Date</Th>
          </Tr>
        </Thead>
        <Tbody>
          {laundries.data.reverse().map(laundry => (
            <Tr
              key={laundry._id}
              _hover={{ cursor: 'pointer', background: 'gray.50' }}
              transition='background ease 300ms'
              onClick={() =>
                router.push({
                  pathname: `/dashboard/laundry/${laundry._id}`,
                  query: { tab: 212 },
                })
              }
            >
              <Td>{laundry.service}</Td>
              <Td>{laundry.serviceQty}</Td>
              <Td>
                {laundry.serviceType === EServiceType.delivery
                  ? 'Delivery'
                  : laundry.serviceType === EServiceType.dropoff
                  ? 'Drop-off'
                  : 'Self Service'}
              </Td>
              <Td>
                <Tooltip hasArrow label={laundry.email} aria-label='email'>
                  {truncate(laundry.email, { length: 15 })}
                </Tooltip>
              </Td>
              <Td>{laundry.location}</Td>
              <Td>
                <Badge
                  fontSize='10px'
                  colorScheme={
                    laundry.progress === EProgress.pending
                      ? 'yellow'
                      : laundry.progress === EProgress.washing
                      ? 'blue'
                      : laundry.progress === EProgress.delivered
                      ? 'green'
                      : 'red'
                  }
                >
                  {laundry.progress}
                </Badge>
              </Td>
              <Td>{UtilityService.formatPrice(laundry.total)}</Td>
              <Td>
                {laundry.isPaid ? (
                  <Badge fontSize='10px' colorScheme='green'>
                    paid
                  </Badge>
                ) : (
                  <Badge fontSize='10px' colorScheme='yellow'>
                    not paid
                  </Badge>
                )}
              </Td>
              <Td color='gray.600' fontSize='xs' fontStyle='italic'>
                {UtilityService.dateFromNow(laundry.createdAt)}
              </Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
      <Center mt='2rem'>
        {laundries.totalCount > 10 && (
          <Pagination
            count={laundries.totalCount}
            itemsPerPage={laundries.count}
            setPage={setPage}
          />
        )}
      </Center>
    </>
  )
}

export { UserLaundryTable, StoreLaundryTable }
