import React, { ChangeEvent, useState } from 'react'
import { Button } from '@chakra-ui/button'
import { Avatar } from '@chakra-ui/avatar'
import { HStack, Center, Stack } from '@chakra-ui/layout'
import { Radio, RadioGroup } from '@chakra-ui/radio'
import { Input, InputGroup, InputLeftAddon } from '@chakra-ui/input'
import { FormControl, FormLabel } from '@chakra-ui/form-control'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/modal'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'
import { HiUser } from 'react-icons/hi'
import { MdEmail, MdPhone } from 'react-icons/md'
import { HiLocationMarker } from 'react-icons/hi'
import { useMemo } from 'react'
import lo_ from 'lodash'
import UtilityService from '../../utils/utils'
import { useEffect } from 'react'
import { bindActionCreators } from 'redux'
import appActions from '../../redux/actions/app'

type Props = {
  isOpen: boolean
  onClose(): void
}

const ProfileModal = ({ isOpen, onClose }: Props) => {
  const dispatch = useDispatch()
  const { user, isAuthenticated } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const [formData, setFormData] = useState({
    name: '',
    address: '',
    phone: 0,
    gender: '',
    email: '',
  })
  const [isNameInvalid, setIsNameInvalid] = useState(false)
  const [isEmailInvalid, setIsEmailInvalid] = useState(false)
  const [isAddressInvalid, setIsAddressInvalid] = useState(false)
  const [isPhoneInvalid, setIsPhoneInvalid] = useState(false)
  const [loading, setLoading] = useState(false)
  const { _updateProfile } = bindActionCreators(appActions, dispatch)

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    lo_.keysIn(formData).forEach(key => {
      if (key === e.target.name && key === 'address' && isAddressInvalid)
        setIsAddressInvalid(false)
      if (key === e.target.name && key === 'name' && isNameInvalid)
        setIsNameInvalid(false)
      if (key === e.target.name && key === 'email' && isEmailInvalid)
        setIsEmailInvalid(false)
      if (key === e.target.name && key === 'phone' && isPhoneInvalid)
        setIsPhoneInvalid(false)
    })

    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const validate = () => {
    let errors: boolean | undefined

    if (!formData.name) {
      errors = true
      setIsNameInvalid(true)
    }

    if (!formData.address) {
      errors = true
      setIsAddressInvalid(true)
    }

    if (!formData.email || !formData.email.match(UtilityService.emailRegex)) {
      errors = true
      setIsEmailInvalid(true)
    }

    /* if (!String(formData.phone).match(UtilityService.phoneRegex)) {
      errors = true
      setIsPhoneInvalid(true)
    } */

    return errors
  }

  const updateProfile = async () => {
    const errors = validate()

    if (!errors) {
      setLoading(true)
      const updatedInfo = {
        ...user,
        ...formData,
      }

      await _updateProfile(updatedInfo, false, setLoading, onClose)
    }
  }

  const loadFields = () => {
    if (user && isAuthenticated && isOpen) {
      setFormData({
        ...formData,
        name: user.name,
        email: user.email,
        phone: !user.phone ? 0 : user.phone,
        address: !user.address ? '' : user.address,
        gender: !user.gender ? '' : user.gender,
      })
    }
  }

  useMemo(() => {
    loadFields()
  }, [user, isOpen])

  useEffect(() => {
    loadFields()
  }, [])

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      motionPreset='slideInBottom'
    >
      <ModalOverlay />
      <ModalContent px='0.6rem'>
        <ModalHeader fontWeight='normal'>Edit Profile</ModalHeader>
        <ModalCloseButton _focus={{ outline: 'none' }} />
        {isAuthenticated && user && (
          <ModalBody>
            <Center mb='1.5rem'>
              <Avatar
                size='xl'
                src={user.photo}
                name={user.name}
                mr='1rem'
                _hover={{ cursor: 'pointer' }}
              />
            </Center>
            <FormControl mb='1rem'>
              <InputGroup>
                <InputLeftAddon
                  mx='-0.5rem'
                  children={<HiUser color='gray.500' />}
                />
                <Input
                  type='text'
                  autoCapitalize='true'
                  isInvalid={isNameInvalid}
                  errorBorderColor='red.500'
                  _focus={{ outline: 'none' }}
                  name='name'
                  variant='filled'
                  placeholder='John Doe'
                  value={formData.name}
                  onChange={onChange}
                />
              </InputGroup>
            </FormControl>
            <FormControl mb='1rem'>
              <InputGroup>
                <InputLeftAddon
                  mx='-0.5rem'
                  children={<MdEmail color='gray.500' />}
                />
                <Input
                  type='email'
                  isInvalid={isEmailInvalid}
                  errorBorderColor='red.500'
                  _focus={{ outline: 'none' }}
                  name='email'
                  variant='filled'
                  placeholder='example@email.com'
                  value={formData.email}
                  onChange={onChange}
                />
              </InputGroup>
            </FormControl>
            <FormControl mb='1rem'>
              <InputGroup>
                <InputLeftAddon
                  mx='-0.5rem'
                  children={<HiLocationMarker color='gray.500' />}
                />
                <Input
                  type='text'
                  isInvalid={isAddressInvalid}
                  errorBorderColor='red.500'
                  _focus={{ outline: 'none' }}
                  name='address'
                  variant='filled'
                  placeholder='Address'
                  value={formData.address}
                  onChange={onChange}
                />
              </InputGroup>
            </FormControl>
            <FormControl mb='1rem'>
              <InputGroup>
                <InputLeftAddon
                  mx='-0.5rem'
                  children={<MdPhone color='gray.500' />}
                />
                <Input
                  type='tel'
                  isInvalid={isPhoneInvalid}
                  errorBorderColor='red.500'
                  _focus={{ outline: 'none' }}
                  name='phone'
                  variant='filled'
                  placeholder='080XXXXXXXX'
                  value={formData.phone}
                  onChange={onChange}
                />
              </InputGroup>
            </FormControl>

            <FormControl mb='1rem'>
              <FormLabel>Gender</FormLabel>
              <RadioGroup
                defaultValue={formData.gender}
                name='gender'
                onChange={(val: 'male' | 'female') =>
                  setFormData({ ...formData, gender: val })
                }
              >
                <Stack spacing={4} direction='row'>
                  <Radio value='male'>Male</Radio>
                  <Radio value='female'>Female</Radio>
                </Stack>
              </RadioGroup>
            </FormControl>
          </ModalBody>
        )}

        <ModalFooter>
          <HStack spacing={4}>
            <Button
              _focus={{ outline: 'none' }}
              variant='ghost'
              onClick={onClose}
            >
              Discard
            </Button>
            <Button
              _focus={{ outline: 'none' }}
              colorScheme='blue'
              mr={3}
              onClick={updateProfile}
              isLoading={loading}
              isDisabled={user?.role !== 'user'}
            >
              Save
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}

export default ProfileModal
