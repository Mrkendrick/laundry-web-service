import React from 'react'
import { useSelector } from 'react-redux'
import { Box, Text, Button, Spinner } from '@chakra-ui/react'
import { Image } from '@chakra-ui/image'
import NextLink from 'next/link'
import { RootState } from '../../redux/reducers'

const NoAuth = () => {
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { isMobile } = useSelector((state: RootState) => state.appReducer)

  return (
    <>
      {authLoading && (
        <Spinner
          position='absolute'
          top='50%'
          left='50%'
          transform='translate(-50%, -50%)'
          size='xl'
        />
      )}

      {authLoading === false && !user && !isAuthenticated && (
        <Box
          position='absolute'
          top='50%'
          left='50%'
          transform='translate(-50%, -50%)'
          textAlign='center'
        >
          {!isMobile && <Image src='/noauth.png' />}

          <Text fontWeight='bold' fontSize='20px' mb='1rem'>
            Please Log In
          </Text>
          <NextLink
            passHref
            href={{ pathname: '/auth', query: { type: 'login' } }}
          >
            <Button
              py='1.2rem'
              px='1.5rem'
              _focus={{ outline: 'none' }}
              colorScheme='blue'
            >
              Login
            </Button>
          </NextLink>
        </Box>
      )}
    </>
  )
}

export default NoAuth
