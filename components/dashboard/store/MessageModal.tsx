import React, { memo } from 'react'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/modal'
import { Button } from '@chakra-ui/button'
import { Text, HStack } from '@chakra-ui/layout'
import UtilityService from '../../../utils/utils'
import { useClipboard } from '@chakra-ui/hooks'
import { Icon } from '@chakra-ui/icon'
import { MdContentCopy } from 'react-icons/md'
import { FiCheck } from 'react-icons/fi'

type Props = {
  owner: string
  message: string
  time: string
  email: string
  isOpen: boolean
  onClose(): void
}

const MessageModal = memo(
  ({ isOpen, onClose, time, owner, message, email }: Props) => {
    const { hasCopied, onCopy } = useClipboard(email)

    return (
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        isCentered
        motionPreset='slideInBottom'
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            <Text mb='0.4rem'>From: {owner}</Text>
            <HStack spacing={3}>
              <Text fontWeight='normal' fontSize='0.85rem'>
                {email}
              </Text>
              <Icon
                color={hasCopied ? 'green.500' : 'gray.600'}
                onClick={onCopy}
                as={hasCopied ? FiCheck : MdContentCopy}
              />
            </HStack>
          </ModalHeader>
          <ModalCloseButton _focus={{ outline: 'none' }} />
          <ModalBody>
            <Text mb='1rem' fontSize='1.1rem'>
              {message}
            </Text>
            <Text fontStyle='italic' fontSize='0.8rem' color='gray.600'>
              {UtilityService.formatDate(time, 'MMM Do YY, h:mm a')}
            </Text>
          </ModalBody>

          <ModalFooter>
            <Button
              _focus={{ outline: 'none' }}
              variant='ghost'
              colorScheme='blue'
              onClick={onClose}
            >
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    )
  },
)

export default MessageModal
