import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { Table, Thead, Tbody, Tr, Th, Td } from '@chakra-ui/table'
import { HStack, Text, Center } from '@chakra-ui/layout'
import { Avatar } from '@chakra-ui/avatar'
import { floor, truncate } from 'lodash'
import Pagination from '../../shared/Pagination'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { useRouter } from 'next/router'

type Props = {
  searchQuery: string
}

const StoreCustomerDashboard = ({ searchQuery }: Props) => {
  const dispatch = useDispatch()
  const router = useRouter()
  const { customers } = useSelector(
    (state: RootState) => state.dashboardReducer,
  )
  const { _getCustomers } = bindActionCreators(appActions, dispatch)

  const setPage = async (num: number, callback: () => void) => {
    const options = {
      page: num,
      limit: 10,
      callback,
    }

    await _getCustomers(
      searchQuery ? searchQuery : undefined,
      undefined,
      options,
    )
  }

  return (
    <>
      <Table size='sm'>
        <Thead>
          <Tr>
            <Th>Name</Th>
            <Th>Email</Th>
            <Th>Points</Th>
            <Th>Phone</Th>
            <Th>Address</Th>
          </Tr>
        </Thead>
        <Tbody>
          {customers.data.map(customer => (
            <Tr
              key={customer._id}
              _hover={{ cursor: 'pointer', background: 'gray.50' }}
              transition='background ease 300ms'
              onClick={() =>
                router.push({
                  pathname: `/dashboard/customer/${customer._id}`,
                  query: { tab: 217, email: customer.email },
                })
              }
            >
              <Td>
                <HStack spacing={3}>
                  <Avatar name={customer.name} size='sm' />
                  <Text>{customer.name}</Text>
                </HStack>
              </Td>
              <Td>{customer.email}</Td>
              <Td>{floor(customer.points, 1)}</Td>
              <Td>{customer.phone}</Td>
              <Td>{truncate(customer.address, { length: 20 })}</Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
      <Center mt='2rem'>
        {customers.totalCount > 10 && (
          <Pagination
            count={customers.totalCount}
            itemsPerPage={10}
            setPage={setPage}
          />
        )}
      </Center>
    </>
  )
}

export default StoreCustomerDashboard
