import React, { useState } from 'react'
import { Table, Thead, Tbody, Tr, Th, Td } from '@chakra-ui/table'
import { HStack, Text, Flex } from '@chakra-ui/layout'
import { Avatar } from '@chakra-ui/avatar'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import UtilityService from '../../../utils/utils'
import Pagination from '../../shared/Pagination'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { Button } from '@chakra-ui/button'
import AlertDialog from '../../shared/AlertDialog'
import { useDisclosure } from '@chakra-ui/react'
import MessageModal from './MessageModal'

const StoreMessages = () => {
  const { onOpen, isOpen, onClose } = useDisclosure()
  const {
    onOpen: onMessageOpen,
    isOpen: isMessageOpen,
    onClose: onMessageClose,
  } = useDisclosure()
  const dispatch = useDispatch()
  const [message, setMessage] = useState({
    owner: '',
    time: '',
    message: '',
    email: '',
  })
  const { messages } = useSelector((state: RootState) => state.dashboardReducer)
  const { user } = useSelector((state: RootState) => state.authReducer)
  const { _getMessages, _deleteMessages } = bindActionCreators(
    appActions,
    dispatch,
  )

  const setPage = async (num: number, callback: () => void) => {
    const options = {
      page: num,
      limit: 10,
      callback,
    }

    await _getMessages(undefined, options)
  }

  const onClick = (data: {
    owner: string
    time: string
    message: string
    email: string
  }) => {
    setMessage(data)
    onMessageOpen()
  }

  const onDelete = async () => {
    onClose()
    await _deleteMessages()
  }

  return (
    <>
      <AlertDialog
        type='delete'
        onClose={onClose}
        isOpen={isOpen}
        heading='Delete All Messages'
        body='Are you sure?'
        onDeleteClick={onDelete}
      />
      <MessageModal
        isOpen={isMessageOpen}
        onClose={onMessageClose}
        time={message.time}
        owner={message.owner}
        message={message.message}
        email={message.email}
      />
      <Table variant='simple' size='sm'>
        <Thead>
          <Tr>
            <Th>Name</Th>
            <Th>Email</Th>
            <Th>Message</Th>
            <Th>Time</Th>
          </Tr>
        </Thead>
        <Tbody>
          {messages.data.map(message => (
            <Tr
              key={message._id}
              _hover={{ cursor: 'pointer', background: 'gray.50' }}
              transition='background ease 300ms'
              onClick={() =>
                onClick({
                  owner: message.name,
                  time: message.createdAt,
                  message: message.message,
                  email: message.email,
                })
              }
            >
              <Td>
                <HStack spacing={3}>
                  <Avatar name={message.name} size='sm' />
                  <Text>{message.name}</Text>
                </HStack>
              </Td>
              <Td>{message.email}</Td>
              <Td>{message.message}</Td>
              <Td color='gray.600' fontSize='xs' fontStyle='italic'>
                {UtilityService.dateFromNow(message.createdAt)}
              </Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
      <Flex
        mt='2rem'
        alignItems='center'
        justifyContent='space-between'
        px='2rem'
      >
        {messages.totalCount > 10 && (
          <Pagination
            count={messages.totalCount}
            itemsPerPage={10}
            setPage={setPage}
          />
        )}
        <Button
          colorScheme='red'
          ml='1rem'
          _focus={{ outline: 'none' }}
          onClick={onOpen}
          isDisabled={user?.role !== 'manager'}
        >
          Delete all
        </Button>
      </Flex>
    </>
  )
}

export default StoreMessages
