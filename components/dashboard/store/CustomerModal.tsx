import React, { ChangeEvent } from 'react'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/modal'
import { Button } from '@chakra-ui/button'
import { FormControl, FormLabel } from '@chakra-ui/form-control'
import { Input, InputGroup } from '@chakra-ui/input'
import { useState } from 'react'
import UtilityService from '../../../utils/utils'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { useDispatch, useSelector } from 'react-redux'
import { trim } from 'lodash'
import { useEffect } from 'react'
import { RootState } from '../../../redux/reducers'

type Props = {
  isOpen: boolean
  onClose(): void
  type: 'edit' | 'create'
}

const CustomerModal = ({ isOpen, onClose, type }: Props) => {
  const dispatch = useDispatch()
  const [customerId, setCustomerId] = useState('')
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    phone: '',
    address: '',
  })
  const [isEmailInvalid, setIsEmailInvalid] = useState(false)
  const [isPhoneInvalid, setIsPhoneInvalid] = useState(false)
  const [isAddressInvalid, setIsAddressInvalid] = useState(false)
  const [isNameInvalid, setIsNameInvalid] = useState(false)
  const [loading, setLoading] = useState(false)
  const { customer } = useSelector((state: RootState) => state.dashboardReducer)
  const { _createCustomer, _updateCustomer } = bindActionCreators(
    appActions,
    dispatch,
  )

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target

    name === 'name' && isNameInvalid && setIsNameInvalid(false)
    name === 'email' && isEmailInvalid && setIsEmailInvalid(false)
    name === 'phone' && isPhoneInvalid && setIsPhoneInvalid(false)
    name === 'address' && isAddressInvalid && setIsAddressInvalid(false)

    setFormData({ ...formData, [name]: value })
  }

  const validate = () => {
    let errors: boolean | undefined
    const { name, email, address, phone } = formData

    if (name === '') {
      errors = true
      setIsNameInvalid(true)
    }

    if (email === '' || !email.match(UtilityService.emailRegex)) {
      errors = true
      setIsEmailInvalid(true)
    }

    if (address === '') {
      errors = true
      setIsAddressInvalid(true)
    }

    if (phone === '') {
      errors = true
      setIsPhoneInvalid(true)
    }

    return errors
  }

  const onSave = async () => {
    const errors = validate()

    if (!errors) {
      setLoading(true)
      const data = {
        ...formData,
        name: trim(formData.name),
        phone: Number(formData.phone),
        address: trim(formData.address),
      }

      const callback = () => {
        setFormData({ name: '', email: '', phone: '', address: '' })
        onClose()
      }

      await _createCustomer(data, setLoading, callback)
    }
  }

  const onUpdate = async () => {
    const errors = validate()

    if (!errors) {
      setLoading(true)
      const data = {
        ...formData,
        id: customerId,
        name: trim(formData.name),
        phone: Number(formData.phone),
        address: trim(formData.address),
      }

      const callback = () => {
        setFormData({ name: '', email: '', phone: '', address: '' })
        onClose()
      }

      await _updateCustomer(data, setLoading, callback)
    }
  }

  const loadData = () => {
    if (isOpen && type === 'edit') {
      setCustomerId(customer._id)
      setFormData({
        name: customer.name,
        address: customer.address,
        phone: customer.phone.toString(),
        email: customer.email,
      })
    }
  }

  useEffect(() => {
    loadData()
  }, [type, isOpen])

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      motionPreset='slideInBottom'
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>
          {type === 'edit' ? 'Edit Customer' : 'Add Customer'}
        </ModalHeader>
        <ModalCloseButton _focus={{ outline: 'none' }} />
        <ModalBody>
          <FormControl mb='1.2rem'>
            <FormLabel>Name</FormLabel>
            <InputGroup>
              <Input
                type='text'
                isInvalid={isNameInvalid}
                errorBorderColor='red.500'
                name='name'
                variant='filled'
                placeholder='John Doe'
                onChange={onChange}
                value={formData.name}
              />
            </InputGroup>
          </FormControl>

          <FormControl mb='1.2rem'>
            <FormLabel>Email</FormLabel>
            <InputGroup>
              <Input
                type='email'
                isInvalid={isEmailInvalid}
                errorBorderColor='red.500'
                name='email'
                variant='filled'
                placeholder='customer@mail.com'
                onChange={onChange}
                value={formData.email}
                isDisabled={type === 'edit'}
              />
            </InputGroup>
          </FormControl>

          <FormControl mb='1.2rem'>
            <FormLabel>Phone</FormLabel>
            <InputGroup>
              <Input
                type='tel'
                isInvalid={isPhoneInvalid}
                errorBorderColor='red.500'
                name='phone'
                variant='filled'
                placeholder='080XXXXXXXX'
                onChange={onChange}
                value={formData.phone}
              />
            </InputGroup>
          </FormControl>

          <FormControl mb='1.2rem'>
            <FormLabel>Address</FormLabel>
            <InputGroup>
              <Input
                type='text'
                isInvalid={isAddressInvalid}
                errorBorderColor='red.500'
                name='address'
                variant='filled'
                placeholder='Enter Address'
                onChange={onChange}
                value={formData.address}
              />
            </InputGroup>
          </FormControl>
        </ModalBody>

        <ModalFooter>
          <Button
            _focus={{ outline: 'none' }}
            variant='ghost'
            colorScheme='blue'
            onClick={onClose}
            mr={3}
          >
            Cancel
          </Button>
          <Button
            _focus={{ outline: 'none' }}
            colorScheme='blue'
            onClick={type === 'edit' ? onUpdate : onSave}
            isLoading={loading}
          >
            {type === 'edit' ? 'Update' : 'Save'}
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}

export default CustomerModal
