import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { Button } from '@chakra-ui/button'
import { Avatar, AvatarBadge } from '@chakra-ui/avatar'
import { HStack, Center, Text, Box } from '@chakra-ui/layout'
import { Radio, RadioGroup } from '@chakra-ui/radio'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/modal'
import { IoIosMale, IoIosFemale } from 'react-icons/io'
import { Icon } from '@chakra-ui/icon'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'

type Props = {
  isOpen: boolean
  onClose(): void
  loadData(): Promise<void>
}

const UserModal = ({ isOpen, onClose, loadData }: Props) => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [role, setRole] = useState('')
  const { user } = useSelector((state: RootState) => state.dashboardReducer)
  const {
    user: authUser,
    isAuthenticated,
    authLoading,
  } = useSelector((state: RootState) => state.authReducer)
  const { _updateProfile } = bindActionCreators(appActions, dispatch)

  const callback = async () => {
    await loadData()
    onClose()
  }

  const update = async () => {
    setLoading(true)
    const isAdmin = true
    const data = {
      userId: user._id,
      role,
    }
    await _updateProfile(data, isAdmin, setLoading, callback)
  }

  useEffect(() => {
    user._id && setRole(user.role)
  }, [isOpen, user._id])

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isCentered
      motionPreset='slideInBottom'
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader fontWeight='normal'>Profile</ModalHeader>
        <ModalCloseButton _focus={{ outline: 'none' }} />
        {isAuthenticated && authUser && authLoading === false && user._id && (
          <ModalBody>
            <Center mb='1rem'>
              <Avatar src={user.photo} size='xl' name={user.name}>
                <AvatarBadge
                  bg={
                    role === 'user'
                      ? 'blue.400'
                      : role === 'attendant'
                      ? 'green.400'
                      : role === 'dispatch'
                      ? 'yellow.500'
                      : 'red.400'
                  }
                  boxSize='1em'
                />
              </Avatar>
            </Center>

            <Box textAlign='center' mb='1rem'>
              <Text fontWeight='bold' fontSize='1.2rem' mb='0.8rem'>
                {user.name}
                {user.gender === 'male' ? (
                  <Icon ml='0.6rem' as={IoIosMale} fontSize='1.5rem' />
                ) : user.gender === 'female' ? (
                  <Icon ml='0.6rem' as={IoIosFemale} fontSize='1.5rem' />
                ) : (
                  'N/A'
                )}
              </Text>
              <Text mb='0.8rem' color='gray.600'>
                {user.email}
              </Text>
              {user.address && (
                <Text color='gray.600' mb='0.8rem'>
                  {user.address}
                </Text>
              )}
              {user.phone && <Text color='gray.600'>{user.phone}</Text>}
            </Box>
            <Center>
              <RadioGroup
                name='progress'
                onChange={setRole}
                value={role}
                size='sm'
              >
                <HStack spacing={4}>
                  <Radio
                    value='dispatch'
                    colorScheme='yellow'
                    _focus={{ outline: 'none' }}
                    isDisabled={authUser.role !== 'manager'}
                  >
                    Dispatch
                  </Radio>
                  <Radio
                    value='user'
                    colorScheme='blue'
                    _focus={{ outline: 'none' }}
                    isDisabled={authUser.role !== 'manager'}
                  >
                    Customer
                  </Radio>
                  <Radio
                    value='attendant'
                    colorScheme='green'
                    _focus={{ outline: 'none' }}
                    isDisabled={authUser.role !== 'manager'}
                  >
                    Attendant
                  </Radio>
                  <Radio
                    value='manager'
                    colorScheme='red'
                    _focus={{ outline: 'none' }}
                    isDisabled={authUser.role !== 'manager'}
                  >
                    Manager
                  </Radio>
                </HStack>
              </RadioGroup>
            </Center>
          </ModalBody>
        )}

        <ModalFooter>
          <HStack spacing={4}>
            <Button
              _focus={{ outline: 'none' }}
              variant='ghost'
              onClick={onClose}
            >
              Discard
            </Button>
            <Button
              _focus={{ outline: 'none' }}
              colorScheme='blue'
              mr={3}
              isDisabled={authUser?.role !== 'manager'}
              onClick={update}
              isLoading={loading}
            >
              Save
            </Button>
          </HStack>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}

export default UserModal
