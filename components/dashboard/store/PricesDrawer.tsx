import React, { useEffect, useState, useMemo } from 'react'
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  DrawerFooter,
} from '@chakra-ui/modal'
import { Button } from '@chakra-ui/button'
import { Image } from '@chakra-ui/image'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { Box, HStack, Text, SimpleGrid, Center } from '@chakra-ui/layout'
import { Spinner } from '@chakra-ui/spinner'
import { AiOutlineReload } from 'react-icons/ai'
import { IconButton } from '@chakra-ui/button'
import { FormControl, FormLabel } from '@chakra-ui/form-control'
import { Input } from '@chakra-ui/input'
import { NumberInput, NumberInputField } from '@chakra-ui/number-input'
import { BiTrash } from 'react-icons/bi'
import { v4 } from 'uuid'

type Props = {
  isOpen: boolean
  onClose(): void
}

const PricesDrawer = ({ isOpen, onClose }: Props) => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [updateLoading, setUpdateLoading] = useState(false)
  const [formData, setFormData] = useState({
    _id: '',
    washDrySelf: 0,
    drySelf: 0,
    washDryDropoff: 0,
    dryDropOff: 0,
    ironDropoff: 0,
    iron: 0,
    ironing: [{ _id: '', name: '', key: '', price: 0 }],
    detergent: 0,
    bleach: 0,
    softner: 0,
    stainRemover: 0,
    delivery: 0,
    laundryBag: 0,
  })
  const [isDrySelfInvalid, setIsDrySelfInvalid] = useState(false)
  const [isDryDropoffInvalid, setIsDryDropoffInvalid] = useState(false)
  const [isWashDrySelfInvalid, setIsWashDrySelfInvalid] = useState(false)
  const [isLaundryBagInvalid, setIsLaundryBagInvalid] = useState(false)
  const [isDeliveryInvalid, setIsDeliveryInvalid] = useState(false)
  const [isSoftnerInvalid, setIsSoftnerInvalid] = useState(false)
  const [isBleachInvalid, setIsBleachInvalid] = useState(false)
  const [isStainRemoverInvalid, setIsStainRemoverInvalid] = useState(false)
  const [isDetergentInvalid, setIsDetergentInvalid] = useState(false)
  const [isIronDropOffInvalid, setIsIronDropOffInvalid] = useState(false)
  const [isWashDryDropoffInvalid, setIsWashDryDropoffInvalid] = useState(false)
  const [isIroningInvalid, setIsIroningInvalid] = useState({
    id: '',
    error: false,
  })
  const { prices } = useSelector((state: RootState) => state.appReducer)
  const { user } = useSelector((state: RootState) => state.authReducer)
  const { _getPrices, _updatePrices } = bindActionCreators(appActions, dispatch)

  const onChange = (name: string, value: any) => {
    name === 'washDrySelf' &&
      isWashDrySelfInvalid &&
      setIsWashDrySelfInvalid(false)
    name === 'laundryBag' &&
      isLaundryBagInvalid &&
      setIsLaundryBagInvalid(false)
    name === 'delivery' && isDeliveryInvalid && setIsDeliveryInvalid(false)
    name === 'softner' && isSoftnerInvalid && setIsSoftnerInvalid(false)
    name === 'bleach' && isBleachInvalid && setIsBleachInvalid(false)
    name === 'stainRemover' &&
      isStainRemoverInvalid &&
      setIsStainRemoverInvalid(false)
    name === 'detergent' && isDetergentInvalid && setIsDetergentInvalid(false)
    name === 'ironDropoff' &&
      isIronDropOffInvalid &&
      setIsIronDropOffInvalid(false)
    name === 'dryDropOff' &&
      isDryDropoffInvalid &&
      setIsDryDropoffInvalid(false)
    name === 'washDryDropoff' &&
      isWashDryDropoffInvalid &&
      setIsWashDryDropoffInvalid(false)
    name === 'drySelf' && isDrySelfInvalid && setIsDrySelfInvalid(false)

    setFormData({
      ...formData,
      [name]: value,
    })
  }

  const onDeleteIroning = (id: string) =>
    setFormData({
      ...formData,
      ironing: [...formData.ironing.filter(data => data._id !== id)],
    })

  const onAddIroning = () =>
    setFormData({
      ...formData,
      ironing: [
        ...formData.ironing,
        { _id: v4(), name: '', key: '', price: 0 },
      ],
    })

  const onIroningChange = (
    id: string,
    type: 'cloth' | 'price',
    value: string | number,
  ) => {
    if (type === 'cloth') {
      isIroningInvalid.error && setIsIroningInvalid({ id: '', error: false })
      setFormData({
        ...formData,
        ironing: [
          ...formData.ironing.map(data =>
            data._id === id
              ? {
                  _id: id,
                  price: data.price,
                  name: String(value),
                  key: String(value).toLowerCase(),
                }
              : data,
          ),
        ],
      })
    }

    if (type === 'price') {
      setFormData({
        ...formData,
        ironing: [
          ...formData.ironing.map(data =>
            data._id === id
              ? {
                  _id: id,
                  price: Number(value),
                  name: data.name,
                  key: data.key,
                }
              : data,
          ),
        ],
      })
    }
  }

  const validate = (): boolean | undefined => {
    let errors: boolean | undefined

    const {
      bleach,
      delivery,
      detergent,
      dryDropOff,
      drySelf,
      ironDropoff,
      laundryBag,
      softner,
      stainRemover,
      washDryDropoff,
      washDrySelf,
      ironing,
    } = formData

    if (bleach === 0) {
      errors = true
      setIsBleachInvalid(true)
    }

    if (delivery === 0) {
      errors = true
      setIsDeliveryInvalid(true)
    }

    if (detergent === 0) {
      errors = true
      setIsDetergentInvalid(true)
    }

    if (dryDropOff === 0) {
      errors = true
      setIsDryDropoffInvalid(true)
    }

    if (drySelf === 0) {
      errors = true
      setIsDrySelfInvalid(true)
    }

    if (ironDropoff === 0) {
      errors = true
      setIsIronDropOffInvalid(true)
    }

    if (laundryBag === 0) {
      errors = true
      setIsLaundryBagInvalid(true)
    }

    if (softner === 0) {
      errors = true
      setIsSoftnerInvalid(true)
    }

    if (stainRemover === 0) {
      errors = true
      setIsStainRemoverInvalid(true)
    }

    if (washDryDropoff === 0) {
      errors = true
      setIsWashDryDropoffInvalid(true)
    }

    if (washDrySelf === 0) {
      errors = true
      setIsWashDrySelfInvalid(true)
    }

    if (ironing.length > 0) {
      for (const { _id, name, price } of ironing) {
        if (_id === '' || name === '' || price === 0) {
          setIsIroningInvalid({ id: _id, error: true })
          errors = true
          break
        }
      }
    }

    return errors
  }

  const update = async () => {
    const errors = validate()

    if (!errors) {
      setUpdateLoading(true)
      const data = {
        ...formData,
        ironing: [
          ...formData.ironing.map(({ key, name, price }) => ({
            key,
            name,
            price,
          })),
        ],
      }

      await _updatePrices(formData._id, data, setUpdateLoading)
    }
  }

  const loadFields = () => {
    if (prices.length > 0) {
      const priceList = prices[0]
      setFormData({
        ...priceList,
      })
    }
  }

  const loadData = async () => {
    setLoading(true)
    await _getPrices(setLoading)
  }

  useMemo(() => {
    loadFields()
  }, [prices])

  useEffect(() => {
    isOpen && loadData()
  }, [isOpen])

  return (
    <Drawer onClose={onClose} isOpen={isOpen} size='xl'>
      <DrawerOverlay />
      <DrawerContent>
        <DrawerCloseButton _focus={{ outline: 'none' }} />
        <DrawerHeader borderBottomWidth='1px'>
          <HStack spacing={5}>
            <Text>Prices</Text>
            <IconButton
              _focus={{ outline: 'none' }}
              aria-label='reload'
              icon={<AiOutlineReload />}
              size='sm'
              onClick={loadData}
            />
          </HStack>
        </DrawerHeader>
        <DrawerBody position='relative'>
          {loading && (
            <Box
              position='absolute'
              top='50%'
              left='50%'
              transform='translate(-50%, -50%)'
            >
              <Spinner size='lg' />
            </Box>
          )}

          {!loading && prices.length === 0 ? (
            <Box>
              <Box
                position='absolute'
                top='50%'
                left='50%'
                transform='translate(-50%, -50%)'
                textAlign='center'
                width='30%'
              >
                <Image src='/pricing.png' />
                <Text fontWeight='bold' mt='1rem'>
                  Price list is unavailable
                </Text>
              </Box>
            </Box>
          ) : (
            <Box>
              <Box mb='1rem'>
                <Text fontWeight='bold' fontSize='lg' mb='0.5rem'>
                  Laundromat
                </Text>
                <SimpleGrid columns={4} spacing={5}>
                  <FormControl>
                    <FormLabel fontSize='sm'>Wash and Dry (Self)</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isWashDrySelfInvalid}
                      errorBorderColor='red.500'
                      name='washDrySelf'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.washDrySelf}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>
                  <FormControl>
                    <FormLabel fontSize='sm'>Dry (Self)</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isDrySelfInvalid}
                      errorBorderColor='red.500'
                      name='drySelf'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.drySelf}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>
                  <FormControl>
                    <FormLabel fontSize='sm'>Wash and Dry (Dropoff)</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isWashDryDropoffInvalid}
                      errorBorderColor='red.500'
                      name='washDryDropoff'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.washDryDropoff}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>

                  <FormControl>
                    <FormLabel fontSize='sm'>Dry (Dropoff)</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isDryDropoffInvalid}
                      errorBorderColor='red.500'
                      name='dryDropOff'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.dryDropOff}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>

                  <FormControl>
                    <FormLabel fontSize='sm'>Iron (Dropoff)</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isIronDropOffInvalid}
                      errorBorderColor='red.500'
                      name='ironDropoff'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.ironDropoff}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>

                  <FormControl>
                    <FormLabel fontSize='sm'>Detergent</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isDetergentInvalid}
                      errorBorderColor='red.500'
                      name='detergent'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.detergent}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>

                  <FormControl>
                    <FormLabel fontSize='sm'>Stain Remover</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isStainRemoverInvalid}
                      errorBorderColor='red.500'
                      name='stainRemover'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.stainRemover}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>

                  <FormControl>
                    <FormLabel fontSize='sm'>Bleach</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isBleachInvalid}
                      errorBorderColor='red.500'
                      name='bleach'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.bleach}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>

                  <FormControl>
                    <FormLabel fontSize='sm'>Softner</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isSoftnerInvalid}
                      errorBorderColor='red.500'
                      name='softner'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.softner}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>
                </SimpleGrid>
              </Box>

              <Box mb='1rem'>
                <Text fontWeight='bold' fontSize='lg'>
                  Misc.
                </Text>
                <SimpleGrid columns={4} spacing={5}>
                  <FormControl>
                    <FormLabel fontSize='sm'>Delivery</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isDeliveryInvalid}
                      errorBorderColor='red.500'
                      name='delivery'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.delivery}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>

                  <FormControl>
                    <FormLabel fontSize='sm'>Laundry Bag</FormLabel>
                    <Input
                      type='number'
                      isInvalid={isLaundryBagInvalid}
                      errorBorderColor='red.500'
                      name='laundryBag'
                      variant='filled'
                      placeholder='Enter Price'
                      value={formData.laundryBag}
                      onChange={e =>
                        onChange(e.target.name, Number(e.target.value))
                      }
                    />
                  </FormControl>
                </SimpleGrid>
              </Box>

              <Box>
                <Text fontWeight='bold' fontSize='lg' mb='0.5rem'>
                  Ironing
                </Text>

                <SimpleGrid columns={2} mb='0.7rem'>
                  {formData.ironing.map(data => (
                    <HStack spacing={4} mx='0.6rem' mb='1rem' key={data._id}>
                      <FormControl
                        isInvalid={
                          data._id === isIroningInvalid.id &&
                          isIroningInvalid.error
                        }
                      >
                        <Input
                          type='text'
                          errorBorderColor='red.500'
                          variant='filled'
                          placeholder='Cloth'
                          value={data.name}
                          onChange={({ target: { value } }) =>
                            onIroningChange(data._id, 'cloth', value)
                          }
                        />
                      </FormControl>

                      <FormControl
                        isInvalid={
                          data._id === isIroningInvalid.id &&
                          isIroningInvalid.error
                        }
                      >
                        <NumberInput
                          defaultValue={1}
                          value={data.price}
                          min={1}
                          variant='filled'
                          onChange={value =>
                            onIroningChange(data._id, 'price', value)
                          }
                        >
                          <NumberInputField />
                        </NumberInput>
                      </FormControl>
                      <IconButton
                        aria-label='delete'
                        colorScheme='red'
                        variant='ghost'
                        icon={<BiTrash />}
                        size='sm'
                        onClick={() => onDeleteIroning(data._id)}
                      />
                    </HStack>
                  ))}
                </SimpleGrid>
                <Center>
                  <Button
                    px='1.5rem'
                    colorScheme='blue'
                    _focus={{ outline: 'none' }}
                    onClick={onAddIroning}
                  >
                    Add Ironing
                  </Button>
                </Center>
              </Box>
            </Box>
          )}
        </DrawerBody>
        <DrawerFooter>
          <Button variant='outline' mr={3} onClick={onClose}>
            Cancel
          </Button>
          <Button
            isDisabled={user?.role !== 'manager'}
            colorScheme='blue'
            onClick={update}
            isLoading={updateLoading}
          >
            Update
          </Button>
        </DrawerFooter>
      </DrawerContent>
    </Drawer>
  )
}

export default PricesDrawer
