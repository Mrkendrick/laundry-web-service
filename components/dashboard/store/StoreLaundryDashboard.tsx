import React, { useState, useEffect } from 'react'
import { Box, Text } from '@chakra-ui/layout'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { Image } from '@chakra-ui/image'
import { StoreLaundryTable } from '../LaundryTable'

type Props = {
  searchQuery: string
}

const StoreLaundryDashboard = ({ searchQuery }: Props) => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const { isAuthenticated, user, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { laundries } = useSelector(
    (state: RootState) => state.dashboardReducer,
  )
  const { _getLaundries, _getPrices } = bindActionCreators(appActions, dispatch)

  const loadData = async () => {
    if (isAuthenticated && user) {
      setLoading(true)
      await _getLaundries(undefined, user.role)
      await _getPrices()
      setLoading(false)
    }
  }

  useEffect(() => {
    loadData()
  }, [isAuthenticated, user])

  // TODO Add Spinner
  return !loading && laundries.data.length === 0 ? (
    <Box
      position='absolute'
      top='50%'
      left='50%'
      transform='translate(-50%, -50%)'
      textAlign='center'
    >
      <Image src='/emptyCart.png' w='75%' />
      <Text fontWeight='bold'>No laundry found</Text>
    </Box>
  ) : (
    <Box pt='1rem'>
      <StoreLaundryTable searchQuery={searchQuery} />
    </Box>
  )
}

export default StoreLaundryDashboard
