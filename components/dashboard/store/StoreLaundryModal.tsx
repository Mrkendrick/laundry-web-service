import React, { useState, useEffect } from 'react'
import { Button } from '@chakra-ui/button'
import { HStack } from '@chakra-ui/layout'
import { Input } from '@chakra-ui/input'
import { Checkbox, CheckboxGroup } from '@chakra-ui/checkbox'
import {
  NumberInputStepper,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInputField,
  NumberInput,
} from '@chakra-ui/number-input'
import { Select } from '@chakra-ui/select'
import { FormControl, FormLabel } from '@chakra-ui/form-control'
import {
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/modal'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { bindActionCreators } from 'redux'
import { useRouter } from 'next/router'
import appActions from '../../../redux/actions/app'
import { v4 } from 'uuid'
import UtilityService from '../../../utils/utils'

type Props = {
  onClose(): void
  isOpen: boolean
}

const StoreLaundryModal = ({ onClose, isOpen }: Props) => {
  const [formData, setFormData] = useState({
    service: '',
    serviceType: '',
    serviceQty: 1,
    iron: false,
    detergent: true,
    detergentQty: 1,
    laundryBag: true,
    laundryBagQty: 1,
    branchLocation: 'Ikeja',
    location: '',
    isPaid: false,
    isPicked: false,
  })
  const [owner, setOwner] = useState({ name: '', email: '', phone: '' })
  const [isNameInvalid, setIsNameInvalid] = useState(false)
  const [isEmailInvalid, setIsEmailInvalid] = useState(false)
  const [isPhoneInvalid, setIsPhoneInvalid] = useState(false)
  const [isServiceInvalid, setIsServiceInvalid] = useState(false)
  const [isServiceTypeInvalid, setIsServiceTypeInvalid] = useState(false)
  const [isLocationInvalid, setIsLocationInvalid] = useState(false)
  const { isAuthenticated, user } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const dispatch = useDispatch()
  const { _createLaundry } = bindActionCreators(appActions, dispatch)
  const router = useRouter()
  const currentTab = Number(router.query.tab)

  const onChange = (value: any, name: string, type: 'user' | 'store') => {
    if (type === 'user') {
      if (name === 'service') isServiceInvalid && setIsServiceInvalid(false)

      if (name === 'serviceType')
        isServiceTypeInvalid && setIsServiceTypeInvalid(false)

      if (name === 'location') isLocationInvalid && setIsLocationInvalid(false)

      setFormData({ ...formData, [name]: value })
    }

    if (type === 'store') {
      if (name === 'name') isNameInvalid && setIsNameInvalid(false)

      if (name === 'email') isEmailInvalid && setIsEmailInvalid(false)

      if (name === 'phone') isPhoneInvalid && setIsPhoneInvalid(false)

      setOwner({ ...owner, [name]: value })
    }
  }

  const validate = (): boolean | undefined => {
    let errors: boolean | undefined

    const { service, serviceType, location } = formData

    const { email, name, phone } = owner

    if (service === '') {
      errors = true
      setIsServiceInvalid(true)
    }

    if (serviceType === '') {
      errors = true
      setIsServiceTypeInvalid(true)
    }

    if (location === '' || location.length < 4) {
      errors = true
      setIsLocationInvalid(true)
    }

    if (!email || !email.match(UtilityService.emailRegex)) {
      errors = true
      setIsEmailInvalid(true)
    }

    if (!name) {
      errors = true
      setIsNameInvalid(true)
    }

    if (!phone) {
      errors = true
      setIsPhoneInvalid(true)
    }

    return errors
  }

  const submit = () => {
    const errors = validate()

    if (!errors) {
      const data = {
        ...formData,
        id: v4(),
        email: owner.email,
        location:
          formData.serviceType === 'delivery' ? formData.location : 'In Store',
        user: {
          name: owner.name,
          email: owner.email,
          phone: Number(owner.phone),
          address:
            formData.serviceType === 'delivery'
              ? formData.location
              : 'In Store',
        },
      }
      _createLaundry('save', data)
      router.push({
        pathname: `/dashboard/laundry/new`,
        query: {
          tab: currentTab,
        },
      })
    }
  }

  useEffect(() => {
    setFormData({ ...formData, location: user?.address ? user?.address : '' })
  }, [isAuthenticated, isOpen])

  return (
    <ModalContent px='0.6rem'>
      <ModalHeader fontWeight='normal'>New Laundry</ModalHeader>
      <ModalCloseButton _focus={{ outline: 'none' }} />

      <ModalBody>
        <HStack mb='1rem'>
          <FormControl>
            <Input
              type='text'
              autoCapitalize='true'
              errorBorderColor='red.500'
              isInvalid={isNameInvalid}
              _focus={{ outline: 'none' }}
              name='name'
              variant='filled'
              placeholder='Name'
              value={owner.name}
              onChange={e => onChange(e.target.value, 'name', 'store')}
            />
          </FormControl>
          <FormControl>
            <Input
              type='tel'
              errorBorderColor='red.500'
              isInvalid={isPhoneInvalid}
              _focus={{ outline: 'none' }}
              name='phone'
              variant='filled'
              placeholder='Phone'
              value={owner.phone}
              onChange={e => onChange(e.target.value, 'phone', 'store')}
            />
          </FormControl>
        </HStack>
        <FormControl mb='1rem'>
          <Input
            type='email'
            errorBorderColor='red.500'
            isInvalid={isEmailInvalid}
            _focus={{ outline: 'none' }}
            name='email'
            variant='filled'
            placeholder='Email'
            value={owner.email}
            onChange={e => onChange(e.target.value, 'email', 'store')}
          />
        </FormControl>
        <FormControl mb='1rem'>
          <Select
            isInvalid={isServiceInvalid}
            placeholder='Select Laundry'
            variant='filled'
            onChange={e => onChange(e.target.value, 'service', 'user')}
            value={formData.service}
          >
            <option value='Wash And Dry'>Wash And Dry</option>
            <option value='Dry'>Dry</option>
            <option value='Iron'>Iron</option>
          </Select>
        </FormControl>

        <FormControl mb='1rem'>
          <NumberInput
            value={formData.serviceQty}
            onChange={value => onChange(Number(value), 'serviceQty', 'user')}
            variant='filled'
            defaultValue={1}
            min={1}
            isDisabled={formData.service === 'Iron'}
            placeholder='Laundry Quantity'
          >
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
        </FormControl>

        <FormControl mb='1rem'>
          <Select
            isInvalid={isServiceTypeInvalid}
            placeholder='Select Type'
            variant='filled'
            onChange={e => onChange(e.target.value, 'serviceType', 'user')}
            value={formData.serviceType}
          >
            <option value='dropoff'>Drop Off</option>
            {formData.service !== 'Iron' && (
              <option value='self'>Self Service</option>
            )}
            <option value='delivery'>Delivery</option>
          </Select>
        </FormControl>

        <FormControl mb='1rem'>
          <Input
            type='text'
            autoCapitalize='true'
            errorBorderColor='red.500'
            isInvalid={isLocationInvalid}
            _focus={{ outline: 'none' }}
            name='location'
            variant='filled'
            placeholder='Address'
            value={formData.location}
            onChange={e => onChange(e.target.value, 'location', 'user')}
          />
        </FormControl>

        <FormControl mb='1rem'>
          <Checkbox
            isChecked={formData.iron || formData.service === 'Iron'}
            onChange={e => onChange(e.target.checked, 'iron', 'user')}
            isDisabled={formData.service === 'Iron'}
          >
            Iron
          </Checkbox>
        </FormControl>
        <HStack mb='1rem' spacing={0}>
          <FormControl>
            <Checkbox
              isChecked={formData.detergent}
              onChange={e => onChange(e.target.checked, 'detergent', 'user')}
            >
              Detergent
            </Checkbox>
          </FormControl>

          <FormControl>
            <NumberInput
              value={formData.detergentQty}
              isDisabled={!formData.detergent}
              onChange={value =>
                onChange(Number(value), 'detergentQty', 'user')
              }
              variant='filled'
              defaultValue={1}
              min={1}
              placeholder='Detergent Quantity'
            >
              <NumberInputField />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
          </FormControl>
        </HStack>

        <HStack mb='1rem' spacing={0}>
          <FormControl>
            <Checkbox
              isChecked={formData.laundryBag}
              onChange={e => onChange(e.target.checked, 'laundryBag', 'user')}
            >
              Laundry Bag
            </Checkbox>
          </FormControl>

          <FormControl>
            <NumberInput
              value={formData.laundryBagQty}
              isDisabled={!formData.laundryBag}
              onChange={value =>
                onChange(Number(value), 'laundryBagQty', 'user')
              }
              variant='filled'
              defaultValue={1}
              min={1}
              placeholder='Bag Quantity'
            >
              <NumberInputField />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
          </FormControl>
        </HStack>
      </ModalBody>

      <ModalFooter>
        <HStack spacing={4}>
          <Button
            _focus={{ outline: 'none' }}
            variant='ghost'
            onClick={onClose}
          >
            Cancel
          </Button>
          <Button
            _focus={{ outline: 'none' }}
            colorScheme='blue'
            mr={3}
            onClick={submit}
          >
            Next
          </Button>
        </HStack>
      </ModalFooter>
    </ModalContent>
  )
}

export default StoreLaundryModal
