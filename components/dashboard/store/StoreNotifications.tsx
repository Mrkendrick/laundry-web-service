import React, { useEffect, useState, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { RootState } from '../../../redux/reducers'
import { HStack, Box, Text, Center, SimpleGrid } from '@chakra-ui/layout'
import { Spinner } from '@chakra-ui/spinner'
import { Image } from '@chakra-ui/image'
import { Avatar, AvatarGroup } from '@chakra-ui/avatar'
import { Checkbox } from '@chakra-ui/checkbox'
import { IconButton, Button } from '@chakra-ui/button'
import { MdClear } from 'react-icons/md'
import { Textarea } from '@chakra-ui/textarea'
import { v4 } from 'uuid'
import UtilityService from '../../../utils/utils'
import Skip from '../../shared/Skip'

type Props = {
  searchQuery: string
}

const StoreNotifications = ({ searchQuery }: Props) => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [sendLoading, setSendLoading] = useState(false)
  const [selectedUsers, setSelectedUsers] = useState<
    { name: string; _id: string; photo: string }[]
  >([])
  const [message, setMessage] = useState('')
  const [isMessageInvalid, setIsMessageInvalid] = useState(false)
  const [isPrevLoading, setIsPrevLoaing] = useState(false)
  const [isNextLoading, setIsNextLoading] = useState(false)
  const {
    isAuthenticated,
    user: authUser,
    authLoading,
  } = useSelector((state: RootState) => state.authReducer)
  const { users } = useSelector((state: RootState) => state.dashboardReducer)
  const [pageCount, setPageCount] = useState(1)
  const { _getUsers, _sendNotification } = bindActionCreators(
    appActions,
    dispatch,
  )

  const templates = useMemo(
    () => [
      {
        id: v4(),
        template: 'Laundry is ready for pick up',
        background: 'yellow.300',
      },
      {
        id: v4(),
        template: 'Laundry has been delivered',
        background: 'green.200',
      },
      {
        id: v4(),
        template: 'Laundry has been confirmed',
        background: 'blue.100',
      },
      {
        id: v4(),
        template: 'Laundry has been cancelled',
        background: 'red.200',
      },
    ],
    [],
  )

  const isSelected = (id: string): boolean => {
    let selected: boolean

    const res = selectedUsers.find(selectedUser => selectedUser._id === id)

    res ? (selected = true) : (selected = false)

    return selected
  }

  const onCheck = (
    details: { name: string; _id: string; photo: string },
    action: 'add' | 'remove',
  ) => {
    action === 'add' && setSelectedUsers([...selectedUsers, details])
    action === 'remove' &&
      setSelectedUsers(
        selectedUsers.filter(selectedUser => selectedUser._id !== details._id),
      )
  }

  const validate = (): boolean | undefined => {
    let errors: boolean | undefined

    if (!selectedUsers) {
      errors = true
      UtilityService.toast({ title: 'Select A user', status: 'warning' })
    }

    if (message === '') {
      errors = true
      setIsMessageInvalid(true)
    }

    return errors
  }

  const sendMessage = async () => {
    const errors = validate()

    if (!errors) {
      setSendLoading(true)

      const data = {
        users: selectedUsers,
        message,
      }

      await _sendNotification(data, setSendLoading)
    }
  }

  const setPage = async (type: 'increment' | 'decrement') => {
    if (type === 'increment') {
      setIsNextLoading(true)
      setPageCount(prev => prev + 1)
      await _getUsers(searchQuery, setLoading, {
        page: pageCount + 1,
        limit: 5,
        callback: () => setIsNextLoading(false),
      })
    }

    if (type === 'decrement') {
      setIsPrevLoaing(true)
      setPageCount(prev => prev - 1)
      await _getUsers(searchQuery, setLoading, {
        page: pageCount - 1,
        limit: 5,
        callback: () => setIsPrevLoaing(false),
      })
    }
  }

  const loadData = async () => {
    if (isAuthenticated && authUser && authLoading === false) {
      setLoading(true)
      await _getUsers('', setLoading, { page: 1, limit: 5, callback: () => {} })
    }
  }

  useEffect(() => {
    loadData()
  }, [isAuthenticated, authUser, authLoading])
  return (
    <>
      {loading ? (
        <Box
          position='absolute'
          top='50%'
          left='50%'
          transform='translate(-50%, -50%)'
        >
          <Spinner size='xl' />
        </Box>
      ) : users.data.length === 0 ? (
        <Box
          position='absolute'
          top='50%'
          left='50%'
          transform='translate(-50%, -50%)'
          w='50%'
          textAlign='center'
        >
          <Image src='/nouser.png' />
          <Text fontWeight='bold'>No Users found</Text>
        </Box>
      ) : (
        <HStack mt='2rem'>
          <Box flex='0.4'>
            <Text fontWeight='bold' fontSize='lg'>
              Select Users
            </Text>

            <Box mt='1rem'>
              {users.data.map(user => (
                <HStack
                  key={user._id}
                  mb='1rem'
                  bg='gray.100'
                  transition='all ease 300ms'
                  p='0.5rem'
                  rounded='md'
                  _hover={{ shadow: 'lg' }}
                  justifyContent='space-between'
                  pr='1.5rem'
                >
                  <HStack spacing={4}>
                    <Avatar src={user.photo} name={user.name} />
                    <Text>{user.name}</Text>
                  </HStack>
                  <Checkbox
                    isChecked={isSelected(user._id)}
                    onChange={() =>
                      onCheck(
                        {
                          name: user.name,
                          _id: user._id,
                          photo: user.photo,
                        },
                        isSelected(user._id) ? 'remove' : 'add',
                      )
                    }
                    borderColor='gray.400'
                  />
                </HStack>
              ))}
            </Box>
            {users.totalCount > 5 && (
              <Box mt='2rem'>
                <Skip
                  onClick={setPage}
                  isNextLoading={isNextLoading}
                  isPrevLoading={isPrevLoading}
                  totalCount={users.totalCount}
                  itemsPerPage={5}
                  pageCount={pageCount}
                />
              </Box>
            )}
          </Box>

          <Box flex='1' h='full' position='relative'>
            {selectedUsers.length === 0 ? (
              <Box
                position='absolute'
                top='50%'
                left='50%'
                transform='translate(-50%, -50%)'
                textAlign='center'
              >
                <Image src='/nouser.png' />
                <Text fontWeight='bold'>No user selected</Text>
              </Box>
            ) : (
              <>
                <Center my='1rem'>
                  <HStack spacing={6}>
                    <AvatarGroup max={3} size='lg'>
                      {selectedUsers.map(selectedUser => (
                        <Avatar
                          name={selectedUser.name}
                          key={selectedUser._id}
                          src={selectedUser.photo}
                        />
                      ))}
                    </AvatarGroup>

                    <IconButton
                      colorScheme='red'
                      variant='solid'
                      _focus={{ outline: 'none' }}
                      size='sm'
                      aria-label='clear'
                      icon={<MdClear />}
                      onClick={() => setSelectedUsers([])}
                    />
                  </HStack>
                </Center>
                <Box w='max' mx='auto'>
                  <Textarea
                    w='full'
                    placeholder='Enter push notification message'
                    isInvalid={isMessageInvalid}
                    _focus={{ outline: 'none' }}
                    size='lg'
                    value={message}
                    onChange={e => {
                      isMessageInvalid && setIsMessageInvalid(false)
                      setMessage(e.target.value)
                    }}
                    mb='1rem'
                  />
                  <Box>
                    <SimpleGrid columns={2} spacing={4} mb='2rem'>
                      {templates.map(temp => (
                        <Box
                          key={temp.id}
                          bg={temp.background}
                          w='max'
                          p='0.5rem'
                          transition='all ease-in-out 300ms'
                          rounded='md'
                          _hover={{ cursor: 'pointer', shadow: 'md' }}
                          onClick={() => setMessage(prev => temp.template)}
                        >
                          <Text fontSize='sm'>{temp.template}</Text>
                        </Box>
                      ))}
                    </SimpleGrid>
                  </Box>
                  <Button
                    colorScheme='blue'
                    size='lg'
                    w='full'
                    onClick={sendMessage}
                    isLoading={sendLoading}
                  >
                    Send Notification
                  </Button>
                </Box>
              </>
            )}
          </Box>
        </HStack>
      )}
    </>
  )
}

export default StoreNotifications
