import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../redux/reducers'
import { Avatar } from '@chakra-ui/avatar'
import { Table, Thead, Tbody, Tr, Th, Td } from '@chakra-ui/table'
import { Text, HStack } from '@chakra-ui/layout'
import { IoIosMale, IoIosFemale } from 'react-icons/io'
import { Icon } from '@chakra-ui/icon'
import { Badge, Center } from '@chakra-ui/layout'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { useDisclosure } from '@chakra-ui/react'
import UserModal from './UserModal'
import Pagination from '../../shared/Pagination'

type Props = {
  searchQuery: string
  loadData(): Promise<void>
}

const UsersTable = ({ loadData, searchQuery }: Props) => {
  const dispatch = useDispatch()
  const { users } = useSelector((state: RootState) => state.dashboardReducer)
  const { _getUser } = bindActionCreators(appActions, dispatch)
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { _getUsers } = bindActionCreators(appActions, dispatch)

  const onRowClick = (id: string) => {
    const user = users.data.find(user => user._id === id)
    _getUser(user)
    onOpen()
  }

  const setPage = async (num: number, callback: () => void) => {
    const options = {
      page: num,
      limit: 10,
      callback,
    }

    await _getUsers(searchQuery, undefined, options)
  }

  return (
    <>
      <UserModal loadData={loadData} isOpen={isOpen} onClose={onClose} />
      <Table variant='simple' size='sm'>
        <Thead>
          <Tr>
            <Th>Name</Th>
            <Th>Email</Th>
            <Th>Phone</Th>
            <Th>Address</Th>
            <Th>Gender</Th>
            <Th>Role</Th>
          </Tr>
        </Thead>
        <Tbody>
          {users.data.map(user => (
            <Tr
              key={user._id}
              _hover={{ cursor: 'pointer', background: 'gray.50' }}
              transition='background ease 300ms'
              onClick={() => onRowClick(user._id)}
            >
              <Td>
                <HStack spacing={3}>
                  <Avatar size='sm' name={user.name} src={user.photo} />
                  <Text>{user.name}</Text>
                </HStack>
              </Td>
              <Td>{user.email}</Td>
              <Td>{user.phone ? user.phone : 'N/A'}</Td>
              <Td>{user.address ? user.address : 'N/A'}</Td>
              <Td>
                {user.gender === 'male' ? (
                  <Icon as={IoIosMale} fontSize='1.3rem' />
                ) : user.gender === 'female' ? (
                  <Icon as={IoIosFemale} fontSize='1.3rem' />
                ) : (
                  'N/A'
                )}
              </Td>
              <Td>
                {user.role === 'user' ? (
                  <Badge fontSize='0.6rem' colorScheme='blue'>
                    customer
                  </Badge>
                ) : (
                  <Badge
                    fontSize='0.6rem'
                    colorScheme={
                      user.role === 'attendant'
                        ? 'green'
                        : user.role === 'dispatch'
                        ? 'yellow'
                        : user.role === 'manager'
                        ? 'red'
                        : 'red'
                    }
                  >
                    {user.role}
                  </Badge>
                )}
              </Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
      <Center mt='2rem'>
        {users.totalCount > 10 && (
          <Pagination
            count={users.totalCount}
            itemsPerPage={users.count}
            setPage={setPage}
          />
        )}
      </Center>
    </>
  )
}

export default UsersTable
