import router, { useRouter } from 'next/router'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'
import appActions from '../../../redux/actions/app'
import { RootState } from '../../../redux/reducers'
import { Box, HStack, Text, Center } from '@chakra-ui/layout'
import { Image } from '@chakra-ui/image'
import { Spinner } from '@chakra-ui/spinner'
import Head from 'next/head'
import SideBar from '../../../components/dashboard/SideBar'
import NoAuth from '../../../components/dashboard/NoAuth'
import { Avatar } from '@chakra-ui/avatar'
import { useMemo } from 'react'
import { Select } from '@chakra-ui/select'
import {
  NumberInput,
  NumberInputField,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInputStepper,
} from '@chakra-ui/number-input'
import { FormControl, FormLabel } from '@chakra-ui/form-control'
import { Checkbox } from '@chakra-ui/checkbox'
import { Radio, RadioGroup } from '@chakra-ui/radio'
import { Button } from '@chakra-ui/button'
import AlertDialog from '../../shared/AlertDialog'
import { useDisclosure } from '@chakra-ui/react'
import { IconButton } from '@chakra-ui/button'
import { BiPlus, BiTrash } from 'react-icons/bi'
import { GrAdd } from 'react-icons/gr'
import { v4 } from 'uuid'
import UtilityService from '../../../utils/utils'
import CheckOption from '../../shared/CheckOption'

const StoreLaundry = () => {
  const dispatch = useDispatch()
  const { isOpen, onClose, onOpen } = useDisclosure()
  const { query, back } = useRouter()
  const [loading, setLoading] = useState(false)
  const [isUpdated, setIsUpdated] = useState(false)
  const [updateLoading, setUpdateLoading] = useState(false)
  const [laundryDetails, setLaundryDetails] = useState<
    { _id: string; fabric: string; count: number }[]
  >([])
  const [formData, setFormData] = useState({
    detergent: false,
    detergentQty: 0,
    email: '',
    extraCareOpt: [{ _id: '', product: '', quantity: 0 }],
    iron: false,
    isDelivered: false,
    isPaid: false,
    isPicked: false,
    laundryBag: false,
    laundryBagQty: 0,
    totalCount: 0,
    location: '',
    paymentMethod: '',
    progress: '',
    service: '',
    serviceQty: 0,
    serviceType: '',
    subTotal: 0,
    total: 0,
    _id: '',
  })
  const [isServiceInvalid, setIsServiceInvalid] = useState(false)
  const [isServiceTypeInvalid, setIsServiceTypeInvalid] = useState(false)
  const [isTotalCountInvalid, setIsTotalCountInvalid] = useState(false)

  const { laundry, cloths } = useSelector(
    (state: RootState) => state.dashboardReducer,
  )
  const { user, isAuthenticated, authLoading } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { prices } = useSelector((state: RootState) => state.appReducer)
  const { _getLaundry, _getPrices, _updateLaundry, _getCloths } =
    bindActionCreators(appActions, dispatch)

  const onChange = (name: string, value: any) => {
    if (name === 'service') isServiceInvalid && setIsServiceInvalid(false)

    if (name === 'serviceType')
      isServiceTypeInvalid && setIsServiceTypeInvalid(false)

    if (name === 'totalCount')
      isTotalCountInvalid && setIsTotalCountInvalid(false)

    setFormData({ ...formData, [name]: value })
  }

  const onExtraOptionsChange = (
    product: string,
    type: 'option' | 'quantity',
    value?: number,
  ) => {
    let extraCareOptions = [...formData.extraCareOpt]

    if (type === 'option') {
      const option = extraCareOptions.find(option => option.product === product)

      if (option) {
        const newExtraCareOptions = extraCareOptions.filter(
          option => option.product !== product,
        )
        setFormData({ ...formData, extraCareOpt: newExtraCareOptions })
      } else {
        const newExtraCareOptions = [
          ...extraCareOptions,
          { _id: v4(), product, quantity: 1 },
        ]
        setFormData({ ...formData, extraCareOpt: newExtraCareOptions })
      }
    }

    if (type === 'quantity') {
      const newExtraCareOption = extraCareOptions.map(option =>
        option.product === product
          ? { ...option, quantity: value ? value : 1 }
          : option,
      )

      setFormData({ ...formData, extraCareOpt: newExtraCareOption })
    }
  }

  const addDetailsField = () =>
    setLaundryDetails([...laundryDetails, { _id: v4(), fabric: '', count: 1 }])

  const removeDetailsField = (id: string) =>
    setLaundryDetails(laundryDetails.filter(details => details._id !== id))

  const onDetailsFieldChange = (id: string, fabric: string, count: number) =>
    setLaundryDetails(
      laundryDetails.map(details =>
        details._id === id ? { _id: id, fabric, count } : details,
      ),
    )

  const calculate = () => {
    let total: number[] = []
    let sub: number[] = []

    if (!prices || prices.length === 0 || !formData.service) return

    const {
      bleach,
      softner,
      laundryBag,
      delivery,
      drySelf,
      dryDropOff,
      washDryDropoff,
      washDrySelf,
      ironDropoff,
      stainRemover,
      iron,
      ironing,
      detergent,
    } = prices[0]

    if (formData.serviceType === 'dropoff') {
      if (formData.service === 'Dry') {
        sub = [...sub, dryDropOff]
        total = [...total, dryDropOff * formData.serviceQty]
      }

      if (formData.service === 'Wash And Dry') {
        sub = [...sub, washDryDropoff]
        total = [...total, washDryDropoff * formData.serviceQty]
      }

      if (formData.service === 'Iron') {
        sub = [...sub, ironDropoff]
        total = [...total, ironDropoff]
      }
    }

    if (formData.serviceType === 'delivery') {
      total = [...total, delivery]
      sub = [...sub, delivery]
    }

    if (formData.serviceType === 'self') {
      if (formData.service === 'Dry') {
        total = [...total, drySelf * formData.serviceQty]
        sub = [...sub, drySelf]
      }

      if (formData.service === 'Wash And Dry') {
        total = [...total, washDrySelf * formData.serviceQty]
        sub = [...sub, washDrySelf]
      }
    }

    if (formData.iron || formData.service === 'Iron') {
      laundryDetails.length > 0 &&
        laundryDetails.forEach(detail => {
          ironing.forEach(price => {
            if (price.key === detail.fabric) {
              total = [...total, price.price * detail.count]
              sub = [...total, price.price]
            }
          })
        })
    }

    if (formData.laundryBag) {
      total = [...total, laundryBag * formData.laundryBagQty]
      sub = [...sub, laundryBag]
    }

    if (formData.detergent) {
      total = [...total, detergent * formData.detergentQty]
      sub = [...sub, detergent]
    }

    if (formData.extraCareOpt.length > 0) {
      const { extraCareOpt } = formData

      extraCareOpt.forEach(option => {
        if (option.product === 'Softner') {
          total = [...total, softner * option.quantity]
          sub = [...sub, softner]
        }

        if (option.product === 'Bleach') {
          total = [...total, bleach * option.quantity]
          sub = [...sub, bleach]
        }

        if (option.product === 'Stain Remover') {
          total = [...total, stainRemover * option.quantity]
          sub = [...sub, stainRemover]
        }
      })
    }

    if (sub.length > 0 && total.length > 0) {
      const newSub = sub.reduce((acc, curr) => acc + curr)
      const newTotal = total.reduce((acc, curr) => acc + curr)

      setFormData({
        ...formData,
        subTotal: newSub,
        total: newTotal,
      })

      return { newSub, newTotal }
    }
  }

  const onSearch = () => {}

  const onPrint = () => {
    calculate()
    window.print()
  }

  const onCancel = () => {
    onClose()
    back()
  }

  const isChecked = (product: string): boolean => {
    const prod = formData.extraCareOpt.find(
      option => option.product === product,
    )

    return prod ? true : false
  }

  const productCount = (product: string): number => {
    const prod = formData.extraCareOpt.find(
      option => option.product === product,
    )

    return prod ? prod.quantity : 1
  }

  const isProductDisabled = (product: string): boolean => {
    const prod = formData.extraCareOpt.find(
      option => option.product === product,
    )

    return prod ? false : true
  }

  const validate = (): boolean | undefined => {
    let errors: boolean | undefined

    const { service, serviceType, totalCount } = formData

    if (service === '') {
      errors = true
      setIsServiceInvalid(true)
    }

    if (serviceType === '') {
      errors = true
      setIsServiceTypeInvalid(true)
    }

    if (totalCount === 0 || '') {
      errors = true
      setIsTotalCountInvalid(true)
    }

    laundryDetails.length > 0 &&
      laundryDetails.every(details => {
        if (
          details.fabric === '' ||
          details.count < 1 ||
          typeof details.count !== 'number'
        ) {
          errors = true
          UtilityService.toast({
            title: 'Check laundry details',
            status: 'error',
          })
          return
        }
      })

    return errors
  }

  const onUpdate = async () => {
    const errors = validate()

    if (!errors) {
      setUpdateLoading(true)
      const calculatedResult = calculate()

      const data = {
        ...formData,
        laundryDetails: [
          ...laundryDetails.map(details => ({
            fabric: details.fabric,
            count: details.count,
          })),
        ],
        extraCareOpt: [
          ...formData.extraCareOpt.map(option => ({
            product: option.product,
            quantity: option.quantity,
          })),
        ],
        subTotal: calculatedResult?.newSub,
        total: calculatedResult?.newTotal,
      }

      await _updateLaundry(laundry._id, data, 'attendant', setUpdateLoading)
      setIsUpdated(true)
    }
  }

  const loadData = async () => {
    if (query.id) {
      setLoading(true)
      await Promise.all([
        _getPrices(),
        _getCloths(),
        _getLaundry(query.id.toString(), user?.role),
      ])
      setLoading(false)
    }
  }

  const loadFields = () => {
    if (laundry._id !== '') {
      setFormData({
        ...formData,
        ...laundry,
      })
      setLaundryDetails([...laundry.laundryDetails])
    }
  }

  useMemo(() => {
    loadFields()
  }, [laundry])

  useEffect(() => {
    loadData()
  }, [query])

  return (
    <>
      <Head>
        <title>Dashboard | Laundry</title>
      </Head>
      <AlertDialog
        onYesClick={onCancel}
        onNoClick={onClose}
        heading='Discard Changes'
        onClose={onClose}
        isOpen={isOpen}
        body='Are you sure?'
        type='prompt'
      />
      <HStack alignItems='flex-start'>
        <SideBar callback={onSearch} />
        <Box height='100vh' width='75%' position='relative'>
          <NoAuth />
          {authLoading === false && user && isAuthenticated && (
            <Box>
              {loading && (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                >
                  <Spinner size='xl' />
                </Box>
              )}
              {!loading && laundry._id === '' ? (
                <Box
                  position='absolute'
                  top='50%'
                  left='50%'
                  transform='translate(-50%, -50%)'
                  textAlign='center'
                >
                  <Image src='/emptyCart.png' w='75%' />
                  <Text fontWeight='bold'>No laundry found</Text>
                </Box>
              ) : (
                <HStack
                  padding='1rem'
                  alignItems='stretch'
                  justifyContent='stretch'
                >
                  <Box width='50%'>
                    <HStack spacing={4} mb='0.7rem'>
                      <Avatar
                        src={laundry.user.photo}
                        name={laundry.user.name}
                        size='lg'
                      />
                      <Box>
                        <Text fontWeight='bold'>{laundry.user.name}</Text>
                        <Text fontSize='sm'>{laundry.user.email}</Text>
                      </Box>
                    </HStack>
                    <Text mb='0.6rem' fontSize='sm'>
                      Phone: {laundry.user.phone}
                    </Text>
                    <Text mb='0.7rem' fontSize='sm'>
                      Address: {laundry.location}
                    </Text>
                    <Box w='70%' mb='1rem'>
                      <FormControl>
                        <Select
                          value={formData.serviceType}
                          isInvalid={isServiceTypeInvalid}
                          placeholder='Select Service Type'
                          variant='filled'
                          onChange={e =>
                            onChange('serviceType', e.target.value)
                          }
                          _focus={{ outline: 'none' }}
                        >
                          <option value='dropoff'>Drop Off</option>
                          {formData.service !== 'Iron' && (
                            <option value='self'>Self Service</option>
                          )}
                          <option value='delivery'>Delivery</option>
                        </Select>
                      </FormControl>
                    </Box>
                    <HStack w='70%' mb='1rem'>
                      <FormControl>
                        <Select
                          value={formData.service}
                          isInvalid={isServiceInvalid}
                          placeholder='Select Laundry'
                          variant='filled'
                          onChange={e => onChange('service', e.target.value)}
                          _focus={{ outline: 'none' }}
                        >
                          <option value='Wash And Dry'>Wash And Dry</option>
                          <option value='Dry'>Dry</option>
                          <option value='Iron'>Iron</option>
                        </Select>
                      </FormControl>

                      <FormControl>
                        <NumberInput
                          defaultValue={1}
                          value={formData.serviceQty}
                          min={1}
                          variant='filled'
                          onChange={value =>
                            onChange('serviceQty', Number(value))
                          }
                        >
                          <NumberInputField _focus={{ outline: 'none' }} />
                          <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                          </NumberInputStepper>
                        </NumberInput>
                      </FormControl>
                    </HStack>
                    <CheckOption
                      isChecked={formData.detergent}
                      onChange={onChange}
                      isDisabled={formData.detergent}
                      checkboxName='detergent'
                      option='Detergent'
                      numberInputName='detergentQty'
                      numberValue={formData.detergentQty}
                    />
                    <CheckOption
                      isChecked={formData.laundryBag}
                      onChange={onChange}
                      isDisabled={formData.laundryBag}
                      checkboxName='laundryBag'
                      option='Laundry Bag'
                      numberInputName='laundryBagQty'
                      numberValue={formData.laundryBagQty}
                    />

                    <Box mb='1rem'>
                      <HStack w='70%' mb='1rem'>
                        <FormControl>
                          <Checkbox
                            isChecked={isChecked('Bleach')}
                            onChange={e =>
                              onExtraOptionsChange('Bleach', 'option')
                            }
                          >
                            Bleach
                          </Checkbox>
                        </FormControl>

                        <FormControl>
                          <NumberInput
                            isDisabled={isProductDisabled('Bleach')}
                            defaultValue={1}
                            value={productCount('Bleach')}
                            min={1}
                            variant='filled'
                            onChange={value =>
                              onExtraOptionsChange(
                                'Bleach',
                                'quantity',
                                Number(value),
                              )
                            }
                          >
                            <NumberInputField _focus={{ outline: 'none' }} />
                            <NumberInputStepper>
                              <NumberIncrementStepper />
                              <NumberDecrementStepper />
                            </NumberInputStepper>
                          </NumberInput>
                        </FormControl>
                      </HStack>
                      <HStack w='70%' mb='1rem'>
                        <FormControl>
                          <Checkbox
                            isChecked={isChecked('Softner')}
                            onChange={e =>
                              onExtraOptionsChange('Softner', 'option')
                            }
                          >
                            Softner
                          </Checkbox>
                        </FormControl>

                        <FormControl>
                          <NumberInput
                            isDisabled={isProductDisabled('Softner')}
                            defaultValue={1}
                            value={productCount('Softner')}
                            min={1}
                            variant='filled'
                            onChange={value =>
                              onExtraOptionsChange(
                                'Softner',
                                'quantity',
                                Number(value),
                              )
                            }
                          >
                            <NumberInputField _focus={{ outline: 'none' }} />
                            <NumberInputStepper>
                              <NumberIncrementStepper />
                              <NumberDecrementStepper />
                            </NumberInputStepper>
                          </NumberInput>
                        </FormControl>
                      </HStack>
                      <HStack w='70%' mb='1rem'>
                        <FormControl>
                          <Checkbox
                            isChecked={isChecked('Stain Remover')}
                            onChange={e =>
                              onExtraOptionsChange('Stain Remover', 'option')
                            }
                          >
                            Stain Remover
                          </Checkbox>
                        </FormControl>

                        <FormControl>
                          <NumberInput
                            isDisabled={isProductDisabled('Stain Remover')}
                            defaultValue={1}
                            value={productCount('Stain Remover')}
                            min={1}
                            variant='filled'
                            onChange={value =>
                              onExtraOptionsChange('Stain Remover', 'quantity')
                            }
                          >
                            <NumberInputField _focus={{ outline: 'none' }} />
                            <NumberInputStepper>
                              <NumberIncrementStepper />
                              <NumberDecrementStepper />
                            </NumberInputStepper>
                          </NumberInput>
                        </FormControl>
                      </HStack>
                    </Box>
                    <FormControl mb='1rem'>
                      <FormLabel>Progress</FormLabel>
                      <RadioGroup
                        name='progress'
                        onChange={val => onChange('progress', val)}
                        value={formData.progress}
                      >
                        <HStack spacing={4}>
                          <Radio
                            value='pending'
                            colorScheme='yellow'
                            _focus={{ outline: 'none' }}
                          >
                            Pending
                          </Radio>
                          <Radio
                            value='washing'
                            colorScheme='blue'
                            _focus={{ outline: 'none' }}
                          >
                            Washing
                          </Radio>
                          <Radio
                            value='delivered'
                            colorScheme='green'
                            _focus={{ outline: 'none' }}
                          >
                            Delivered
                          </Radio>
                          <Radio
                            value='cancelled'
                            colorScheme='red'
                            _focus={{ outline: 'none' }}
                          >
                            Cancelled
                          </Radio>
                        </HStack>
                      </RadioGroup>
                    </FormControl>

                    <FormControl mb='1rem'>
                      <FormLabel>Payment Method</FormLabel>
                      <RadioGroup
                        name='paymentMethod'
                        onChange={val => onChange('paymentMethod', val)}
                        value={formData.paymentMethod}
                      >
                        <HStack spacing={4}>
                          <Radio
                            value='card'
                            colorScheme='yellow'
                            _focus={{ outline: 'none' }}
                          >
                            Card
                          </Radio>
                          <Radio
                            value='pos'
                            colorScheme='blue'
                            _focus={{ outline: 'none' }}
                          >
                            POS
                          </Radio>
                          <Radio
                            value='cash'
                            colorScheme='green'
                            _focus={{ outline: 'none' }}
                          >
                            Cash
                          </Radio>
                          <Radio
                            value='transfer'
                            colorScheme='cyan'
                            _focus={{ outline: 'none' }}
                          >
                            Transfer
                          </Radio>
                        </HStack>
                      </RadioGroup>
                    </FormControl>

                    <HStack spacing={6}>
                      <Checkbox
                        isChecked={formData.isPaid}
                        onChange={e => onChange('isPaid', e.target.checked)}
                      >
                        Paid
                      </Checkbox>
                      <Checkbox
                        isChecked={formData.isDelivered}
                        onChange={e =>
                          onChange('isDelivered', e.target.checked)
                        }
                      >
                        Delivered
                      </Checkbox>
                      <Checkbox
                        isChecked={formData.iron}
                        onChange={e => onChange('iron', e.target.checked)}
                      >
                        Iron
                      </Checkbox>
                    </HStack>
                  </Box>
                  <Box width='52%'>
                    <HStack justifyContent='space-between' mb='1rem'>
                      <Box>
                        <Text fontWeight='bold' fontSize='2xl'>
                          Total: #{UtilityService.formatPrice(formData.total)}
                        </Text>
                      </Box>
                      <HStack justifyContent='flex-end' spacing={5}>
                        {!isUpdated ? (
                          <Button
                            _focus={{ outline: 'none' }}
                            variant='ghost'
                            colorScheme='gray'
                            onClick={onOpen}
                          >
                            Discard
                          </Button>
                        ) : (
                          <Button
                            _focus={{ outline: 'none' }}
                            variant='ghost'
                            colorScheme='gray'
                            onClick={() =>
                              router.push({
                                pathname: `/dashboard/laundry`,
                                query: { tab: 212 },
                              })
                            }
                          >
                            Go Back
                          </Button>
                        )}
                        <Button
                          _focus={{ outline: 'none' }}
                          variant='outline'
                          colorScheme='blue'
                          onClick={onPrint}
                        >
                          Print
                        </Button>
                        <Button
                          _focus={{ outline: 'none' }}
                          colorScheme='blue'
                          onClick={onUpdate}
                          isLoading={updateLoading}
                        >
                          Update
                        </Button>
                      </HStack>
                    </HStack>
                    <Box>
                      <Text fontSize='lg' fontWeight='bold' mb='1rem'>
                        Ironing details
                      </Text>
                      {laundryDetails.length === 0 ? (
                        <Box textAlign='center' mt='2rem'>
                          <Button
                            leftIcon={<BiPlus />}
                            _focus={{ outline: 'none' }}
                            onClick={addDetailsField}
                          >
                            Add Details
                          </Button>
                        </Box>
                      ) : (
                        <Box>
                          {laundryDetails.map(details => (
                            <HStack w='70%' mb='1rem' key={details._id}>
                              <FormControl>
                                <Select
                                  value={details.fabric}
                                  placeholder='Select Cloth'
                                  variant='filled'
                                  onChange={e =>
                                    onDetailsFieldChange(
                                      details._id,
                                      e.target.value,
                                      details.count,
                                    )
                                  }
                                  _focus={{ outline: 'none' }}
                                >
                                  {cloths.length > 0 &&
                                    cloths.map(cloth => (
                                      <option key={cloth._id} value={cloth.key}>
                                        {cloth.cloth}
                                      </option>
                                    ))}
                                </Select>
                              </FormControl>

                              <FormControl>
                                <NumberInput
                                  defaultValue={details.count}
                                  value={details.count}
                                  min={1}
                                  variant='filled'
                                  onChange={val =>
                                    onDetailsFieldChange(
                                      details._id,
                                      details.fabric,
                                      Number(val),
                                    )
                                  }
                                >
                                  <NumberInputField
                                    _focus={{ outline: 'none' }}
                                  />
                                  <NumberInputStepper>
                                    <NumberIncrementStepper />
                                    <NumberDecrementStepper />
                                  </NumberInputStepper>
                                </NumberInput>
                              </FormControl>
                              <IconButton
                                aria-label='delete'
                                colorScheme='red'
                                variant='ghost'
                                icon={<BiTrash />}
                                size='sm'
                                _focus={{ outline: 'none' }}
                                onClick={() => removeDetailsField(details._id)}
                              />
                            </HStack>
                          ))}
                          <Center w='70%'>
                            <IconButton
                              textAlign='center'
                              aria-label='add'
                              icon={<GrAdd />}
                              size='sm'
                              _focus={{ outline: 'none' }}
                              onClick={addDetailsField}
                              isRound
                            />
                          </Center>
                        </Box>
                      )}
                    </Box>
                    <Box mt='1rem'>
                      <Text mb='1rem' fontWeight='bold'>
                        Total Clothes Count
                      </Text>
                      <Box w='50%'>
                        <FormControl>
                          <NumberInput
                            defaultValue={formData.totalCount}
                            value={formData.totalCount}
                            min={1}
                            variant='filled'
                            onChange={val =>
                              onChange('totalCount', Number(val))
                            }
                            isInvalid={isTotalCountInvalid}
                            errorBorderColor='red.500'
                          >
                            <NumberInputField _focus={{ outline: 'none' }} />
                            <NumberInputStepper>
                              <NumberIncrementStepper />
                              <NumberDecrementStepper />
                            </NumberInputStepper>
                          </NumberInput>
                        </FormControl>
                      </Box>
                    </Box>
                  </Box>
                </HStack>
              )}
            </Box>
          )}
        </Box>
      </HStack>
    </>
  )
}

export default StoreLaundry
