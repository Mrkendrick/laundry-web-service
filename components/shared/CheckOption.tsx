import React, { memo } from 'react'
import { HStack } from '@chakra-ui/layout'
import { Checkbox } from '@chakra-ui/checkbox'
import { FormControl } from '@chakra-ui/form-control'
import {
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberDecrementStepper,
  NumberIncrementStepper,
} from '@chakra-ui/number-input'

type Props = {
  isDisabled: boolean
  numberValue: number
  isChecked: boolean
  onChange(name: string, value: any): void
  option: string
  checkboxName: string
  numberInputName: string
}

const CheckOption = memo(
  ({
    isDisabled,
    isChecked,
    numberValue,
    onChange,
    option,
    checkboxName,
    numberInputName,
  }: Props) => {
    return (
      <HStack w='70%' mb='1rem'>
        <FormControl>
          <Checkbox
            isChecked={isChecked}
            onChange={e => onChange(checkboxName, e.target.checked)}
          >
            {option}
          </Checkbox>
        </FormControl>

        <FormControl>
          <NumberInput
            isDisabled={!isDisabled}
            defaultValue={1}
            value={numberValue}
            min={1}
            variant='filled'
            onChange={value => onChange(numberInputName, Number(value))}
          >
            <NumberInputField _focus={{ outline: 'none' }} />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
        </FormControl>
      </HStack>
    )
  },
)

export default CheckOption
