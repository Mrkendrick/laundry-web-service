import React, { memo } from 'react'
import { HStack } from '@chakra-ui/layout'
import { IconButton } from '@chakra-ui/button'
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa'

type Props = {
  onClick(type: 'increment' | 'decrement'): void
  isPrevLoading: boolean
  isNextLoading: boolean
  totalCount: number
  itemsPerPage: number
  pageCount: number
}

const Skip = memo(
  ({
    onClick,
    isNextLoading,
    isPrevLoading,
    totalCount,
    itemsPerPage,
    pageCount,
  }: Props) => {
    return (
      <HStack spacing={4}>
        <IconButton
          onClick={() => onClick('decrement')}
          icon={<FaChevronLeft />}
          aria-label='prev'
          isLoading={isPrevLoading}
          variant='solid'
          colorScheme='blue'
          _focus={{ outline: 'none' }}
          isDisabled={pageCount <= 1}
        />
        <IconButton
          onClick={() => onClick('increment')}
          icon={<FaChevronRight />}
          aria-label='next'
          isLoading={isNextLoading}
          variant='solid'
          colorScheme='blue'
          _focus={{ outline: 'none' }}
          isDisabled={pageCount >= Math.ceil(totalCount / itemsPerPage)}
        />
      </HStack>
    )
  },
)

export default Skip
