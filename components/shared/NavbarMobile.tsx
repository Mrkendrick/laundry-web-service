import { Box } from '@chakra-ui/layout'
import React from 'react'
import { CgMenuLeftAlt } from 'react-icons/cg'
import { IconButton } from '@chakra-ui/button'
import {
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerOverlay,
} from '@chakra-ui/modal'
import { useDisclosure } from '@chakra-ui/hooks'
import { LinksMobile } from './Links'

type Props = {
  links: {
    name: string
    key: string
  }[]
}

const NavbarMobile = ({ links }: Props) => {
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <Box>
      <IconButton
        variant='ghost'
        _focus={{ outline: 'none' }}
        aria-label='menu'
        icon={<CgMenuLeftAlt fontSize='2.5rem' />}
        onClick={onOpen}
      />

      <Drawer isOpen={isOpen} placement='right' onClose={onClose}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton size='lg' _focus={{ outline: 'none' }} />

          <DrawerBody>
            <LinksMobile onClose={onClose} links={links} />
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </Box>
  )
}

export default NavbarMobile
