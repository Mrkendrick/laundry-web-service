import { Box, Flex, Link } from '@chakra-ui/layout'
import { Image } from '@chakra-ui/image'
import NextLink from 'next/link'
import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'
import { Links } from './Links'
import NavbarMobile from './NavbarMobile'

const Navbar = () => {
  const { isMobile } = useSelector((state: RootState) => state.appReducer)

  const links = useMemo(
    () => [
      {
        name: 'Our Process',
        key: '#our_process',
      },

      {
        name: 'Services',
        key: '#services',
      },

      {
        name: 'Pricing',
        key: '#pricing',
      },

      {
        name: 'Our Location',
        key: '#our_location',
      },

      {
        name: 'Contact Us',
        key: '#contact_us',
      },
    ],
    [],
  )

  return (
    <Box
      width='100%'
      backgroundColor='white'
      position='sticky'
      as='nav'
      color='gray.700'
      top='0'
      zIndex='1000'
      paddingX={{ base: '2rem', md: '3rem', lg: '5rem' }}
      paddingY='1.5rem'
      shadow='lg'
    >
      <Flex alignItems='center' justifyContent='space-between'>
        <NextLink href='/'>
          <Link textDecor='none'>
            <Image
              src={isMobile ? '/icon.png' : '/logo.png'}
              alt='logo'
              width='162'
              height='50'
              _hover={{ cursor: 'pointer' }}
            />
          </Link>
        </NextLink>

        <Box>
          {isMobile ? <NavbarMobile links={links} /> : <Links links={links} />}
        </Box>
      </Flex>
    </Box>
  )
}

export default Navbar
