import { IconButton } from '@chakra-ui/react'
import React from 'react'
import { IoMdAdd } from 'react-icons/io'

type Props = {
  callback?(): void
}

const FloatingButton = ({ callback }: Props) => {
  return (
    <IconButton
      aria-label='Add Laundry'
      colorScheme='blue'
      _hover={{ transform: 'rotate(90deg)' }}
      rounded='full'
      onClick={callback && callback}
      transition='all ease 250ms'
      size='lg'
      fontSize='25px'
      position='absolute'
      _focus={{ outline: 'none' }}
      bottom='5'
      right='5'
      icon={<IoMdAdd />}
      boxShadow='md'
    />
  )
}

export default FloatingButton
