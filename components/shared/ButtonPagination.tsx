import { IconButton } from '@chakra-ui/button'
import { HStack } from '@chakra-ui/layout'
import React, { memo } from 'react'
import { BiChevronLeft, BiChevronRight } from 'react-icons/bi'

type Props = {
  onClick(type: 'prev' | 'next'): void
  isNextDisabled: boolean
  isPrevDisabled: boolean
}

const ButtonPagination = memo(
  ({ onClick, isNextDisabled, isPrevDisabled }: Props) => {
    return (
      <HStack spacing={3}>
        <IconButton
          color='gray.600'
          size='xs'
          icon={<BiChevronLeft size={20} />}
          aria-label='prev'
          isDisabled={isPrevDisabled}
          onClick={() => onClick('prev')}
          _focus={{ outline: 'none' }}
        />
        <IconButton
          color='gray.600'
          size='xs'
          icon={<BiChevronRight size={20} />}
          aria-label='next'
          isDisabled={isNextDisabled}
          onClick={() => onClick('next')}
          _focus={{ outline: 'none' }}
        />
      </HStack>
    )
  },
)

export default ButtonPagination
