import { Box, Flex, HStack, Link, VStack } from '@chakra-ui/layout'
import React from 'react'
import NextLink from 'next/link'
import UtilityService from '../../utils/utils'
import { Avatar } from '@chakra-ui/avatar'
import { Button } from '@chakra-ui/button'
import { Spinner } from '@chakra-ui/spinner'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'

type Props = {
  links: {
    name: string
    key: string
  }[]
  onClose?(): void
}

const Links = ({ links }: Props) => {
  const route = useRouter()
  const { user, authLoading, isAuthenticated } = useSelector(
    (state: RootState) => state.authReducer,
  )

  return (
    <Flex alignItems='center'>
      {links.map(link => (
        <Box key={link.key} display='inline' marginLeft='30px'>
          <NextLink href={`${link.key}`} passHref>
            <Link
              fontWeight='bold'
              transition='all ease 300ms'
              color={
                route.asPath.split('/')[1] === link.key
                  ? 'blue.600'
                  : 'gray.700'
              }
              _hover={{ color: 'blue.600' }}
              _focus={{ outline: 'none' }}
            >
              {link.name}
            </Link>
          </NextLink>
        </Box>
      ))}

      {authLoading && (
        <Box ml='30px'>
          <Spinner size='lg' color='blue.500' />
        </Box>
      )}

      {!authLoading && isAuthenticated && user && (
        <Box ml='30px'>
          <NextLink
            passHref
            href={{ pathname: '/dashboard/laundry', query: { tab: 212 } }}
          >
            <Flex alignItems='center'>
              <Link
                fontWeight='bold'
                color='gray.700'
                _hover={{ color: 'blue.600' }}
              >
                Hello, {UtilityService.getFirstName(user?.name)}
              </Link>
              <Avatar
                name={user?.name}
                src={user?.photo}
                shadow='md'
                ml='0.8rem'
                _hover={{ cursor: 'pointer' }}
              />
            </Flex>
          </NextLink>
        </Box>
      )}

      {!authLoading && !isAuthenticated && !user && (
        <HStack spacing={4} ml='1.5rem'>
          <NextLink
            passHref
            href={{ pathname: '/auth', query: { type: 'login' } }}
          >
            <Link
              fontWeight='bold'
              transition='all ease 300ms'
              color='gray.700'
              _hover={{ color: 'blue.600' }}
              _focus={{ outline: 'none' }}
            >
              Login
            </Link>
          </NextLink>
          <NextLink
            href={{ pathname: '/auth', query: { type: 'signup' } }}
            passHref
          >
            <Button
              colorScheme='blue'
              rounded={10}
              marginLeft='30px'
              _focus={{ outline: 'none' }}
            >
              Get Started
            </Button>
          </NextLink>
        </HStack>
      )}
    </Flex>
  )
}

const LinksMobile = ({ links, onClose }: Props) => {
  const route = useRouter()
  const { user, authLoading, isAuthenticated } = useSelector(
    (state: RootState) => state.authReducer,
  )

  return (
    <Box mt='2rem'>
      {links.map(link => (
        <Box key={link.key} mb='2rem'>
          <Box>
            <NextLink href={`${link.key}`} passHref>
              <Link
                fontWeight='bold'
                fontSize='1.15rem'
                transition='all ease 300ms'
                color={
                  route.asPath.split('/')[1] === link.key
                    ? 'blue.600'
                    : 'gray.700'
                }
                _hover={{ color: 'blue.600' }}
                _focus={{ outline: 'none' }}
                onClick={() => setTimeout(() => onClose && onClose(), 1200)}
              >
                {link.name}
              </Link>
            </NextLink>
          </Box>
        </Box>
      ))}

      {authLoading && (
        <Box>
          <Spinner size='lg' color='blue.500' />
        </Box>
      )}

      {!authLoading && isAuthenticated && user && (
        <NextLink
          passHref
          href={{ pathname: '/dashboard/laundry', query: { tab: 212 } }}
        >
          <Link textDecor='none'>
            <HStack spacing={4}>
              <Avatar
                name={user?.name}
                src={user?.photo}
                shadow='md'
                _hover={{ cursor: 'pointer' }}
              />
              <Link
                fontWeight='bold'
                color='gray.700'
                _hover={{ color: 'blue.600' }}
                fontSize='1.2rem'
                textDecoration='none'
              >
                Hello, {UtilityService.getFirstName(user?.name)}
              </Link>
            </HStack>
          </Link>
        </NextLink>
      )}

      {!authLoading && !isAuthenticated && !user && (
        <VStack spacing={7} alignItems='start'>
          <Box>
            <NextLink
              passHref
              href={{ pathname: '/auth', query: { type: 'login' } }}
            >
              <Link
                fontWeight='bold'
                fontSize='1.15rem'
                transition='all ease 300ms'
                color='gray.700'
                _hover={{ color: 'blue.600' }}
                _focus={{ outline: 'none' }}
              >
                Login
              </Link>
            </NextLink>
          </Box>
          <Box>
            <NextLink
              href={{ pathname: '/auth', query: { type: 'signup' } }}
              passHref
            >
              <Link textDecor='none' fontSize='1.15rem'>
                <Button
                  colorScheme='blue'
                  rounded={10}
                  _focus={{ outline: 'none' }}
                  size='lg'
                >
                  Get Started
                </Button>
              </Link>
            </NextLink>
          </Box>
        </VStack>
      )}
    </Box>
  )
}

export { Links, LinksMobile }
