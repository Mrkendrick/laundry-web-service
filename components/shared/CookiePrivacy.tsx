import React from 'react'
import { Button, Center, Link, Text, Flex } from '@chakra-ui/react'
import NextLink from 'next/link'
import { useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import appActions from '../../redux/actions/app'

const CookiePrivacy = () => {
  const dispatch = useDispatch()

  const { _isOnBoarded } = bindActionCreators(appActions, dispatch)

  return (
    <Flex
      bgGradient='linear(to-r, gray.800, gray.900)'
      justifyContent='center'
      alignItems='center'
      direction={['column', 'column', 'row']}
      w='full'
      py='1.2rem'
      position='fixed'
      bottom='0'
      zIndex='100'
      textAlign='center'
    >
      <Text color='white' mr='20px' mb={['1rem', '1rem', '0']}>
        By using our website, you agree to the use of cookies as described in
        our{' '}
        <NextLink href='/privacy' passHref>
          <Link textDecor='underline' _hover={{ color: 'blue.300' }}>
            cookie policy
          </Link>
        </NextLink>
      </Text>
      <Button onClick={() => _isOnBoarded('set')} _focus={{ outline: 'none' }}>
        Accept
      </Button>
    </Flex>
  )
}

export default CookiePrivacy
