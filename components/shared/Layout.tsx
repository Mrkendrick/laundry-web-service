import React from 'react'
import { useEffect } from 'react'
import { ReactNode } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'
import appActions from '../../redux/actions/app'
import { RootState } from '../../redux/reducers'
import CookiePrivacy from './CookiePrivacy'
import { useBreakpointValue, useMediaQuery } from '@chakra-ui/react'
import { useMemo } from 'react'

type Props = {
  children: ReactNode
}

const Layout = ({ children }: Props) => {
  const dispatch = useDispatch()
  const { isBoarded } = useSelector((state: RootState) => state.appReducer)
  const isMobile = useBreakpointValue({
    base: true,
    md: true,
    lg: false,
    xl: false,
  })

  const { _getMe, _isOnBoarded, _isMobile } = bindActionCreators(
    appActions,
    dispatch,
  )

  useEffect(() => {
    _getMe()
    _isOnBoarded('check')
    _isMobile(isMobile)
  }, [isMobile])

  return (
    <main>
      {children}
      {isBoarded === false && <CookiePrivacy />}
    </main>
  )
}

export default Layout
