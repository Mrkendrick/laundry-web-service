import React, { useState, useEffect } from 'react'
import { Button, IconButton } from '@chakra-ui/button'
import { HStack } from '@chakra-ui/layout'
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa'

type Props = {
  setPage(number: number, callback: () => void): void
  count: number
  itemsPerPage: number
}

const Pagination = ({ setPage, count, itemsPerPage }: Props) => {
  const [currentSet, setCurrentSet] = useState(1)
  const [mappingArray, setMappingArray] = useState<number[]>([])
  const [active, setActive] = useState(1)
  const [loading, setLoading] = useState(false)
  const [loadingPage, setLoadingPage] = useState(0)
  const [totalPages] = useState(Math.ceil(count / itemsPerPage))
  const [totalSet] = useState(Math.ceil(totalPages / 5))

  const setArray = (newSet: number) => {
    let mainArray = Array.from(Array(totalPages), (x, i) => i + 1)
    const arr = mainArray.slice(newSet * 5 - 5, newSet * 5)
    setMappingArray(arr)
    setCurrentSet(newSet)
  }

  const onButtonClick = (type: 'prev' | 'next') => {
    if (type === 'prev') {
      if (currentSet > 1) {
        setArray(currentSet - 1)
      }
    } else if (type === 'next') {
      if (currentSet < totalSet) {
        setArray(currentSet + 1)
      }
    }
  }

  const changePage = (num: number) => {
    setLoading(true)
    setLoadingPage(num)
    const callback = () => {
      setActive(num)
      setLoading(false)
      setLoadingPage(0)
    }
    setPage(num, callback)
  }

  useEffect(() => {
    setArray(currentSet)
  }, [count, itemsPerPage])

  return (
    <HStack spacing={4}>
      <IconButton
        aria-label='prev'
        icon={<FaChevronLeft />}
        id='prev'
        isDisabled={currentSet <= 1 ? true : false}
        onClick={() => onButtonClick('prev')}
        variant='ghost'
        colorScheme='blue'
        _focus={{ outline: 'none' }}
      />

      {mappingArray.map(num => (
        <Button
          id={`${num}`}
          key={num}
          className={active === num ? 'font-bold active' : 'font-bold'}
          onClick={() => {
            changePage(num)
          }}
          isLoading={loading && loadingPage === num}
          variant='solid'
          color={active === num ? 'white' : 'gray.600'}
          _hover={{ background: 'blue.500', color: 'white' }}
          _focus={{ outline: 'none' }}
          transition='all ease 300ms'
          bg={active === num ? 'blue.500' : 'gray.200'}
        >
          {num}
        </Button>
      ))}
      <IconButton
        aria-label='next'
        icon={<FaChevronRight />}
        id='next'
        className='next font-bold'
        disabled={currentSet >= 1 && currentSet < totalSet ? false : true}
        onClick={() => onButtonClick('next')}
        variant='ghost'
        colorScheme='blue'
        _focus={{ outline: 'none' }}
      />
    </HStack>
  )
}

export default Pagination
