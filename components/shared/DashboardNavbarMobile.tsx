import { IconButton } from '@chakra-ui/button'
import { Image } from '@chakra-ui/image'
import { Box, Flex, Link } from '@chakra-ui/layout'
import {
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerOverlay,
} from '@chakra-ui/modal'
import React from 'react'
import { CgMenuLeftAlt } from 'react-icons/cg'
import NextLink from 'next/link'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'
import { useDisclosure } from '@chakra-ui/hooks'
import SideBar from '../dashboard/SideBar'

const DashboardNavbarMobile = () => {
  const { isMobile } = useSelector((state: RootState) => state.appReducer)
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <Box
      width='100%'
      backgroundColor='white'
      position='sticky'
      as='nav'
      color='gray.700'
      top='0'
      zIndex='1000'
      paddingX={{ base: '2rem', md: '3rem', lg: '5rem' }}
      paddingY='1.5rem'
      shadow='lg'
    >
      <Flex alignItems='center' justifyContent='space-between'>
        <NextLink href='/'>
          <Link textDecor='none'>
            <Image
              src={isMobile ? '/icon.png' : '/logo.png'}
              alt='logo'
              width='162'
              height='50'
              _hover={{ cursor: 'pointer' }}
            />
          </Link>
        </NextLink>

        <Box>
          <IconButton
            variant='ghost'
            _focus={{ outline: 'none' }}
            aria-label='menu'
            icon={<CgMenuLeftAlt fontSize='2.5rem' />}
            onClick={onOpen}
          />

          <Drawer isOpen={isOpen} placement='right' onClose={onClose}>
            <DrawerOverlay />
            <DrawerContent>
              <DrawerCloseButton size='lg' _focus={{ outline: 'none' }} />

              <DrawerBody>
                <SideBar />
              </DrawerBody>
            </DrawerContent>
          </Drawer>
        </Box>
      </Flex>
    </Box>
  )
}

export default DashboardNavbarMobile
