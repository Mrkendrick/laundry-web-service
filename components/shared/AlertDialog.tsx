import React, { MutableRefObject, RefObject, useRef } from 'react'
import {
  AlertDialog as Dialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from '@chakra-ui/modal'
import { Button } from '@chakra-ui/button'

type Props = {
  isOpen: boolean
  onClose(): void
  heading: string
  body: string
  type: 'info' | 'prompt' | 'delete'
  onYesClick?(): void
  onNoClick?(): void
  onOkayClick?(): void
  onDeleteClick?(): void
}

const AlertDialog = ({
  isOpen,
  onClose,
  heading,
  body,
  type,
  onNoClick,
  onOkayClick,
  onYesClick,
  onDeleteClick,
}: Props) => {
  const cancelRef = useRef<any>()
  return (
    <Dialog isOpen={isOpen} leastDestructiveRef={cancelRef} onClose={onClose}>
      <AlertDialogOverlay>
        <AlertDialogContent>
          <AlertDialogHeader fontSize='lg' fontWeight='bold'>
            {heading}
          </AlertDialogHeader>

          <AlertDialogBody>{body}</AlertDialogBody>
          {type === 'delete' ? (
            <AlertDialogFooter>
              <Button
                ref={cancelRef}
                onClick={onClose}
                _focus={{ outline: 'none' }}
                variant='ghost'
              >
                Cancel
              </Button>
              <Button
                colorScheme='red'
                onClick={onDeleteClick}
                ml={3}
                _focus={{ outline: 'none' }}
              >
                Delete
              </Button>
            </AlertDialogFooter>
          ) : type === 'info' ? (
            <AlertDialogFooter>
              <Button
                variant='ghost'
                colorScheme='blue'
                ref={cancelRef}
                onClick={onOkayClick}
                _focus={{ outline: 'none' }}
              >
                Close
              </Button>
            </AlertDialogFooter>
          ) : (
            <AlertDialogFooter>
              <Button
                ref={cancelRef}
                onClick={onNoClick}
                _focus={{ outline: 'none' }}
                variant='ghost'
              >
                No
              </Button>
              <Button
                colorScheme='blue'
                onClick={onYesClick}
                ml={3}
                _focus={{ outline: 'none' }}
              >
                Yes
              </Button>
            </AlertDialogFooter>
          )}
        </AlertDialogContent>
      </AlertDialogOverlay>
    </Dialog>
  )
}

export default AlertDialog
