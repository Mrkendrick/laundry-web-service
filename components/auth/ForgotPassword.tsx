import { Box, Heading, Link } from '@chakra-ui/layout'
import React, { useState } from 'react'
import NextLink from 'next/link'
import { Image } from '@chakra-ui/image'
import { FormControl, FormHelperText } from '@chakra-ui/form-control'
import { Input } from '@chakra-ui/input'
import { Button } from '@chakra-ui/button'
import UtilityService from '../../utils/utils'
import { bindActionCreators } from 'redux'
import appActions from '../../redux/actions/app'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'

const ForgotPassword = () => {
  const dispatch = useDispatch()
  const [email, setEmail] = useState('')
  const [isEmailInvalid, setIsEmailInvalid] = useState(false)
  const [loading, setLoading] = useState(false)
  const { _forgotPassword } = bindActionCreators(appActions, dispatch)
  const { isMobile } = useSelector((state: RootState) => state.appReducer)

  const onChange = (value: string) => {
    isEmailInvalid && setIsEmailInvalid(false)
    setEmail(value)
  }

  const validate = () => {
    let errors

    if (email === '' || !email.match(UtilityService.emailRegex)) {
      errors = true
      setIsEmailInvalid(true)
    }

    return errors
  }

  const onSubmit = async () => {
    const errors = validate()

    if (!errors) {
      setLoading(true)
      const data = { email }

      await _forgotPassword(data, setLoading)
    }
  }

  return (
    <Box padding='2rem'>
      <Box>
        <NextLink href='/'>
          <Image
            src={isMobile ? '/icon.png' : '/logo.png'}
            alt='logo'
            width='167'
            height='57'
            _hover={{ cursor: 'pointer' }}
          />
        </NextLink>
      </Box>

      <Box marginTop='3rem' width={['100%', '100%', '70%']} mx='auto'>
        <Heading color='gray.700' textAlign='center' id='header' mb='1.2rem'>
          Recover Password
        </Heading>

        <FormControl mb='1.2rem'>
          <FormHelperText textAlign='left' fontWeight='bold' mb='0.5rem'>
            We'll send a recovery link to
          </FormHelperText>
          <Input
            type='email'
            isInvalid={isEmailInvalid}
            errorBorderColor='red.500'
            name='email'
            variant='filled'
            placeholder='Enter email'
            value={email}
            onChange={e => onChange(e.target.value)}
          />
        </FormControl>

        <Button
          isLoading={loading}
          paddingY='25px'
          type='submit'
          colorScheme='blue'
          width='100%'
          boxShadow='lg'
          onClick={onSubmit}
          _focus={{ outline: 'none' }}
        >
          Send Recovery Link
        </Button>
        <Box textAlign='center' mt='1rem'>
          <NextLink
            passHref
            href={{ pathname: '/auth', query: { type: 'login' } }}
          >
            <Link
              color='blue.400'
              _hover={{ textDecoration: 'none' }}
              _focus={{ outline: 'none' }}
            >
              Return to login
            </Link>
          </NextLink>
        </Box>
      </Box>
    </Box>
  )
}

export default ForgotPassword
