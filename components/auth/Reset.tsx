import { Box, Heading, Link, Text } from '@chakra-ui/layout'
import React, { useState, useEffect } from 'react'
import NextLink from 'next/link'
import { Image } from '@chakra-ui/image'
import { FormControl, FormHelperText, FormLabel } from '@chakra-ui/form-control'
import { Input } from '@chakra-ui/input'
import { Button } from '@chakra-ui/button'
import UtilityService from '../../utils/utils'
import { bindActionCreators } from 'redux'
import appActions from '../../redux/actions/app'
import { useDispatch, useSelector } from 'react-redux'
import { Alert, AlertIcon } from '@chakra-ui/alert'
import { useRouter } from 'next/router'
import { RootState } from '../../redux/reducers'

const Reset = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const [formData, setFormData] = useState({
    password: '',
    passwordConfirm: '',
  })
  const [isPasswordInvalid, setIsPasswordInvalid] = useState(false)
  const [isPasswordConfirmInvalid, setIsPasswordConfirmInvalid] =
    useState(false)
  const [loading, setLoading] = useState(false)
  const resetToken = router.query.resetToken
  const { _resetPassword } = bindActionCreators(appActions, dispatch)
  const { authLoading, isAuthenticated, user } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { isMobile } = useSelector((state: RootState) => state.appReducer)

  const onChange = (name: string, value: string) => {
    if (name === 'password' && isPasswordInvalid) setIsPasswordInvalid(false)
    if (name === 'passwordConfirm' && isPasswordConfirmInvalid)
      setIsPasswordConfirmInvalid(false)

    setFormData({ ...formData, [name]: value })
  }

  const validate = () => {
    let errors = false
    const { passwordConfirm, password } = formData

    if (password === '' || password.length < 8) {
      errors = true
      setIsPasswordInvalid(true)
    }

    if (passwordConfirm === '' || passwordConfirm !== password) {
      errors = true
      setIsPasswordConfirmInvalid(true)
    }

    return errors
  }

  const onSubmit = async () => {
    const errors = validate()

    const callback = () => router.replace('/')

    if (!errors) {
      setLoading(true)
      await _resetPassword(
        resetToken?.toString()!,
        formData,
        setLoading,
        callback,
      )
    }
  }

  useEffect(() => {
    router.prefetch('/dashboard/laundry')

    if (!authLoading && user && isAuthenticated) {
      router.replace({ pathname: '/dashboard/laundry', query: { tab: 212 } })
    }
  }, [authLoading, user, isAuthenticated])

  return (
    <Box padding='2rem'>
      <Box>
        <NextLink href='/'>
          <Image
            src={isMobile ? '/icon.png' : '/logo.png'}
            alt='logo'
            width='167'
            height='57'
            _hover={{ cursor: 'pointer' }}
          />
        </NextLink>
      </Box>

      <Box marginTop='3rem' width={['100%', '100%', '70%']} mx='auto'>
        <Heading color='gray.700' textAlign='center' id='header' mb='1.2rem'>
          Recover Password
        </Heading>

        <FormControl mb='1rem'>
          <FormLabel>New Password</FormLabel>
          <Input
            type='password'
            isInvalid={isPasswordInvalid}
            errorBorderColor='red.500'
            name='password'
            variant='filled'
            placeholder='**********'
            value={formData.password}
            onChange={e => onChange('password', e.target.value)}
          />
          <FormHelperText fontSize='xs'>
            Password must be not be less than 8 characters
          </FormHelperText>
        </FormControl>

        <FormControl mb='1.2rem'>
          <FormLabel>Confirm Password</FormLabel>
          <Input
            type='password'
            isInvalid={isPasswordConfirmInvalid}
            errorBorderColor='red.500'
            name='passwordConfirm'
            variant='filled'
            placeholder='**********'
            value={formData.passwordConfirm}
            onChange={e => onChange('passwordConfirm', e.target.value)}
          />
        </FormControl>

        <Button
          isLoading={loading}
          paddingY='25px'
          type='submit'
          colorScheme='blue'
          width='100%'
          boxShadow='lg'
          onClick={onSubmit}
          _focus={{ outline: 'none' }}
          disabled={!resetToken}
        >
          Submit
        </Button>
        <Box textAlign='center' mt='1rem'>
          <NextLink
            passHref
            href={{ pathname: '/auth', query: { type: 'login' } }}
          >
            <Link
              color='blue.400'
              _hover={{ textDecoration: 'none' }}
              _focus={{ outline: 'none' }}
            >
              Return to login
            </Link>
          </NextLink>
        </Box>
        <Box mt='1.2rem'>
          {!resetToken && (
            <Alert status='error' variant='left-accent' rounded='lg'>
              <AlertIcon />
              <Text>Reset Token Not found</Text>
            </Alert>
          )}
        </Box>
      </Box>
    </Box>
  )
}

export default Reset
