import React, { ChangeEvent, useEffect, useState } from 'react'
import { Text, Box, Flex, Heading, Spacer, Link } from '@chakra-ui/layout'
import { Input, InputGroup, InputRightElement } from '@chakra-ui/input'
import { FormControl, FormLabel } from '@chakra-ui/form-control'
import { Checkbox } from '@chakra-ui/checkbox'
import { Button } from '@chakra-ui/button'
import { Image } from '@chakra-ui/image'
import NextLink from 'next/link'
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai'
import UtilityService from '../../utils/utils'
import appActions from '../../redux/actions/app'
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'
import { RootState } from '../../redux/reducers'
import { useRouter } from 'next/router'

const Login = () => {
  const [isVisible, setIsVisible] = useState(false)
  const [formData, setFormData] = useState({ email: '', password: '' })
  const [loading, setLoading] = useState(false)
  const [isEmailInvalid, setIsEmailInvalid] = useState(false)
  const [isPasswordInvalid, setIsPasswordInvalid] = useState(false)
  const { authLoading, isAuthenticated, user } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { isMobile } = useSelector((state: RootState) => state.appReducer)
  const router = useRouter()
  const dispatch = useDispatch()

  const { _login, _getMe } = bindActionCreators(appActions, dispatch)

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target

    if (name === 'email' && isEmailInvalid) {
      setIsEmailInvalid(false)
    }

    if (name === 'password' && isPasswordInvalid) {
      setIsPasswordInvalid(false)
    }

    setFormData({ ...formData, [name]: value })
  }

  const validate = () => {
    let errors = false
    const { email, password } = formData

    if (email === '' || !email.match(UtilityService.emailRegex)) {
      errors = true
      setIsEmailInvalid(true)
    }

    if (password === '') {
      errors = true
      setIsPasswordInvalid(true)
    }

    return errors
  }

  const onSubmit = async () => {
    const errors = validate()

    if (!errors) {
      setLoading(true)

      await _login(formData, setLoading)
      await _getMe()
    }
  }

  useEffect(() => {
    router.prefetch('/dashboard/laundry')

    if (!authLoading && user && isAuthenticated) {
      router.replace({ pathname: '/dashboard/laundry', query: { tab: 212 } })
    }
  }, [authLoading, user, isAuthenticated])

  return (
    <Box padding='2rem'>
      <Box>
        <NextLink href='/'>
          <Image
            src={isMobile ? '/icon.png' : '/logo.png'}
            alt='logo'
            width='167'
            height='57'
            _hover={{ cursor: 'pointer' }}
          />
        </NextLink>
      </Box>

      <Box marginTop='3rem' width={['100%', '100%', '70%']} mx='auto'>
        <Heading color='gray.700' textAlign='center' id='header' mb='1.2rem'>
          Login
        </Heading>

        <FormControl mb='1.2rem'>
          <FormLabel>Email</FormLabel>
          <Input
            type='email'
            isInvalid={isEmailInvalid}
            errorBorderColor='red.500'
            name='email'
            variant='filled'
            placeholder='email@address.com'
            value={formData.email}
            onChange={onChange}
          />
        </FormControl>

        <FormControl>
          <FormLabel>Password</FormLabel>
          <InputGroup>
            <Input
              type={isVisible ? 'text' : 'password'}
              id='field-2'
              isInvalid={isPasswordInvalid}
              errorBorderColor='red.500'
              name='password'
              variant='filled'
              placeholder='*********'
              onChange={onChange}
              value={formData.password}
            />

            <InputRightElement
              children={
                isVisible ? (
                  <AiOutlineEyeInvisible size='19px' />
                ) : (
                  <AiOutlineEye size='19px' />
                )
              }
              onClick={() => setIsVisible(!isVisible)}
              color='gray.500'
              _hover={{ cursor: 'pointer' }}
            />
          </InputGroup>
        </FormControl>
        <Flex mt='1.2rem' alignItems='center'>
          <Checkbox color='gray.500'>
            <Text fontSize='14px'>Remember me</Text>
          </Checkbox>

          <Spacer />
          <NextLink
            passHref
            href={{ pathname: '/auth', query: { type: 'forgotPassword' } }}
          >
            <Link
              _hover={{ textDecoration: 'none' }}
              _focus={{ outline: 'none' }}
              color='gray.500'
            >
              <Text fontSize='14px'>Forgot Password?</Text>
            </Link>
          </NextLink>
        </Flex>

        <Button
          isLoading={loading}
          mt='2rem'
          paddingY='25px'
          type='submit'
          colorScheme='blue'
          width='100%'
          boxShadow='lg'
          onClick={onSubmit}
          _focus={{ outline: 'none' }}
        >
          Log in
        </Button>
        <Box textAlign='center' mt='1rem'>
          <NextLink
            passHref
            href={{ pathname: '/auth', query: { type: 'signup' } }}
          >
            <Link
              color='blue.400'
              _hover={{ textDecoration: 'none' }}
              _focus={{ outline: 'none' }}
            >
              Create an account.
            </Link>
          </NextLink>
        </Box>
      </Box>
    </Box>
  )
}

export default Login
