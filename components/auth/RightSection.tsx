import React from 'react';
import { Box, Text, VStack } from '@chakra-ui/layout';
import { useRouter } from 'next/router';
import styles from '../../styles/wave.module.scss';

const RightSection = () => {
  const { query } = useRouter();

  return (
    <VStack
      width="50%"
      backgroundImage={`url('/auth.jpg')`}
      backgroundRepeat="no-repeat"
      backgroundPosition="center"
      backgroundSize="cover"
      height="100vh"
      justifyContent="center"
    >
      <Box>
        {query.type === 'login' ? (
          <Text fontSize="5xl" color="white" fontWeight="bold">
            <span className={styles.wave}>👋</span> Welcome Back,
            <br />
            Log into your account.
          </Text>
        ) : query.type === 'forgotPassword' ? (
          <Text fontSize="5xl" color="white" fontWeight="bold">
            <span className={styles.wave}>😩</span> Forgot your password?
            <br />
            Enter your email.
          </Text>
        ) : query.type === 'reset' ? (
          <Text fontSize="5xl" color="white" fontWeight="bold">
            <span className={styles.wave}>😉</span> Well done!,
            <br />
            Now enter a new password.
          </Text>
        ) : (
          <Text fontSize="5xl" color="white" fontWeight="bold">
            <span className={styles.wave}>😀</span> Hey there,
            <br />
            Create an account.
          </Text>
        )}
      </Box>
    </VStack>
  );
};

export default RightSection;
