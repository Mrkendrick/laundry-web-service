import React, { ChangeEvent, useEffect, useState } from 'react'
import {
  Box,
  Button,
  FormControl,
  Input,
  InputGroup,
  InputRightElement,
  Image,
  Link,
  Heading,
  FormHelperText,
  FormLabel,
} from '@chakra-ui/react'
import NextLink from 'next/link'
import UtilityService from '../../utils/utils'
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai'
import { useDispatch, useSelector } from 'react-redux'
import appActions from '../../redux/actions/app'
import { bindActionCreators } from 'redux'
import lo_ from 'lodash'
import { useRouter } from 'next/router'
import { RootState } from '../../redux/reducers'

const Signup = () => {
  const dispatch = useDispatch()
  const router = useRouter()
  const [isVisible, setIsVisible] = useState(false)
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
  })
  const [loading, setLoading] = useState(false)
  const [isEmailInvalid, setIsEmailInvalid] = useState(false)
  const [isNameInvalid, setIsNameInvalid] = useState(false)
  const [isPasswordInvalid, setIsPasswordInvalid] = useState(false)
  const { authLoading, isAuthenticated, user } = useSelector(
    (state: RootState) => state.authReducer,
  )
  const { isMobile } = useSelector((state: RootState) => state.appReducer)

  const { _signup, _getMe } = bindActionCreators(appActions, dispatch)

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target
    if (name === 'name' && isEmailInvalid) setIsNameInvalid(false)

    if (name === 'email' && isEmailInvalid) setIsEmailInvalid(false)

    if (name === 'password' && isPasswordInvalid) setIsPasswordInvalid(false)

    setFormData({ ...formData, [name]: value })
  }

  const validate = () => {
    let errors
    const { email, password, name } = formData

    if (name === '') {
      errors = true
      setIsNameInvalid(true)
    }

    if (email === '' || !email.match(UtilityService.emailRegex)) {
      errors = true
      setIsEmailInvalid(true)
    }

    if (password === '' || password.length < 8) {
      errors = true
      setIsPasswordInvalid(true)
    }

    if (
      password !== '' &&
      password.length >= 8 &&
      email.match(UtilityService.emailRegex) &&
      email !== ''
    ) {
      errors = false
    }
    return errors
  }

  const onSubmit = async () => {
    const errors = validate()

    if (!errors) {
      setLoading(true)

      const { email, password, name } = formData

      const userData = {
        name: lo_.trim(name),
        email,
        password,
        passwordConfirm: password,
      }

      await _signup(userData, setLoading)
      await _getMe()
    }
  }

  useEffect(() => {
    router.prefetch('/dashboard/laundry')

    if (!authLoading && user && isAuthenticated) {
      router.replace({ pathname: '/dashboard/laundry', query: { tab: 212 } })
    }
  }, [authLoading, user, isAuthenticated])

  return (
    <Box padding='2rem'>
      <Box>
        <NextLink href='/'>
          <Image
            src={isMobile ? '/icon.png' : '/logo.png'}
            alt='logo'
            width='167'
            height='57'
            _hover={{ cursor: 'pointer' }}
          />
        </NextLink>
      </Box>

      <Box marginTop='3rem' width={['100%', '100%', '70%']} mx='auto'>
        <Heading color='gray.700' textAlign='center' id='header'>
          Register
        </Heading>
        <FormControl mb='1.2rem'>
          <FormLabel>Name</FormLabel>
          <Input
            type='text'
            isInvalid={isNameInvalid}
            errorBorderColor='red.500'
            name='name'
            variant='filled'
            placeholder='John Doe'
            autoCapitalize='true'
            value={formData.name}
            onChange={onChange}
          />
        </FormControl>

        <FormControl mb='1.2rem'>
          <FormLabel>Email Address</FormLabel>
          <Input
            type='email'
            isInvalid={isEmailInvalid}
            errorBorderColor='red.500'
            name='email'
            variant='filled'
            placeholder='email@address.com'
            value={formData.email}
            onChange={onChange}
          />
        </FormControl>

        <FormControl>
          <FormLabel>Password</FormLabel>
          <InputGroup>
            <Input
              type={isVisible ? 'text' : 'password'}
              isInvalid={isPasswordInvalid}
              errorBorderColor='red.500'
              name='password'
              variant='filled'
              placeholder='********'
              onChange={onChange}
              value={formData.password}
            />

            <InputRightElement
              children={
                isVisible ? (
                  <AiOutlineEyeInvisible size='19px' />
                ) : (
                  <AiOutlineEye size='19px' />
                )
              }
              onClick={() => setIsVisible(!isVisible)}
              color='gray.500'
              _hover={{ cursor: 'pointer' }}
            />
          </InputGroup>
          <FormHelperText>
            Password length must be greater than 7 characters.
          </FormHelperText>
        </FormControl>

        <Button
          isLoading={loading}
          mt='2rem'
          paddingY='25px'
          type='submit'
          colorScheme='blue'
          width='100%'
          boxShadow='lg'
          onClick={onSubmit}
          _focus={{ outline: 'none' }}
        >
          Register
        </Button>
        <Box textAlign='center' mt='1rem'>
          <NextLink
            href={{ pathname: '/auth', query: { type: 'login' } }}
            passHref
          >
            <Link
              color='blue.400'
              _hover={{ textDecoration: 'none' }}
              _focuys={{ outline: 'none' }}
            >
              Already have an account? Login
            </Link>
          </NextLink>
        </Box>
      </Box>
    </Box>
  )
}

export default Signup
