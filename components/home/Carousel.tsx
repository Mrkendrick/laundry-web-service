import React, { useMemo } from 'react'
import 'react-responsive-carousel/lib/styles/carousel.min.css' // requires a loader
import { Carousel as _Carousel } from 'react-responsive-carousel'
import { Center, Heading, Box, Text, Link } from '@chakra-ui/layout'
import { Button } from '@chakra-ui/button'
import NextLink from 'next/link'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'
import { v4 } from 'uuid'

const Carousel = () => {
  const { user } = useSelector((state: RootState) => state.authReducer)
  const carouselData = useMemo(
    () => [
      {
        id: v4(),
        heading: ' Laundry Service In Your City',
        description:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum illo debitis odio, obcaecati similique mollitia magni, dolore quam cu ',
      },
      {
        id: v4(),
        heading: ' Laundry Service In Your City',
        description:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum illo debitis odio, obcaecati similique mollitia magni, dolore quam cu ',
      },
      {
        id: v4(),
        heading: ' Laundry Service In Your City',
        description:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum illo debitis odio, obcaecati similique mollitia magni, dolore quam cu ',
      },
    ],
    [],
  )

  return (
    <_Carousel
      axis='horizontal'
      autoPlay
      centerMode
      infiniteLoop
      showStatus={false}
      showThumbs={false}
      showIndicators={false}
      interval={5000}
    >
      {carouselData.map(data => (
        <Center key={data.id}>
          <Box width={['75%', '75%', '70%']} py='1rem'>
            <Heading
              mb='1rem'
              fontSize={['2rem', '2rem', '3rem']}
              color='white'
            >
              {data.heading}
            </Heading>
            <Text fontSize='0.9rem' color='white' mb='1rem'>
              {data.description}
            </Text>
            {user ? (
              <NextLink
                href={{ pathname: '/dashboard', query: { tab: 211 } }}
                passHref
              >
                <Link textDecor='none'>
                  <Button py='1.4rem' px='1.6rem' colorScheme='blue'>
                    Dashboard
                  </Button>
                </Link>
              </NextLink>
            ) : (
              <NextLink
                href={{ pathname: '/auth', query: { type: 'signup' } }}
                passHref
              >
                <Link textDecor='none'>
                  <Button
                    py={['1rem', '1rem', '1.4rem']}
                    px={['1rem', '1rem', '1.6rem']}
                    colorScheme='blue'
                  >
                    Get Started
                  </Button>
                </Link>
              </NextLink>
            )}
          </Box>
        </Center>
      ))}
    </_Carousel>
  )
}

export default Carousel
