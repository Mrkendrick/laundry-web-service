import { Box, Icon } from '@chakra-ui/react'
import React from 'react'
import NextLink from 'next/link'
import { IoIosArrowDropdown } from 'react-icons/io'
import { Link } from '@chakra-ui/layout'
import Carousel from './Carousel'

const SectionA = () => {
  return (
    <Box
      backgroundImage={`url('/banner.jpg')`}
      backgroundPosition='center'
      backgroundRepeat='no-repeat'
      backgroundSize='cover'
      height='85vh'
      position='relative'
    >
      <Box
        position='absolute'
        width={['100%', '100%', '90%']}
        top='50%'
        left='50%'
        transform='translate(-50%, -50%)'
        mb='1rem'
      >
        <Carousel />
      </Box>
      <Link href='#our_process'>
        <Icon
          position='absolute'
          bottom='15px'
          left='50%'
          transform='translateX(-50%)'
          transition='color ease 300ms'
          _hover={{ color: 'blue.300', cursor: 'pointer' }}
          boxSize={['40px', '40px', '55px']}
          color='white'
          as={IoIosArrowDropdown}
        />
      </Link>
    </Box>
  )
}

export default SectionA
