import { Box, Flex, Text, Heading } from '@chakra-ui/react'
import Image from 'next/image'
import React from 'react'

const Process = () => {
  return (
    <Box
      mt='110px'
      id='our_process'
      mx='auto'
      textAlign='center'
      w={['95%', '95%', '90%']}
    >
      <Text
        color='blue.800'
        fontSize='14px'
        py='0.5rem'
        px='1.8rem'
        rounded='full'
        bg='blue.50'
        display='inline-block'
        mb='2rem'
      >
        Our Process
      </Text>
      <Heading color='blue.800' mb='3rem'>
        How We Work
      </Heading>
      <Flex alignItems='center' flexDirection={['column', 'column', 'row']}>
        <Box mx='auto' w={['95%', '95%', '33%']} mb={['3rem', '3rem', '0rem']}>
          <Image width='105px' height='105px' src='/services-icon1.svg' />
          <Heading color='gray.700' fontSize='24px' my='1.2rem'>
            We Collect Your Clothes
          </Heading>
          <Text color='#57667e' lineHeight='30px' fontSize='16px'>
            The automated process starts as soon as your clothes go into the
            machine. The outcome is gleaming clothes!
          </Text>
        </Box>
        <Box mx='auto' w={['95%', '95%', '33%']} mb={['3rem', '3rem', '0rem']}>
          <Image width='105px' height='105px' src='/services-icon2.svg' />
          <Heading color='gray.700' fontSize='24px' my='1.2rem'>
            Wash Your Clothes
          </Heading>
          <Text color='#57667e' lineHeight='30px' fontSize='16px'>
            The automated process starts as soon as your clothes go into the
            machine. The outcome is gleaming clothes!
          </Text>
        </Box>
        <Box mx='auto' w={['95%', '95%', '33%']} mb={['3rem', '3rem', '0rem']}>
          <Image width='105px' height='105px' src='/services-icon3.svg' />
          <Heading color='gray.700' fontSize='24px' my='1.2rem'>
            Get It Delivered
          </Heading>
          <Text color='#57667e' lineHeight='30px' fontSize='16px'>
            The automated process starts as soon as your clothes go into the
            machine. The outcome is gleaming clothes!
          </Text>
        </Box>
      </Flex>
    </Box>
  )
}

export default Process
