import { Box, Text } from '@chakra-ui/layout'
import React from 'react'
import SectionA from './SectionA'

const Home = () => {
  return (
    <>
      <SectionA />
    </>
  )
}

export default Home
