import React from 'react'
import { useSelector } from 'react-redux'
import { Icon } from '@chakra-ui/icon'
import { Box, Heading, Text, VStack, HStack } from '@chakra-ui/layout'
import { FaMapMarkerAlt, FaPhoneAlt } from 'react-icons/fa'
import { MdEmail } from 'react-icons/md'
import { RootState } from '../../redux/reducers'

const Location = () => {
  const { isMobile } = useSelector((state: RootState) => state.appReducer)

  return (
    <Box mt='110px' id='our_location' width='90%' mx='auto' textAlign='center'>
      <Text
        color='blue.800'
        fontSize='14px'
        py='0.5rem'
        px='1.8rem'
        rounded='full'
        bg='blue.50'
        display='inline-block'
        mb='2rem'
      >
        Our Location
      </Text>

      <Heading color='blue.800' mb='3rem'>
        Visit Our Store Today
      </Heading>
      {isMobile && (
        <Box
          shadow='xl'
          bg='blue.900'
          rounded='md'
          padding='1.7rem'
          color='white'
          textAlign='left'
          mb='2rem'
        >
          <Heading fontSize='20px' mb='0.5rem'>
            Instant Wash N Dry
          </Heading>

          <VStack alignItems='flex-start' spacing={4} my='0.7rem'>
            <HStack spacing={3}>
              <Icon as={FaMapMarkerAlt} fontSize='18px' color='blue.200' />
              <Text fontSize='12px' fontWeight='bold'>
                29b Afolabi Aina St, Allen 100212, Ikeja
              </Text>
            </HStack>
            <HStack spacing={3}>
              <Icon as={FaPhoneAlt} fontSize='18px' color='blue.200' />
              <Text fontSize='12px' fontWeight='bold'>
                09048329045
              </Text>
            </HStack>
            <HStack spacing={3}>
              <Icon as={MdEmail} fontSize='18px' color='blue.200' />
              <Text fontSize='12px' fontWeight='bold'>
                instantwashndry@gmail.com
              </Text>
            </HStack>
          </VStack>
        </Box>
      )}

      <Box overflowX='hidden' position='relative'>
        <iframe
          src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.3197682561868!2d3.348123014161221!3d6.6071292240200075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b93d5eba67af7%3A0x33d59a7ed849dedd!2sInstant%20wash%20and%20Dry%20Laundromat%20Ltd!5e0!3m2!1sen!2sng!4v1625442758755!5m2!1sen!2sng'
          width='100%'
          height='450'
          loading='lazy'
        ></iframe>

        {!isMobile && (
          <Box
            position='absolute'
            shadow='xl'
            bg='blue.900'
            rounded='md'
            width='auto'
            height='260px'
            zIndex='100'
            top='5'
            right='5'
            padding='1.7rem'
            textAlign='left'
            color='white'
            ml={['0.5rem', '0.5rem', '1rem']}
          >
            <Heading fontSize='20px' mb='0.6rem'>
              Instant Wash N Dry
            </Heading>

            <Text fontSize='13px' color='whiteAlpha.700'>
              Lorem ipsum dolor sit, amet consectetur adipisicing.
            </Text>
            <VStack alignItems='flex-start' spacing={4} my='1rem'>
              <HStack spacing={3}>
                <Icon as={FaMapMarkerAlt} fontSize='20px' color='blue.200' />
                <Text fontSize='12px' fontWeight='bold'>
                  29b Afolabi Aina St, Allen 100212, Ikeja
                </Text>
              </HStack>
              <HStack spacing={3}>
                <Icon as={FaPhoneAlt} fontSize='20px' color='blue.200' />
                <Text fontSize='12px' fontWeight='bold'>
                  09048329045
                </Text>
              </HStack>
              <HStack spacing={3}>
                <Icon as={MdEmail} fontSize='20px' color='blue.200' />
                <Text fontSize='12px' fontWeight='bold'>
                  instantwashndry@gmail.com
                </Text>
              </HStack>
            </VStack>
          </Box>
        )}
      </Box>
    </Box>
  )
}

export default Location
