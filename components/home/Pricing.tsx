import {
  VStack,
  Box,
  Text,
  Heading,
  HStack,
  StackDivider,
} from '@chakra-ui/layout'
import { Button } from '@chakra-ui/button'
import { Icon } from '@chakra-ui/icon'
import { FiCheck } from 'react-icons/fi'
import React from 'react'
import UtilityService from '../../utils/utils'

type Props = {
  prices: {
    delivery: number
    drySelf: number
    washDrySelf: number
    iron: number
  }
}

const Pricing = ({ prices }: Props) => {
  return (
    <Box
      mt='110px'
      id='pricing'
      width={['95%', '95%', '90%']}
      mx='auto'
      textAlign='center'
    >
      <Text
        color='blue.800'
        fontSize='14px'
        py='0.5rem'
        px='1.8rem'
        rounded='full'
        bg='blue.50'
        display='inline-block'
        mb='2rem'
      >
        Pricing
      </Text>

      <Heading color='blue.800' mb='3rem'>
        Pocket Friendly Cost
      </Heading>

      <HStack
        w={['95%', '95%', '85%']}
        flexDirection={['column', 'column', 'row']}
        mx='auto'
        overflow='hidden'
        shadow='lg'
        rounded='md'
        bg='gray.50'
        divider={<StackDivider borderColor='gray.200' />}
      >
        <Box px='3rem' py='4rem' textAlign='left' w='full'>
          <Heading>Dry</Heading>
          <Box my='6' h='3px' w='56px' bg='blue.500' />
          <Text maxW='300px' mb='1rem'>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit.
          </Text>
          <Heading mb='1rem' fontSize='3rem'>
            {!prices?.drySelf
              ? 'N/A'
              : `#${UtilityService.formatPrice(
                  prices.drySelf + prices.delivery,
                )}`}
          </Heading>
          <Text mb='0.8em' color='gray.800' fontWeight='bold'>
            PER DRY
          </Text>
          <Text mb='1.5rem' fontSize='15px' color='gray.600'>
            Additional Ironing Service:{' '}
            {!prices.iron ? 'N/A' : `#${prices.iron}`}
          </Text>
          <Button mb='1.5rem' colorScheme='blue' px='2rem' py='1.5rem'>
            Order Now
          </Button>
          <Box>
            <Text fontWeight='bold' mb='1rem'>
              What you get:
            </Text>
            <VStack alignItems='flex-start' spacing={4}>
              <HStack>
                <Icon color='blue.500' fontSize='25px' as={FiCheck} />
                <Text>Lorem ipsum dolor sit amet.</Text>
              </HStack>
              <HStack>
                <Icon color='blue.500' fontSize='25px' as={FiCheck} />
                <Text>Lorem ipsum dolor sit amet.</Text>
              </HStack>
              <HStack>
                <Icon color='blue.500' fontSize='25px' as={FiCheck} />
                <Text>Lorem ipsum dolor sit amet.</Text>
              </HStack>
            </VStack>
          </Box>
        </Box>
        <Box px='3rem' py='4rem' textAlign='left' w='full'>
          <Heading>Wash N Dry</Heading>
          <Box my='6' h='3px' w='56px' bg='red.500' />
          <Text maxW='300px' mb='1rem'>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit.
          </Text>
          <Heading mb='1rem' fontSize='3rem'>
            {!prices?.drySelf
              ? 'N/A'
              : `#${UtilityService.formatPrice(
                  prices.washDrySelf + prices.delivery,
                )}`}
          </Heading>
          <Text mb='0.8em' color='gray.800' fontWeight='bold'>
            PER WASH
          </Text>
          <Text mb='1.5rem' fontSize='15px' color='gray.600'>
            Additional Ironing Service:{' '}
            {!prices.iron ? 'N/A' : `#${prices.iron}`}
          </Text>
          <Button mb='1.5rem' colorScheme='red' px='2rem' py='1.5rem'>
            Order Now
          </Button>
          <Box>
            <Text fontWeight='bold' mb='1rem'>
              What you get:
            </Text>
            <VStack alignItems='flex-start' spacing={4}>
              <HStack>
                <Icon color='red.500' fontSize='25px' as={FiCheck} />
                <Text>Lorem ipsum dolor sit amet.</Text>
              </HStack>
              <HStack>
                <Icon color='red.500' fontSize='25px' as={FiCheck} />
                <Text>Lorem ipsum dolor sit amet.</Text>
              </HStack>
              <HStack>
                <Icon color='red.500' fontSize='25px' as={FiCheck} />
                <Text>Lorem ipsum dolor sit amet.</Text>
              </HStack>
            </VStack>
          </Box>
        </Box>
      </HStack>
    </Box>
  )
}

export default Pricing
