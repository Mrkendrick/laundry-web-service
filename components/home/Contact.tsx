import React, { useState } from 'react'
import { Box, Heading, Text } from '@chakra-ui/layout'
import { FormControl, FormHelperText, FormLabel } from '@chakra-ui/form-control'
import { Button } from '@chakra-ui/button'
import { Input } from '@chakra-ui/input'
import { Textarea } from '@chakra-ui/textarea'
import { Alert, AlertIcon } from '@chakra-ui/alert'
import lo_ from 'lodash'
import { useDispatch } from 'react-redux'
import appActions from '../../redux/actions/app'
import { bindActionCreators } from 'redux'
import UtilityService from '../../utils/utils'

const Contact = () => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    message: '',
  })
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [alertType, setAlertType] = useState<'info' | 'warning'>('info')
  const [showAlert, setShowAlert] = useState(false)
  const [isNameInvalid, setIsNameInvalid] = useState(false)
  const [isEmailInvalid, setIsEmailInvalid] = useState(false)
  const [isMessageInvalid, setIsMessageInvalid] = useState(false)

  const { _sendMessage } = bindActionCreators(appActions, dispatch)

  const onChange = (name: string, value: string) => {
    name === 'name' && isNameInvalid && setIsNameInvalid(false)
    name === 'email' && isEmailInvalid && setIsEmailInvalid(false)
    name === 'message' && isMessageInvalid && setIsMessageInvalid(false)

    setFormData({ ...formData, [name]: value })
  }

  const validate = () => {
    let errors
    const { name, email, message } = formData

    if (name === '') {
      errors = true
      setIsNameInvalid(true)
    }

    if (email === '' || !email.match(UtilityService.emailRegex)) {
      errors = true
      setIsEmailInvalid(true)
    }

    if (message === '') {
      errors = true
      setIsMessageInvalid(true)
    }

    return errors
  }

  const setAlert = (type: 'success' | 'error') => {
    if (type === 'success') {
      setFormData({ name: '', email: '', message: '' })
      setAlertType('info')
      setShowAlert(true)

      setTimeout(() => {
        setShowAlert(false)
      }, 3000)
    }

    if (type === 'error') {
      setAlertType('warning')
      setShowAlert(true)

      setTimeout(() => {
        setShowAlert(false)
      }, 3000)
    }
  }

  const onSubmit = async () => {
    const errors = validate()

    if (!errors) {
      setLoading(true)

      const data = {
        ...formData,
        name: lo_.trim(formData.name),
      }

      _sendMessage(data, setLoading, setAlert)
    }
  }

  return (
    <Box
      w={['90%', '90%', '60%']}
      mx='auto'
      mt='110px'
      id='contact_us'
      textAlign='center'
    >
      <Text
        color='blue.800'
        fontSize='14px'
        py='0.5rem'
        px='1.8rem'
        rounded='full'
        bg='blue.50'
        display='inline-block'
        mb='2rem'
      >
        Contact Us
      </Text>

      <Heading color='blue.800' mb={['1.8rem', '1.8rem', '3rem']}>
        Send Us a Message
      </Heading>
      <Box
        padding={['1.2rem', '1.2rem', '2rem']}
        bg='white'
        shadow='lg'
        rounded='lg'
        transition='all ease 300ms'
      >
        <FormControl mb='1.2rem'>
          <FormLabel>Name</FormLabel>
          <Input
            isInvalid={isNameInvalid}
            type='text'
            errorBorderColor='red.500'
            name='name'
            variant='filled'
            autoCapitalize='true'
            value={formData.name}
            onChange={e => onChange('name', e.target.value)}
            placeholder='John Doe'
          />
        </FormControl>

        <FormControl mb='1.2rem'>
          <FormLabel>Email</FormLabel>
          <Input
            isInvalid={isEmailInvalid}
            type='email'
            errorBorderColor='red.500'
            name='email'
            variant='filled'
            value={formData.email}
            onChange={e => onChange('email', e.target.value)}
            placeholder='johndoe@email.com'
          />
          <FormHelperText textAlign='left'>
            We'll never share your email.
          </FormHelperText>
        </FormControl>

        <FormControl mb='1.2rem'>
          <FormLabel>Message</FormLabel>
          <Textarea
            isInvalid={isMessageInvalid}
            resize='none'
            type='text'
            errorBorderColor='red.500'
            name='message'
            variant='filled'
            value={formData.message}
            placeholder='Enter a message'
            onChange={e => onChange('message', e.target.value)}
          />
        </FormControl>

        <Button
          isLoading={loading}
          mt='2rem'
          paddingY='25px'
          type='submit'
          colorScheme='blue'
          width='100%'
          onClick={onSubmit}
          _focus={{ outline: 'none' }}
        >
          Send Message
        </Button>
        <Box mt='1.2rem'>
          {showAlert && (
            <Alert status={alertType} variant='left-accent' rounded='lg'>
              <AlertIcon />
              {alertType === 'info' ? (
                <Text>Message sent successfully</Text>
              ) : (
                <Text>Something went wrong</Text>
              )}
            </Alert>
          )}
        </Box>
      </Box>
    </Box>
  )
}

export default Contact
