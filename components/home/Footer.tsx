import React from 'react'
import {
  Box,
  HStack,
  Text,
  Heading,
  Center,
  Link,
  ListIcon,
  List,
  ListItem,
  Flex,
} from '@chakra-ui/layout'
import { IconButton } from '@chakra-ui/button'
import {
  TiSocialTwitter,
  TiSocialFacebook,
  TiSocialPinterest,
} from 'react-icons/ti'
import { BsDash } from 'react-icons/bs'
import { Image } from '@chakra-ui/image'
import NextLink from 'next/link'

const Footer = () => {
  return (
    <Box
      w='100%'
      padding={{ lg: '1.5rem' }}
      mt='110px'
      bgImage={`url('/hill.png')`}
      bgRepeat='no-repeat'
      bgPosition='bottom'
      bgSize='contain'
    >
      <Flex
        alignItems='center'
        mx='auto'
        flexDirection={['column', 'column', 'row']}
        spacing={[0, 0, 5]}
        padding={{ lg: '1rem' }}
      >
        <Box
          w={['100%', '100%', '40%']}
          alignSelf='start'
          px='2rem'
          mb={['2rem', '2rem', 0]}
        >
          <Box mb='2rem'>
            <Image src='/logo.png' w={['100%', '100%', '50%']} />
          </Box>

          <Text
            color='gray.700'
            fontSize='14px'
            lineHeight='1.8'
            mb={['1.5rem', '1.5rem', '3rem']}
          >
            Lorem ipsum, dolor sit amet consectetur adipisicing elit.
            Accusantium, officiis doloremque similique voluptatum quam in sed?
            Id rerum, dolorem earum nam doloremque fuga itaque laudantium
            recusandae ab iste. Maiores, cum?
          </Text>

          <HStack spacing={3}>
            <NextLink href='' passHref>
              <Link>
                <IconButton
                  aria-label='twitter'
                  icon={<TiSocialTwitter />}
                  backgroundColor='bg.blue'
                  color='#1c165c'
                  isRound
                />
              </Link>
            </NextLink>

            <NextLink href='' passHref>
              <Link>
                <IconButton
                  aria-label='facebook'
                  icon={<TiSocialFacebook />}
                  backgroundColor='bg.blue'
                  color='#1c165c'
                  isRound
                />
              </Link>
            </NextLink>

            <NextLink href='' passHref>
              <Link>
                <IconButton
                  aria-label='instagram'
                  icon={<TiSocialPinterest />}
                  backgroundColor='bg.blue'
                  color='#1c165c'
                  isRound
                />
              </Link>
            </NextLink>
          </HStack>
        </Box>

        <Box
          w={['100%', '100%', '40%']}
          alignSelf={['left', 'left', 'start']}
          px='2rem'
          mb={['2rem', '2rem', 0]}
        >
          <Heading as='h2' textAlign='left' mb='2rem'>
            Services
          </Heading>
          <List spacing={5}>
            <List color='gray.600' fontSize='14px' spacing={5}>
              <ListItem>
                <ListIcon as={BsDash} />
                Dry Cleaning
              </ListItem>
              <ListItem>
                <ListIcon as={BsDash} />
                Dry Clean
              </ListItem>
              <ListItem>
                <ListIcon as={BsDash} />
                Ironing Services
              </ListItem>
              <ListItem>
                <ListIcon as={BsDash} />
                Laundry Service London
              </ListItem>
              <ListItem>
                <ListIcon as={BsDash} />
                Laundry App
              </ListItem>
            </List>
          </List>
        </Box>

        <Box w={['100%', '100%', '40%']} alignSelf={['start']} px='2rem'>
          <Heading as='h2'>Get in touch</Heading>

          <Text color='#1c165c' fontSize='1.7rem' mt='2rem'>
            (90) 898 789-8957
          </Text>
          <Text color='gray.500' mt='1rem' lineHeight='2' fontSize='14px'>
            laundry@567.com
            <br />
            789/A, Green road NYC-9089
          </Text>
        </Box>
      </Flex>

      <Text textAlign='center' mt='7rem' fontSize='15px' fontWeight='bold'>
        Copyright &copy; 2021 All rights reserved.
      </Text>
    </Box>
  )
}

export default Footer
