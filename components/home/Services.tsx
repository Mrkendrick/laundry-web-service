import React from 'react'
import {
  Box,
  Text,
  Heading,
  HStack,
  Image,
  Flex,
  Button,
} from '@chakra-ui/react'
import { FaPhoneAlt } from 'react-icons/fa'

const Services = () => {
  return (
    <Box
      mt='110px'
      mx='auto'
      textAlign='center'
      w={['95%', '95%', '90%']}
      id='services'
    >
      <Text
        color='blue.800'
        fontSize='14px'
        py='0.5rem'
        px='1.8rem'
        rounded='full'
        bg='blue.50'
        display='inline-block'
        mb='2rem'
      >
        Services
      </Text>
      <Heading color='blue.800' mb='3rem'>
        Services We Offer
      </Heading>
      <Flex
        alignItems='center'
        justifyContent='space-evenly'
        flexDirection={['column', 'column', 'row']}
        w={['95%', '95%', '80%']}
        mx='auto'
        spacing={9}
      >
        <Box w={['95%', '95%', '33%']} mb={['3rem', '3rem', '0rem']}>
          <Box
            bg='#2B5FBD'
            w='110px'
            mx='auto'
            h='110px'
            padding='1.5rem'
            rounded='full'
            position='relative'
          >
            <Image
              position='absolute'
              transform='translate(-50%, -50%)'
              top='50%'
              left='50%'
              src='/bubbles.svg'
              width='70px'
              height='70px'
            />
          </Box>
          <Heading color='gray.700' fontSize='24px' my='1.2rem'>
            Cloth Laundry
          </Heading>
          <Text color='#57667e' lineHeight='30px' fontSize='16px'>
            The automated process starts as soon as your clothes go into the
            machine. The outcome is gleaming clothes!
          </Text>
        </Box>

        <Box w={['95%', '95%', '33%']} mb={['3rem', '3rem', '0rem']}>
          <Box
            position='relative'
            bg='#2B5FBD'
            mx='auto'
            w='110px'
            h='110px'
            padding='1.5rem'
            rounded='full'
          >
            <Image
              position='absolute'
              transform='translate(-50%, -50%)'
              top='50%'
              left='50%'
              src='/iron.svg'
              width='70px'
              height='70px'
            />
          </Box>
          <Heading color='gray.700' fontSize='24px' my='1.2rem'>
            Cloth Ironing
          </Heading>
          <Text color='#57667e' lineHeight='30px' fontSize='16px'>
            The automated process starts as soon as your clothes go into the
            machine. The outcome is gleaming clothes!
          </Text>
        </Box>
      </Flex>
      <Flex
        alignItems='center'
        flexDirection={['column', 'column', 'row']}
        textAlign={['center', 'center', 'left']}
        justifyContent={['center', 'center', 'space-between']}
        height='250px'
        bgPosition='bottom'
        w='90%'
        mx='auto'
        mt='4rem'
        color='white'
        px='2rem'
        bgColor='rgb(43,95,189)'
        rounded='md'
        bgImage={`url('/hill.png')`}
        bgRepeat='no-repeat'
      >
        <Box mb={['1.2rem', '1rem', '0']}>
          <Heading>Call Us For A Service</Heading>
          <Text>We offer the best laundry service.</Text>
        </Box>
        <HStack>
          <Button
            leftIcon={<FaPhoneAlt />}
            colorScheme='white'
            variant='outline'
            size='lg'
            rounded='full'
          >
            Call us
          </Button>
        </HStack>
      </Flex>
    </Box>
  )
}

export default Services
