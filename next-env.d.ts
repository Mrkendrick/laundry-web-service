/// <reference types="next" />
/// <reference types="next/types/global" />

declare module '*.scss'
declare module '*.css'

declare module '*.png' {
  const content: any
  export default content
}

declare module '*.jpg' {
  const content: any
  export default content
}
