import axios from 'axios'
import Cookies from 'universal-cookie'
import { iwad } from './generics'

export const imageURL = 'http://192.168.8.101:5000/img/users'

// export const cloudinaryURL = `https://api.cloudinary.com/v1_1/${cloudinaryName}/image/upload`

// const baseURL = 'http://localhost:5000/api/v1/'

const baseURL = 'https://instant-wash-and-dry-laundry.herokuapp.com/api/v1/'

const cookies = new Cookies()

const defaultConfig = {
  baseURL,
  timeout: 60000,
  headers: {
    'Content-type': ['application/json'],
    'Access-Control-Allow-Origin': '*',
  },
}

const api = axios.create({ ...defaultConfig })

api.interceptors.request.use(
  config => {
    const token = cookies.get(iwad, {})
    if (token) config.headers.Authorization = `Bearer ${token}`

    return config
  },
  err => Promise.reject(err),
)

class API {
  async login(data: { email: string; password: string }) {
    return api.post('/auth/login', data)
  }

  async signup(data: {
    name: string
    email: string
    password: string
    passwordConfirm: string
  }) {
    return api.post('/auth/signup', data)
  }

  async forgotPassword(data: { email: string }) {
    return api.post('auth/forgotPassword', data)
  }

  async resetPassword(
    resetToken: string,
    data: { password: string; passwordConfirm: string },
  ) {
    return api.patch(`auth/resetPassword/${resetToken}`, data)
  }

  async sendMessage(data: any) {
    return api.post('/message', data)
  }

  async getProfile() {
    return api.get('/user')
  }

  async updateProfile(data: any) {
    return api.patch('/user/updateUser', data)
  }

  async getLaundries(
    query?: string | string[],
    options?: { page: number; limit: number },
  ) {
    return query
      ? api.get(
          `/laundry?email=${query}&page=${options?.page || 1}&limit=${
            options?.limit || 10
          }`,
        )
      : api.get(
          `/laundry?page=${options?.page || 1}&limit=${options?.limit || 10}`,
        )
  }

  async getLaundry(id: string) {
    return api.get(`/laundry/${id}`)
  }

  async updateLaundry(id: string, data: any) {
    return api.patch(`laundry/${id}`, data)
  }

  async getPrices() {
    return api.get('/prices')
  }

  async createLaundry(data: any) {
    return api.post('/laundry', data)
  }

  async imageUpload(data: any) {
    // return axios.post(cloudinaryURL, data)
  }

  async getUsers(query?: string, options?: { page: number; limit: number }) {
    return query
      ? api.get(
          `/user/all?email=${query}&page=${options?.page || 1}&limit=${
            options?.limit || 10
          }`,
        )
      : api.get(
          `/user/all?page=${options?.page || 1}&limit=${options?.limit || 10}`,
        )
  }

  async getUser(id: string) {
    return api.get(`/user/all/${id}`)
  }

  async usersByEmail(email: string) {
    return api.get(`/user/all?email=${email}`)
  }

  async sendNotification(data: any) {
    return api.post('/notification', data)
  }

  async getNotifications() {
    return api.get('/notification')
  }

  async getMessages(options?: { page: number; limit: number }) {
    return api.get(
      `/message?page=${options?.page || 1}&limit=${options?.limit || 10}`,
    )
  }

  async getMessage(id: string) {
    return api.get(`/message/${id}`)
  }

  async deleteMessages() {
    return api.delete('/message')
  }

  async getCustomers(options?: { page: number; limit: number }) {
    return api.get(
      `/customer?page=${options?.page || 1}&limit=${options?.limit || 10}`,
    )
  }

  async getCustomer(id?: string | string[]) {
    return api.get(`/customer/${id}`)
  }

  async deleteCustomer(id: string) {
    return api.delete(`/customer/${id}`)
  }

  async searchCustomer(
    query: string,
    options?: { page: number; limit: number },
  ) {
    return api.get(
      `/customer/search/${query}?page=${options?.page || 1}&limit=${
        options?.limit || 10
      }`,
    )
  }

  async createCustomer(data: any) {
    return api.post('/customer', data)
  }

  async updateCustomer(id: string, data: any) {
    return api.patch(`/customer/${id}`, data)
  }

  async getCloths() {
    return api.get('/cloth')
  }

  async updatePrices(id: string, data: any) {
    return api.patch(`/prices/${id}`, data)
  }

  async getSettings() {
    return api.get('/settings')
  }
}

const APIService = new API()

export default APIService
