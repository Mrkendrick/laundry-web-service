export enum EPaymentMethods {
  cash = 'cash',
  pos = 'pos',
  transfer = 'transfer',
  card = 'card',
}

export enum EServiceType {
  dropoff = 'dropoff',
  delivery = 'delivery',
  self = 'self',
}

export enum EProgress {
  pending = 'pending',
  washing = 'washing',
  delivered = 'delivered',
  cancelled = 'cancelled',
}

export enum EAutoCap {
  words = 'words',
  none = 'none',
  sentences = 'sentences',
  characters = 'characters',
}

export enum EKeyboardType {
  default = 'default',
  email = 'email-address',
  numeric = 'numeric',
  phone = 'phone-pad',
  number = 'number-pad',
  decimal = 'decimal-pad',
}

export enum EAutoCompleteType {
  email = 'email',
  name = 'name',
  tel = 'tel',
  password = 'password',
}

export enum EOnChangeType {
  email = 'email',
  phone = 'phone',
  name = 'name',
  address = 'address',
}

export const iwad = 'iwad'

export type TRole = 'user' | 'dispatch' | 'attendant' | 'manager' | ''
