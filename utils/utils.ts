import { createStandaloneToast } from '@chakra-ui/toast';
import moment from 'moment';

interface IStorage {
  type: 'get' | 'set' | 'del';
  key: string;
  value?: any | undefined;
}
class Utils {
  emailRegex: RegExp;
  costRegex: RegExp;
  phoneRegex: RegExp;

  constructor() {
    this.emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    this.costRegex = /\B(?=(\d{3})+(?!\d))/g;
    this.phoneRegex = /^\d{10}$/;
  }

  toast(options: {
    title: string;
    status: 'success' | 'error' | 'info' | 'warning';
    description?: string;
    duration?: number;
  }) {
    const defaultConfig = {
      duration: 5000,
      isClosable: true,
    };

    const toast = createStandaloneToast();
    toast({ ...defaultConfig, ...options, position: 'bottom-right' });
  }

  formatPrice(cost: number) {
    return cost.toString().replace(this.costRegex, ',');
  }

  getFirstName(name?: string) {
    return name?.includes(' ') ? name.split(' ')[0] : name;
  }

  getLastName(name?: string) {
    return name?.includes(' ') ? name?.split(' ')[0] : name;
  }

  dateFromNow(date: string) {
    return moment(date).fromNow();
  }

  formatDate(date: string, type: string) {
    return moment(date).format(type);
  }

  error(error: any) {
    if (error?.message === 'Network Error') {
      this.toast({
        title: 'You are offline',
        description: 'Please check your connection',
        status: 'error',
      });
    } else if (error?.response?.data?.message) {
      this.toast(error?.response?.data?.message);
    } else {
      this.toast({ title: 'Something went wrong!', status: 'error' });
    }
  }

  checkNetwork() {
    const condition = navigator.onLine;

    if (condition) {
      this.toast({ title: 'You are connected', status: 'success' });
    } else {
      this.toast({
        title: 'You are offline',
        status: 'error',
        description: 'Check your internet connection.',
        duration: 10000,
      });
    }
  }
}

const UtilityService = new Utils();

export default UtilityService;

export enum EStorage {
  onBoarding = 'onboaring',
  token = 'token',
}
